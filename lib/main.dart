import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/api/api.dart';
import 'package:krispykreme/api/security_interceptor.dart';
import 'package:krispykreme/api/session_invalidation_notifier.dart';
import 'package:krispykreme/api/token_inteceptor.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/card_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/blocs/page_bloc.dart';
import 'package:krispykreme/blocs/store_bloc.dart';
import 'package:krispykreme/dao/mao_dao.dart';
import 'package:krispykreme/repositories/contracts/auth_repository.dart';
import 'package:krispykreme/repositories/contracts/card_repository.dart';
import 'package:krispykreme/repositories/contracts/page_repository.dart';
import 'package:krispykreme/repositories/contracts/store_repository.dart';
import 'package:krispykreme/repositories/implementations/auth_repository_impl.dart';
import 'package:krispykreme/repositories/implementations/card_repository_impl.dart';
import 'package:krispykreme/repositories/implementations/page_repository_impl.dart';
import 'package:krispykreme/repositories/implementations/store_repository_impl.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/about_us_page.dart';
import 'package:krispykreme/screens/branches_page.dart';
import 'package:krispykreme/screens/card_page.dart';
import 'package:krispykreme/screens/contact_us_page.dart';
import 'package:krispykreme/screens/edit_profile_page.dart';
import 'package:krispykreme/screens/getting_started_page.dart';
import 'package:krispykreme/screens/history_page.dart';
import 'package:krispykreme/screens/landing_page.dart';
import 'package:krispykreme/screens/newsletter_page.dart';
import 'package:krispykreme/screens/notification_page.dart';
import 'package:krispykreme/screens/profile_page.dart';
import 'package:krispykreme/screens/promos_page.dart';
import 'package:krispykreme/screens/dashboard_page.dart';
import 'package:krispykreme/screens/faqs_page.dart';
import 'package:krispykreme/screens/forgot_password_page.dart';
import 'package:krispykreme/screens/login_question_page.dart';
import 'package:krispykreme/screens/login_page.dart';
import 'package:krispykreme/screens/registration_OGCard_Page.dart';
import 'package:krispykreme/screens/registration_activation.dart';
import 'package:krispykreme/screens/splash_screen_page.dart';
import 'package:krispykreme/screens/success_page.dart';
import 'package:krispykreme/screens/terms_and_condition_page.dart';
import 'package:krispykreme/screens/videos_page.dart';
import 'package:krispykreme/screens/vouchers_page.dart';

void main() => runApp(PlainsAndPrintsApp());

final Dio tokenDio = Dio();

final Dio dio = Dio();

final SessionInvalidationNotifier sessionInvalidationNotifier =
SessionInvalidationNotifier();

final TokenInterceptor interceptor =
TokenInterceptor(tokenDio: tokenDio, mainDio: dio, dao: mapDao);

final LogInterceptor logInterceptor = LogInterceptor(
    requestBody: true, responseBody: true);

final SecurityInterceptor securityInterceptor = SecurityInterceptor();

final Api api = Api(
    dio: dio,
    tokenInterceptor: interceptor,
    logInterceptor: logInterceptor,
    securityInterceptor: securityInterceptor,
    mapDao: mapDao,
    sessionInvalidationNotifier: sessionInvalidationNotifier);

final MapDao mapDao = MapDao();

final AuthRepository authRepository =
AuthRepositoryImpl(api: api, mapDao: mapDao);
final CardRepository cardRepository = CardRepositoryImpl(api: api);
final StoreRepository storeRepository = StoreRepositoryImpl(api: api);
final PageRepository pageRepository = PageRepositoryImpl(api: api);

final AuthBloc authBloc = AuthBloc(authRepository: authRepository);
final CardBloc cardBloc = CardBloc(cardRepository: cardRepository);
final StoreBloc storeBloc = StoreBloc(storeRepository: storeRepository);
final PageBloc pageBloc = PageBloc(pageRepository: pageRepository);

class PlainsAndPrintsApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    return MasterProvider(
      cardBloc: cardBloc,
      storeBloc: storeBloc,
      authBloc: authBloc,
      pageBloc: pageBloc,
      sessionInvalidationNotifier: sessionInvalidationNotifier,
      child: MaterialApp(
        title: 'Krispy Kreme And Prints',
        theme: ThemeData(
          fontFamily: "Quicksand",
          primarySwatch: Colors.blue,
          textTheme: TextTheme(bodyText1: TextStyle(color: AppColors.gray1)),
          brightness: Brightness.light
        ),
        routes: {
          Routes.SPLASH_SCREEN: (context) => SplashScreen(),
          Routes.GETTING_STARTED: (context) => GettingStarted(),
          Routes.REGISTRATION_QUESTION: (context) => OGCardPage(),
          Routes.LANDING: (context) => LandingPage(),
          Routes.REGISTRATION: (context) => ActivationPage(),
          Routes.LOGIN_QUESTION_PAGE: (context) => LoginQuestionPage(),
          Routes.LOGIN: (context) => LoginPage(),
          Routes.DASHBOARD: (context) => DashboardPage(),
          Routes.NOTIFICATION: (context) => NotificationPage(),
          Routes.PROFILEPAGE: (context) => ProfilePage(),
          Routes.NEWSLETTER: (context) => NewsletterPage(),
          Routes.VIDEOS: (context) => VideosPage(),
          Routes.PROMOS: (context) => PromosPage(),
          Routes.HISTORY: (context) => HistoryPage(),
          Routes.EDIT_PROFILE: (context) => EditProfilePage(),
          Routes.FORGOT_PASSWORD: (context) => ForgotPasswordPage(),
          Routes.BRANCHES: (context) => BranchesPage(),
          Routes.CARD: (context) => CardPage(),
          Routes.VOUCHERS: (context) => VouchersPage(),
          Routes.SUCCESS_PAGE: (context) => SuccessPage(),
          Routes.TERMS_AND_CONDITIONS: (context) => TermsAndConditionPage(),
          Routes.ABOUT_US: (context) => AboutUsPage(),
          Routes.CONTACT_US: (context) => ContactUsPage(),
          Routes.FAQS: (context) => FAQsPage(),
        },
      ),
    );
  }
}