import 'package:flutter/widgets.dart';
import 'package:krispykreme/model/cards.dart';
import 'package:krispykreme/model/city.dart';
import 'package:meta/meta.dart';
import 'package:krispykreme/blocs/bloc.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/balance.dart';
import 'package:krispykreme/model/history.dart';
import 'package:krispykreme/model/request/balance_request.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/model/voucher.dart';
import 'package:krispykreme/model/vouchers.dart';
import 'package:krispykreme/repositories/contracts/card_repository.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:rxdart/rxdart.dart';

class CardBloc implements Bloc {
  final CardRepository cardRepository;

  CardBloc({@required this.cardRepository});

  PublishSubject<ScreenState> get updateState => _updateState;

  PublishSubject<ScreenState> get cancelVoucherState => _cancelVoucherState;

  PublishSubject<Voucher> get newVoucher => _newVoucher;

  BehaviorSubject<Balance> get balance => _balance;

  BehaviorSubject<User> get currentUser => _currentUser;
  
  BehaviorSubject<Cards> get cards => _card;

  BehaviorSubject<History> get history => _history;

  BehaviorSubject<Vouchers> get vouchers => _vouchers;

  PublishSubject<Voucher> get voucherDetails => _voucherDetails;

  BehaviorSubject<Cities> get cities => _cities;

  void getAccountDetails(String accountId) async {
    try {
      final User user = await cardRepository.getAccountDetails(accountId);
      _currentUser.add(user);
    } catch (error) {
      _currentUser.addError(error);
    }
  }

  void getCardDetails(String accountId) async {
    try {
      final Cards card = await cardRepository.getCardDetails(accountId);
      _card.add(card);
    } catch (error) {
      _card.addError(error);
    }
  }

  void getBalance(BalanceRequest request) async {
    try {
      final Balance balance = await cardRepository.getBalance(request);
      _balance.add(balance);
    } catch (error) {
      _balance.addError(error);
    }
  }

  Future<void> getHistory(ListRequest request) async {
    try {
      final History newHistory = await cardRepository.getHistory(request);
      _history.add(newHistory);
    } catch (error) {
      _history.addError(error);
    }
  }

  Future<void> getCities() async {
    try {
      final Cities cities = await cardRepository.getCities();
      _cities.add(cities);
    } catch (error) {
      print(error);
      _cities.addError(error);
    }
  }

  Future<void> getVouchers(ListRequest request) async {
    try {
      final Vouchers newVouchers = await cardRepository.getVouchers(request);
      _vouchers.add(newVouchers);
    } catch (error) {
      if (_vouchers.value == null) _vouchers.addError(error);
    }
  }


  void getVoucherDetails(String username, int voucherId) async {
    try {
      final Voucher voucher = await cardRepository.getVoucherDetails(username, voucherId);
      _voucherDetails.add(voucher);
    } catch (error) {
      _voucherDetails.addError(error);
    }
  }

  void updateAccount(String accountId, User user) async {
    try {
      _updateState.add(ScreenState(States.WAITING));
      await cardRepository.updateAccount(accountId, user);
      _updateState.add(ScreenState(States.DONE));
      getAccountDetails(accountId);
    } catch (error) {
      _updateState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void close() {
    _currentUser.close();
    _voucherDetails.close();
    _card.close();
    _balance.close();
    _history.close();
    _updateState.close();
    _newVoucher.close();
    _cancelVoucherState.close();
    _cities.close();
  }

  @override
  void clearData() {
    _currentUser.add(null);
    _card.add(null);
    _balance.add(null);
    _history.add(null);
    _vouchers.add(null);
    _cities.add(null);
  }
  final BehaviorSubject<User> _currentUser = BehaviorSubject();
  final BehaviorSubject<Cards> _card = BehaviorSubject();
  final BehaviorSubject<Balance> _balance = BehaviorSubject();
  final BehaviorSubject<History> _history = BehaviorSubject();
  final BehaviorSubject<Vouchers> _vouchers = BehaviorSubject();
  final PublishSubject<Voucher> _voucherDetails = PublishSubject();
  final PublishSubject<ScreenState> _updateState = PublishSubject();
  final PublishSubject<ScreenState> _cancelVoucherState = PublishSubject();
  final PublishSubject<Voucher> _newVoucher = PublishSubject();
  final BehaviorSubject<Cities> _cities = BehaviorSubject();


}