import 'package:krispykreme/model/activation.dart';
import 'package:krispykreme/model/request/link_account.dart';
import 'package:meta/meta.dart';
import 'package:krispykreme/blocs/bloc.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/request/activation_request.dart';
import 'package:krispykreme/model/request/change_password_request.dart';
import 'package:krispykreme/model/request/forgot_password_password.dart';
import 'package:krispykreme/model/request/login_request.dart';
import 'package:krispykreme/model/request/member_request.dart';
import 'package:krispykreme/model/request/reset_password_request.dart';
import 'package:krispykreme/model/request/verification_request.dart';
import 'package:krispykreme/model/session.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/repositories/contracts/auth_repository.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc implements Bloc {
  final AuthRepository authRepository;

  PublishSubject<ScreenState> get loginState => _loginState;

  PublishSubject<ScreenState> get loginwithFbState => _loginwithFbState;

  PublishSubject<ScreenState> get registerState => _registerState;

  PublishSubject<ScreenState> get verificationState => _verificationState;

  PublishSubject<ScreenState> get activationState => _activationState;

  PublishSubject<ScreenState> get changePasswordState => _changePasswordState;

  PublishSubject<ScreenState> get forgotPasswordState => _forgotPasswordState;
  
  PublishSubject<ScreenState> get linkAccountState => _linkAccount;

  PublishSubject<ScreenState> get resetPasswordState => _resetPasswordState;

  PublishSubject<User> get existingMember => _existingMember;

  BehaviorSubject<User> get user => _user;

  PublishSubject<VerificationRequest> get registrationCredentials => _registrationCredentials;
  
  BehaviorSubject<Session> get session => _session;

  AuthBloc({@required this.authRepository}) {
    this.authRepository.getCachedSession().then((session) {
      if (session.accountId != null)
        _session.add(session);
    });
  }

  void existingmember(MemberRequest request, String cardNumber) async {
    try {
      final User newuser = await authRepository.existingMember(request, cardNumber);
      _existingMember.add(newuser);
      _user.add(null);
    } catch (error) {
      _existingMember.addError(error);
    }
  }

  void login(LoginRequest request, String method, String fcmToken) async {
    try {
      _loginState.add(ScreenState(States.WAITING));
      final Session session = await authRepository.login(request, method, fcmToken);
      _session.add(session);
      _loginState.add(ScreenState(States.DONE));
    } catch (error) {
      print(error);
      _session.addError(error);
      _loginState.add(ScreenState(States.ERROR, error: error));
    }
  }


  void loginwithFb(LoginRequest request, String method, String fcmToken) async {
    try {
      _loginwithFbState.add(ScreenState(States.WAITING));
      final Session session = await authRepository.login(request, method, fcmToken);
      _session.add(session);
      _loginwithFbState.add(ScreenState(States.DONE));
    } catch (error) {
      print(error);
      _session.addError(error);
      _loginwithFbState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void register(User user) async {
    try {
      _registerState.add(ScreenState(States.WAITING));
      final VerificationRequest credentials =
      await authRepository.register(user);
      _registrationCredentials.add(credentials);
      _registerState.add(ScreenState(States.DONE));
    } catch (error) {
      _registerState.add(ScreenState(States.ERROR, error: error));
    }
  }

    void activate(Activation activation, String cardNumber) async {
    try {
      _registerState.add(ScreenState(States.WAITING));
      await authRepository.activate(activation, cardNumber);
      _registerState.add(ScreenState(States.DONE));
    } catch (error) {
      _registerState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void forgotPassword(ForgotPasswordRequest request) async {
    try {
      _forgotPasswordState.add(ScreenState(States.WAITING));
      await authRepository.forgotPassword(request);
      _forgotPasswordState.add(ScreenState(States.DONE));
    } catch (error) {
      _forgotPasswordState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void changePassword(ChangePasswordRequest request) async {
    try {
      _changePasswordState.add(ScreenState(States.WAITING));
      await authRepository.changePassword(request);
      _changePasswordState.add(ScreenState(States.DONE));
    } catch (error) {
      _changePasswordState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void resetPassword(ResetPasswordRequest request) async {
    try {
      _resetPasswordState.add(ScreenState(States.WAITING));
      await authRepository.resetPassword(request);
      _resetPasswordState.add(ScreenState(States.DONE));
    } catch (error) {
      _resetPasswordState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void verifyAccount(VerificationRequest request) async {
    try {
      _verificationState.add(ScreenState(States.WAITING));
      await authRepository.verifyAccount(request);
      _verificationState.add(ScreenState(States.DONE));
    } catch (error) {
      _verificationState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void activateAccount(ActivationRequest request) async {
    try {
      _activationState.add(ScreenState(States.WAITING));
      await authRepository.activateAccount(request);
      _activationState.add(ScreenState(States.DONE));
    } catch (error) {
      _activationState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void linkAccount(LinkAccountRequest request) async {
    try {
      _linkAccount.add(ScreenState(States.WAITING));
      await authRepository.linkAccount(request);
      _linkAccount.add(ScreenState(States.DONE));
    } catch (error) {
      _linkAccount.add(ScreenState(States.ERROR, error: error));
    }
  }

  void unlinkAccount(LinkAccountRequest request) async {
    try {
      _linkAccount.add(ScreenState(States.WAITING));
      await authRepository.unlinkAccount(request);
      _linkAccount.add(ScreenState(States.DONE));
    } catch (error) {
      _linkAccount.add(ScreenState(States.ERROR, error: error));
    }
  }


  close() {
    _session.close();
    _loginState.close();
    _loginwithFbState.close();
    _registerState.close();
    _changePasswordState.close();
    _forgotPasswordState.close();
    _linkAccount.close();
    _resetPasswordState.close();
    _verificationState.close();
    _activationState.close();
    _registrationCredentials.close();
    _existingMember.close();
    _user.close();
  }

  final BehaviorSubject<Session> _session = BehaviorSubject();
  final PublishSubject<ScreenState> _loginState = PublishSubject();
  final PublishSubject<ScreenState> _loginwithFbState = PublishSubject();
  final PublishSubject<VerificationRequest> _registrationCredentials = PublishSubject();
  final PublishSubject<ScreenState> _registerState = PublishSubject();
  final PublishSubject<ScreenState> _verificationState = PublishSubject();
  final PublishSubject<ScreenState> _activationState = PublishSubject();
  final PublishSubject<ScreenState> _changePasswordState = PublishSubject();
  final PublishSubject<ScreenState> _resetPasswordState = PublishSubject();
  final PublishSubject<ScreenState> _forgotPasswordState = PublishSubject();
  final PublishSubject<ScreenState> _linkAccount = PublishSubject();
  final BehaviorSubject<User> _user = BehaviorSubject();
  final PublishSubject<User> _existingMember = PublishSubject();

  @override
  void clearData() {
    _session.add(null);
  }

  Future<bool> hasSession() async {
    final Session session = await authRepository.getCachedSession();
    return session.accountId != null;
  }
}
