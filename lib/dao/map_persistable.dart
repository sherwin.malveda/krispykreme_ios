import 'package:krispykreme/dao/mao_dao.dart';

abstract class MapPersistable<T> {
  void saveMap(MapDao preference);
}
