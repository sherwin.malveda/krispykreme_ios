import 'package:shared_preferences/shared_preferences.dart';

class MapDao {
  SharedPreferences preferences;

  MapDao() {
    init();
  }

  MapDao.withPrefs(this.preferences);

  String getString(String key) => preferences.getString(key);

  int getInt(String key) => preferences.getInt(key);

  void setString(String key, value) => preferences.setString(key, value);

  void setInt(String key, value) => preferences.setInt(key, value);

  Future<SharedPreferences> init() async {
    preferences = await SharedPreferences.getInstance();
    return preferences;

  }
}
