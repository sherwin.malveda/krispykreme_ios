import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/api/session_invalidation_notifier.dart';
import 'package:krispykreme/routes.dart';
import 'blocs/master_provider.dart';

abstract class MasterState<T extends StatefulWidget> extends State<T>
    with AfterLayoutMixin<T> {
  SessionInvalidationNotifier _notifier;

  @override
  void didChangeDependencies() {
    _notifier = MasterProvider.sessionInvalidator(context);
    if (_notifier.hasListeners) {
      _notifier.removeListener(onInvalidSession);
    }
    _notifier.addListener(onInvalidSession);
    super.didChangeDependencies();
  }

  void onInvalidSession() {
    logout();
  }

  @override
  void dispose() {
    _notifier.removeListener(onInvalidSession);
    super.dispose();
  }

  void logout() {
    print("LOGOUT");
    MasterProvider.clearData(context);
    Navigator.of(context).pushNamedAndRemoveUntil(
        Routes.SPLASH_SCREEN, ModalRoute.withName(Routes.SPLASH_SCREEN));
  }

}
