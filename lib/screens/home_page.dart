import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/card_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/blocs/store_bloc.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/controllers/navigation_controller.dart';
import 'package:krispykreme/db/dbHelper.dart';
import 'package:krispykreme/model/news_letters.dart';
import 'package:krispykreme/model/offline_data_model/newsletter.dart';
import 'package:krispykreme/model/promos.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/model/voucher.dart';
import 'package:krispykreme/model/vouchers.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/hotlight_page.dart';
import 'package:krispykreme/screens/news_detailed_page.dart';
import 'package:krispykreme/screens/promos_detailed_page.dart';
import 'package:krispykreme/screens/videos_detailed_page.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/news_list.dart';
import 'package:krispykreme/widgets/videos_list.dart';
import 'package:krispykreme/widgets/promo_list.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  final NavigationController navigationController;

  const HomePage({Key key, @required this.navigationController})
      : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  RefreshController _refreshController = RefreshController(initialRefresh: false);
  final dbHelper = DatabaseHelper.instance;
  StoreBloc _storeBloc;
  CardBloc _cardBloc;
  AuthBloc _authBloc;
  var count, name = "";
  String hotlightOnOff = "Off";

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
    gethotlight();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(0, 90, 0, 0),
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: ClassicHeader(
            refreshingText: "Refreshing",
            textStyle: TextStyle(fontFamily: 'Quicksand'),
          ),
          footer: CustomFooter(
            builder: (BuildContext context,LoadStatus mode){
              Widget body ;
              if(mode==LoadStatus.loading){body =  CupertinoActivityIndicator();}
              else{body = Text("No more items", style: TextStyle(fontFamily: 'Quicksand'),);}
              return Container(
                height: 55.0,
                child: Center(child:body),
              );
            },
          ),       
          controller: _refreshController,
          onRefresh:()=> _onRefresh(),
          onLoading:()=> _onLoading(),
          child: ListView(
            padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
            children: <Widget>[
              hotlight(),      
              Container(
                child: Text(
                  "HOT LIGHT ALERT!",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: AppColors.pink
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                child: Text(
                  "Top the hot light to see the nearest\nKrispy Kreme Store cooking up a fresh batch for you",
                  style: TextStyle(
                    fontSize: 15,
                    fontFamily: 'Quicksand',
                    color: AppColors.green
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 20),
              Container(
                child: Text(
                  "NEW E-VOUCERS",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: AppColors.green
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 10),
              StreamBuilder<Vouchers>(
                stream: _cardBloc.vouchers,
                builder: (context, snapshot) {
                  return StreamHandler(
                    snapshot: snapshot,
                    noData: Center(
                      child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                        child: Text("No voucher at the moment",
                        )
                      )
                    ),
                    loading: Center(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: CircularProgressIndicator()
                      )
                    ),
                    withError: (error) =>  Center(
                      child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                        child: Text("No voucher at the moment",
                        )
                      )
                    ),
                    withData: (Vouchers vouchers) {
                      return SizedBox(
                        height: MediaQuery.of(context).size.height * .2,
                        child: Container(
                          margin: EdgeInsets.fromLTRB(5.0,0.0,5.0,.0),
                          child: Carousel(
                            dotColor: AppColors.green,
                            indicatorBgPadding: 10,
                            dotBgColor: Colors.transparent,
                            dotIncreasedColor: AppColors.green,
                            dotSize: 5.0,
                            dotSpacing: 20.0,
                            autoplay: true,
                            autoplayDuration: Duration(seconds: 10),
                            boxFit: BoxFit.contain,
                            images: vouchers.vouchers
                              .take(vouchers.vouchers.length > 5
                                ? 5
                                : vouchers.vouchers.length)
                                .map((voucher) =>
                                bgImages('${voucher.voucherCode}',voucher.voucherType)).toList(),
                            onImageTap: (value) {}    
                          )
                        )
                      );
                    }
                  );
                }
              ),      
              SizedBox(height: 50),
              Container(
                height: 2,
                width: MediaQuery.of(context).size.width * 1,
                color: Colors.black12,
              ),
              SizedBox(height: 50),
              Container(
                child: Text(
                "FEATURED PROMOS",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: AppColors.green
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              StreamBuilder<Promos>(
                stream: _storeBloc.promos,
                builder: (context, snapshot) {
                  return StreamHandler(
                    snapshot: snapshot,
                    noData: Center(
                      child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                        child: Text("No promos at the moment",
                        )
                      )
                    ),
                    loading: Center(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: CircularProgressIndicator()
                      )
                    ),
                    withError: (error) =>  Center(
                      child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                        child: Text("No promos at the moment",
                        )
                      )
                    ),
                    withData: (Promos promos) {
                      return Column(
                        children: [
                          getPromos(promos, promos.totalPromotions, 0),
                          getPromos(promos, promos.totalPromotions, 1),
                          getPromos(promos, promos.totalPromotions, 2),
                          getPromos(promos, promos.totalPromotions, 3)
                        ],
                      );
                    }
                  );
                }
              ),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
                height: 30,
                alignment: Alignment.center,
                  child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(15.0),
                  child: MaterialButton(
                    height: 30,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        side: BorderSide(color: AppColors.green)
                    ),
                    child: SizedBox(
                      width:MediaQuery.of(context).size.width * .3, 
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: Text("View All Promos", 
                              style: TextStyle(fontSize: 12.0, color: AppColors.green),textAlign: TextAlign.center,),
                          ),
                          SizedBox(width: 10),
                          Image.asset("assets/icons/rightarrow.png"),
                        ]
                      )
                    ),
                    onPressed: () async{Navigator.of(context).pushNamed(Routes.PROMOS);},
                  ),
                )
              ),
              SizedBox(height: 30),
              Container(
                height: 2,
                width: MediaQuery.of(context).size.width * 1,
                color: Colors.black12,
              ),
              SizedBox(height: 50),
              Container(
                child: Text(
                "NEWS",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: AppColors.green
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 10),
              StreamBuilder<NewsLetters>(
                stream: _storeBloc.newsLetters,
                builder: (context, snapshot) {
                  return StreamHandler(
                    snapshot: snapshot,
                    noData: Center(
                      child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                        child: Text("No news at the moment",
                        )
                      )
                    ),
                    loading: Center(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: CircularProgressIndicator()
                      )
                    ),
                    withError: (error) =>  Center(
                      child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                        child: Text("No news at the moment",
                        )
                      )
                    ),
                    withData: (NewsLetters newsletter) {
                      return SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: 10),
                            getNewsletter(newsletter, newsletter.totalNewsLetters, 0),
                            getNewsletter(newsletter, newsletter.totalNewsLetters, 1),
                            getNewsletter(newsletter, newsletter.totalNewsLetters, 2),
                            getNewsletter(newsletter, newsletter.totalNewsLetters, 3),
                            SizedBox(width: 20)
                          ]
                        )
                      ); 
                    }
                  );
                }
              ),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
                height: 30,
                alignment: Alignment.center,
                  child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(15.0),
                  child: MaterialButton(
                    height: 30,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        side: BorderSide(color: AppColors.green)
                    ),
                    child: SizedBox(
                      width:MediaQuery.of(context).size.width * .3,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: Text("View All News", 
                              style: TextStyle(fontSize: 12.0, color: AppColors.green),textAlign: TextAlign.center,),
                          ),
                          SizedBox(width: 10),
                          Image.asset("assets/icons/rightarrow.png"),
                        ]
                      )
                    ),
                    onPressed: () async{Navigator.of(context).pushNamed(Routes.NEWSLETTER);},
                  ),
                )
              ),
              SizedBox(height: 30),
              Container(
                height: 2,
                width: MediaQuery.of(context).size.width * 1,
                color: Colors.black12,
              ),
              SizedBox(height: 50),
              Container(
                child: Text(
                "VIDEOS",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: AppColors.green
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 10),
              StreamBuilder<Promos>(
                stream: _storeBloc.promosvideos,
                builder: (context, snapshot) {
                  return StreamHandler(
                    snapshot: snapshot,
                    noData: Center(
                      child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                        child: Text("No videos at the moment",
                        )
                      )
                    ),
                    loading: Center(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: CircularProgressIndicator()
                      )
                    ),
                    withError: (error) =>  Center(
                      child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                        child: Text("No videos at the moment",
                        )
                      )
                    ),
                    withData: (Promos promos) {
                      return SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: 10),
                            getVideos(promos, promos.totalPromotions, 0),
                            getVideos(promos, promos.totalPromotions, 1),
                            getVideos(promos, promos.totalPromotions, 2),
                            getVideos(promos, promos.totalPromotions, 3),
                            SizedBox(width: 20)
                          ]
                        )
                      ); 
                    }
                  );
                }
              ),
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
                height: 30,
                alignment: Alignment.center,
                  child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(15.0),
                  child: MaterialButton(
                    height: 30,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        side: BorderSide(color: AppColors.green)
                    ),
                    child: SizedBox(
                      width:MediaQuery.of(context).size.width * .3, 
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: Text("View All Videos", 
                              style: TextStyle(fontSize: 12.0, color: AppColors.green),textAlign: TextAlign.center,),
                          ),
                          SizedBox(width: 10),
                          Image.asset("assets/icons/rightarrow.png"),
                        ]
                      )
                    ),
                    onPressed: () async{Navigator.of(context).pushNamed(Routes.VIDEOS);},
                  ),
                )
              ),
              SizedBox(height:30)
            ]
          ),
        ),        
      ),
    );
  }

  Widget hotlight() {
    if(hotlightOnOff == "On"){
      return FlatButton(
        shape: CircleBorder(),
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
          height: MediaQuery.of(context).size.height * .3,
          child: Image.asset("assets/image/kkhotlightgif.gif", fit: BoxFit.contain,)
        ),
        onPressed: (){
          Navigator.push(
            context, MaterialPageRoute(
              builder: (_) {
                return HotlightPage();
              }
            )
          );
        },
      );
    }else{
      return Container(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        height: MediaQuery.of(context).size.height * .3,
        child: Image.asset("assets/image/hottime.png", fit: BoxFit.contain,)
      );
    } 
  }

  Future gethotlight() async {
    var result = await dbHelper.selectAllData("HotLight");
    List<Map<String, dynamic>> data = result;
    final dataList = data;
    for(var items in dataList){  
      try{
        setState(() {
          final DateTime now = DateTime.now();
          final DateTime date = DateTime.parse(items["start_date"]);
          int remainingDays = date.difference(now).inMinutes;
          if(remainingDays < 0){
            int remainingDays = date.difference(now).inHours;
            if(remainingDays <= 0){
              int remainingDays = date.difference(now).inDays;
              if(remainingDays <= 0){
                hotlightOnOff = "Off";
                dbHelper.delete("HotLight");
              }else{
                hotlightOnOff = "On";
              }
            }else{
              hotlightOnOff = "On";
            }
          }else{
            hotlightOnOff = "On";
          }
        });
      }catch(Exception){}
    }
  }

  bgImages(String image, int type){
    if(image.length == 0 || image == null){
      return Image.asset('assets/image/logo.png', fit: BoxFit.contain);
    }
    else{
      return  Container(
        height: MediaQuery.of(context).size.height * .20,
        width: MediaQuery.of(context).size.width * 1,
        decoration: BoxDecoration(
          color: AppColors.blue2,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0))
        ),
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * .20,
              width: MediaQuery.of(context).size.width * 1,
              child: ClipRRect(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0)),
                child:Image.asset("assets/image/voucher.png", fit: BoxFit.cover)
              )
            ),
            Positioned(
              left:  MediaQuery.of(context).size.width * .135,
              top: MediaQuery.of(context).size.height * .03,
              child:Container(
                padding: EdgeInsets.all(5),
                height: MediaQuery.of(context).size.height * .133,
                width: MediaQuery.of(context).size.width * .295,
                child:voucherImage(type),
              )
            )
          ],
        )
      );
    }
  }

  Widget voucherImage(int type){
    if(type == 1){
      return Image.asset("assets/image/donut.png", fit: BoxFit.contain);
    }else if(type == 2){
      return Image.asset("assets/image/boxes.jpg", fit: BoxFit.contain);
    } else if(type == 3){
      return Image.asset("assets/image/donuts.jpg", fit: BoxFit.contain);
    }else{
      return Image.asset("assets/image/logo.png", fit: BoxFit.contain);
    }
  }

  getPromos(Promos promos, int i, int index){
    if(index < i){
      return PromoList(
        promo: promos.promotions[index].promoName,
        validity: promos.promotions[index].endDate,
        image: promos.promotions[index].promoImage,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>
              PromosDetailsPage(
                promosid: promos.promotions[index].promoId,
              )
            )
          );
        } 
      );
    }else{
      return Container();
    }
  }

  getNewsletter(NewsLetters newsLetters, int i, int index){
    if(index < i){
      return NewsList(
        news: newsLetters.newsLetters[index].title,
        description: "",
        image: newsLetters.newsLetters[index].imagePath,
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>
              NewsDetailsPage(
                newsid: newsLetters.newsLetters[index].id,
              )
            )
          );
        } 
      );
    }else{
      return Container();
    }
  }
  
  
  getVideos(Promos promos, int i, int index){
    if(index < i){
      return VideosList(
        description: promos.promotions[index].promoName,
        image: promos.promotions[index].promoImage,
        id: promos.promotions[index].promoId,
        onTap: () {
          goToWebsite(promos.promotions[index].promosVideo);
        } 
      );
    }else{
      return Container();
    }
  }

  
  goToWebsite(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      errorMessage('Could not launch $url', context);
    }
  }

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      gethotlight();
      _cardBloc.getAccountDetails(_authBloc.session.value.accountId);
      _cardBloc.getCardDetails(_authBloc.session.value.accountId);
      _cardBloc.getHistory(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _cardBloc.getVouchers(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getNotification(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getNewsLetter(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getPromos(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getPromosVideos(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getBranches(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: INFINITE_ROW_LIST,
        page: 1)
      );
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      gethotlight();
      _cardBloc.getAccountDetails(_authBloc.session.value.accountId);
      _cardBloc.getCardDetails(_authBloc.session.value.accountId);
      _cardBloc.getHistory(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _cardBloc.getVouchers(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getNotification(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getNewsLetter(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getPromos(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getPromosVideos(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getBranches(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: INFINITE_ROW_LIST,
        page: 1)
      );
    });
    _refreshController.loadComplete();
  }
}