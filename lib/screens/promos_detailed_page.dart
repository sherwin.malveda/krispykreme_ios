import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/blocs/store_bloc.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/promo_details.dart';
import 'package:krispykreme/model/request/link_account.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/success.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_share_plugin/social_share_plugin.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/dom.dart' as dom;
import 'package:http/http.dart' as http;

class PromosDetailsPage extends StatefulWidget {
  final int promosid;
  const PromosDetailsPage({Key key, @required this.promosid}) : super(key: key);
  @override
  _PromosDetailsPageState createState() => _PromosDetailsPageState();
}

class _PromosDetailsPageState extends State<PromosDetailsPage> {

  StoreBloc _storeBloc;
  AuthBloc _authBloc;
  StreamSubscription _stateSubs;
  bool linked = false;
  String facebookUrl = "";

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    _storeBloc.getPromoDetails(widget.promosid);
     _authBloc = MasterProvider.auth(context);
    _stateSubs = _authBloc.linkAccountState.listen(onStateChangedLinking);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    getFacebookLinked();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            StreamBuilder<PromoDetails>(
              stream: _storeBloc.promoDetails,
              builder: (context, snapshot) {
                return StreamHandler(
                  snapshot: snapshot,
                  loading: Center(child: CircularProgressIndicator()),
                  withError: (error) => Padding(
                    padding: EdgeInsets.all(16),
                    child: Center(
                      child: Text("Data not available",
                        textAlign: TextAlign.center,
                      )
                    )
                  ),
                  noData: Padding(
                    padding: EdgeInsets.all(16),
                    child: Center(
                      child: Text("Data not available",
                        textAlign: TextAlign.center,
                      )
                    )
                  ),
                  withData: (PromoDetails promoDetails) { 
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        SizedBox(height: 90),
                        Container(
                          height: MediaQuery.of(context).size.height * .25,
                          width: MediaQuery.of(context).size.width * 1,
                          decoration: BoxDecoration(
                            color: AppColors.blue2,
                            borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0))
                          ),
                          child: Container(
                            child: CachedNetworkImage(
                              imageUrl: promoDetails.promoImage,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => new Center (
                                child: CircularProgressIndicator(backgroundColor: AppColors.green,)
                              ),
                              errorWidget: (context, url, error) => new Container(
                                child:Center(
                                  child: Image.asset('assets/image/logo.png', fit: BoxFit.contain)
                                )
                              )
                            )
                          )
                        ),
                        SizedBox(height: 10.0),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal:10.0),
                          child: Text(promoDetails.promoName,
                            style: TextStyle(
                              fontSize: 18,
                              fontFamily: 'Quicksand',
                              color: AppColors.black,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal:10.0),
                          child: Text("Valid till " +promoDetails.endDate.substring(0,10),
                            style: TextStyle(
                              fontSize: 14,
                              fontFamily: 'Quicksand',
                              color: AppColors.black,
                            ),
                          ),
                        ),
                        Expanded(
                          child: ListView(
                            padding: EdgeInsets.all(0),
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
                                child: Html(
                                  data: ''+promoDetails.promoDescription+'',
                                  defaultTextStyle: TextStyle(
                                    color: AppColors.black,
                                    height: 1.5,
                                        fontSize: 13
                                    ),
                                    linkStyle: const TextStyle(
                                    decoration: TextDecoration.underline
                                  ),
                                  onLinkTap: (url) async{
                                    if (await canLaunch(url)) {
                                      await launch(url);
                                    } else {
                                      throw 'Could not launch $url';
                                    }
                                  },
                                  onImageTap: (src) {
                                    print(src);
                                  },
                                  customRender: (node, children) {
                                    if (node is dom.Element) {
                                      switch (node.localName) {
                                        case "custom_tag":
                                          return Column(children: children);
                                      }
                                    }
                                    return null;
                                  },
                                  customTextAlign: (dom.Node node) {
                                    if (node is dom.Element) {
                                      switch (node.localName) {
                                        case "p":
                                          return TextAlign.start;
                                        case "h":
                                          return TextAlign.start;
                                      }
                                    }
                                    return null;
                                  },
                                  customTextStyle: (dom.Node node, TextStyle baseStyle) {
                                    if (node is dom.Element) {
                                      switch (node.localName) {
                                        case "p":
                                          return baseStyle.merge(TextStyle(color: AppColors.black, height: 1.5, fontSize: 13));
                                        case "h":
                                          return baseStyle.merge(TextStyle(color: AppColors.black, height: 1.5, fontSize: 13));
                                      }
                                    }
                                    return baseStyle;
                                  },
                                ),
                              ),
                              SizedBox(height: 15.0),
                              linked == true
                                ? Container(
                                  margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
                                  height: 40,
                                  alignment: Alignment.bottomLeft,
                                  child: Material(
                                    elevation: 5.0,
                                    borderRadius: BorderRadius.circular(15.0),
                                    color: AppColors.blue,
                                    child: FlatButton.icon(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(15.0),
                                      ),
                                      label: Container(
                                        child: Text("  Share on Facebook", 
                                          style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                      ),
                                      icon: Image.asset("assets/icons/facebook.png"),
                                      onPressed: () async{
                                        String url = promoDetails.shareableImage;
                                        await SocialSharePlugin.shareToFeedFacebookLink(
                                          url: url,
                                          onSuccess: (_) {
                                            successMessage('Successfully shared on facebook', context);
                                            return;
                                          },
                                          onCancel: () {
                                            errorMessage('Sharing on facebook was cancelled by user', context);
                                            return;
                                          },
                                          onError: (error) {
                                            errorMessage(error.toString(), context);
                                            return;
                                          },
                                        );
                                      },
                                    ),
                                  )
                                )
                                : Container(
                                  height: 60.0,
                                  child: StreamBuilder<ScreenState>(
                                  stream: _authBloc.linkAccountState,
                                  builder: (context, snapshot) {
                                    return _buildLinkButton(snapshot);
                                  }
                                ),
                              ),
                              SizedBox(height: 50.0),
                            ],
                          ),
                        ),
                      ],
                    );
                  }
                );
              }
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.NOTIFICATION);},
                        elevation: 5.0,
                        child: Image.asset(notifImage, fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:5),
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.PROFILEPAGE);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:10)
                  ]
                )
              )
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            )   
          ]
        )
      )
    );
  }

  Widget _buildLinkButton(AsyncSnapshot<ScreenState> snapshot){
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
        height: 40,
        alignment: Alignment.bottomLeft,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.blue,
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: CircularProgressIndicator(backgroundColor: Colors.white,),
            onPressed: _onLink,
          ),
        )
      );
    } else {
     return Container(
        margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
        height: 40,
        alignment: Alignment.bottomLeft,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.blue,
          child: FlatButton.icon(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            label: Container(
              child: Text("  Connect to Facebook", 
                style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
            ),
            icon: Image.asset("assets/icons/facebook.png"),
            onPressed: _onLink,
          ),
        )
      );
    }
  }

  Future<void> _onLink() async {
    
    final facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.nativeOnly;  
    final facebookLoginResult = await facebookLogin.logIn(['email']);

    try{
      switch (facebookLoginResult.status) {
        case FacebookLoginStatus.loggedIn:
          var graphResponse = await http.get(
          'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.width(800).height(800)&access_token=${facebookLoginResult.accessToken.token}');
          var profile = json.decode(graphResponse.body);
          facebookUrl = profile['picture']['data']['url'];
          _showConfirmDialog(profile['email'], profile['name'], profile['picture']['data']['url']);
          break;
        case FacebookLoginStatus.cancelledByUser:
          errorMessage("Facebook connect is cancelled by the user!", context);
          break;
        case FacebookLoginStatus.error:
          errorMessage('Something went wrong with the connecting process.\n'
            'Here\'s the error Facebook gave us: ${facebookLoginResult.errorMessage}', context);
          break;
      }
    }catch(Exception){
      errorMessage(Exception.toString(), context);
    }
  }
  
  void _showConfirmDialog(String email, String username, String url) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .55,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .01),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(
                    height: 30,
                    width: 30,
                    child: RawMaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                        errorMessage("Linking Facebook have been cancelled!", context);
                      },
                      elevation: 5.0,
                      child: Image.asset("assets/icons/close.png", fit: BoxFit.cover,),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                  SizedBox(width: 15),
                  ],
                ),
                Container(
                  alignment: Alignment.center,
                  height: 120.0,
                  width: 120.0,
                  child: CircularProfileAvatar(
                    url, //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
                    radius: 100, // sets radius, default 50.0              
                    backgroundColor: Colors.transparent, // sets background color, default Colors.white
                    borderWidth: 2,  // sets border, default 0.0
                    borderColor: AppColors.green, // sets border color, default Colors.white
                    elevation: 5.0, // sets elevation (shadow of the profile picture), default value is 0.0
                    cacheImage: true, // allow widget to cache image against provided url
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  child: Text(
                    username,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  child: Text(
                    "Continue linking using \n"+ email,
                    style: TextStyle(
                      fontSize: 15,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  height:50,
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child:Text("Link", 
                        style: TextStyle(fontSize: 18.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){        
                        Navigator.pop(context);   
                        final LinkAccountRequest user = LinkAccountRequest(
                          username: _authBloc.session.value.accountId,
                          account: "facebook",
                          url: url,
                          email: email
                        );
                        _authBloc.linkAccount(user);
                      },
                    ),
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
              ]
            )
          ),
        );
      },
    );
  }
  
  void onStateChangedLinking(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        successMessage("Successfully linking facebook account.", context);
        savedtoSharedPref(facebookUrl);
        break;
      case States.ERROR:
        errorMessage(state.error.toString(), context);
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  getFacebookLinked() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();  
    try{
      setState(() {
        if(prefs.getBool('FacebookLink') == true){
          linked = true;
        }else{
          linked = false;
        }
      });
    }catch(Exception){}
  }
  
  savedtoSharedPref(String link) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try{
      setState(() {
        prefs.setBool("IsFacebookUrl", true);
        prefs.setBool("FacebookLink", true);
        prefs.setString("FacebookUrl", link);
        linked = true;
      });
    }catch(Exception){}
  }
}