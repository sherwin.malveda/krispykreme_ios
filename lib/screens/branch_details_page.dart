import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_phone_state/flutter_phone_state.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/blocs/store_bloc.dart';
import 'package:krispykreme/model/branch.dart';
import 'package:krispykreme/model/branches.dart';
import 'package:krispykreme/screens/notification_page.dart';
import 'package:krispykreme/screens/profile_page.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/input_field.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:permission_handler/permission_handler.dart' as perm;

class BranchDetailsPage extends StatefulWidget {

  @override
  _BranchDetailsPageState createState() => _BranchDetailsPageState();
}

class _BranchDetailsPageState extends State<BranchDetailsPage> {
  
  final TextEditingController _searchController = TextEditingController();

  bool cLickData = false;

  String branchname = "", branchAddress ="", branchTime = "", mobileNo = "";
  BitmapDescriptor pinLocationIcon;
  GoogleMapController mapController;

  Set<Marker> _markers = {};

  StoreBloc _storeBloc;
  
  double lat, long;
  double currentLocationlat, currentLocationlong;
  
  CameraPosition initialLocation;

  @override
  void initState() {
    super.initState();
    getCurrentLocation();
    checkPermission();
    _searchController.addListener(_onSearch);
    setCustomMapPin();
  }

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            lat == null 
              ? Container(child: Center(child: CircularProgressIndicator()))
              :Container(
                margin: EdgeInsets.fromLTRB(0, 90, 0, 0),
                child: StreamBuilder<Branches>(
                    stream: _storeBloc.branches,
                    builder: (context, snapshot) {
                      return StreamHandler(
                        snapshot: snapshot,
                        loading: Center(child: CircularProgressIndicator()),
                        withError: (error) => Padding(
                          padding: EdgeInsets.all(16),
                          child: Center(
                            child: Text("Map not available",
                              textAlign: TextAlign.center,
                            )
                          )
                        ),
                        noData: Padding(
                          padding: EdgeInsets.all(16),
                          child: Center(
                            child: Text("Map not available",
                              textAlign: TextAlign.center,
                            )
                          )
                        ),
                        withData: (Branches branch) {      
                          final List<Branch> _filteredBranch = _filterBranches(branch); 
                            if(_filteredBranch.length !=0){
                              if(_searchController.text != ""){
                                lat = _filteredBranch[0].latitude;
                                long = _filteredBranch[0].longitude;
                              }
                              initialLocation = new CameraPosition(
                                target: LatLng(lat,long),
                                zoom: 12,
                              );
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: GoogleMap(
                                    markers: _markers,
                                    mapType: MapType.normal,
                                    initialCameraPosition: initialLocation,
                                    onMapCreated: (GoogleMapController controller) {
                                      mapController = controller;
                                      setState(() {
                                        if(currentLocationlat != null){
                                          _markers.add(Marker(
                                            markerId: MarkerId("Your Location"),
                                            position: LatLng(currentLocationlat, currentLocationlong),
                                          ));
                                        }
                                        for(int i = 0 ; i< _filteredBranch.length; i++){
                                          _markers.add(
                                              Marker(
                                                markerId: MarkerId(_filteredBranch[i].branchName),
                                                position: LatLng(_filteredBranch[i].latitude, _filteredBranch[i].longitude),
                                                icon: pinLocationIcon,
                                                onTap: () {
                                                  setState(() {
                                                    cLickData = true;
                                                    branchname = _filteredBranch[i].branchName;
                                                    branchAddress = _filteredBranch[i].branchAddress;
                                                    branchTime = _filteredBranch[i].startDate + " - " + _filteredBranch[i].endDate;
                                                    mobileNo = _filteredBranch[i].mobileno;
                                                  });
                                                }
                                              )
                                          );
                                        }
                                      });
                                    },              
                                  ),
                                ),
                                cLickData == true
                                  ? Container(
                                  height: MediaQuery.of(context).size.height * .27,
                                  width: MediaQuery.of(context).size.width * 1,
                                  color: Colors.white,
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          branchname,
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Quicksand',
                                            color: AppColors.green
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          SizedBox(width: 5,),
                                          Image.asset("assets/icons/loc.png"),
                                          SizedBox(width: 10,),
                                          Expanded(
                                            child: Text(
                                              branchAddress,
                                              maxLines: 2,
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontFamily: 'Quicksand',
                                                color: AppColors.black
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(height: 5,),
                                      Row(
                                        children: <Widget>[
                                          SizedBox(width: 3,),
                                          Image.asset("assets/icons/time.png"),
                                          SizedBox(width: 7,),
                                          Expanded(
                                            child: Text(
                                              branchTime,
                                              style: TextStyle(
                                                fontSize: 12,
                                                fontFamily: 'Quicksand',
                                                color: AppColors.black
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(height: 20),
                                      Row(
                                        children: <Widget>[
                                          SizedBox(width: 10,),
                                          Expanded(
                                            child: Material(
                                              elevation: 5.0,
                                              borderRadius: BorderRadius.circular(10.0),
                                              color: AppColors.white,
                                              child: MaterialButton(
                                                height: 50,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                  side: BorderSide(
                                                    color: AppColors.green, //Color of the border
                                                    style: BorderStyle.solid, //Style of the border
                                                    width: 2, //width of the border
                                                  ),
                                                ),
                                                child:Row(
                                                  children: <Widget>[
                                                    SizedBox(width: 5,),
                                                    Image.asset("assets/icons/call.png"),
                                                    SizedBox(width: 10,),
                                                    Text("Call Hotline", 
                                                      style: TextStyle(fontSize: 15.0, color: AppColors.green, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                                  ],
                                                ),
                                                onPressed: () {initiateCall("888-79000");}
                                              )
                                            ),
                                          ),
                                          SizedBox(width: 10,),
                                          Expanded(
                                            child: Material(
                                              elevation: 5.0,
                                              borderRadius: BorderRadius.circular(10.0),
                                              color: AppColors.green,
                                              child: MaterialButton(
                                                height: 50,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(10.0),
                                                ),
                                                child:Row(
                                                  children: <Widget>[
                                                    SizedBox(width: 5,),
                                                    Image.asset("assets/icons/order.png"),
                                                    SizedBox(width: 10,),
                                                    Text("Order Now", 
                                                      style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                                  ],
                                                ),
                                                onPressed: () {goToWebsite();}
                                              )
                                            ),
                                          ),
                                          SizedBox(width: 10,),
                                        ],
                                      )
                                    ],
                                  ),
                                )
                                : Container()
                              ],
                            );
                          }else{
                            return Padding(
                              padding: EdgeInsets.all(16),
                              child: Center(
                                child: Text("Search Branch does not exist",
                                  textAlign: TextAlign.center,
                                )
                              )
                            );
                          }
                        },
                      );
                    }
                  )
              ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context, MaterialPageRoute(
                              builder: (_) {
                                return NotificationPage();
                              }
                            )
                          );
                        },
                        elevation: 5.0,
                        child: Image.asset("assets/icons/notif.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:5),
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context, MaterialPageRoute(
                              builder: (_) {
                                return ProfilePage();
                              }
                            )
                          );
                        },
                        elevation: 5.0,
                        child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:10)
                  ]
                )
              )
            ),
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            ),
            Positioned(
              top: 140,
              left: 0,
              right: 0,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child :Stack(
                  children: [
                    Container(
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        border: Border.all(
                          color: AppColors.green,
                          width: 1.0,
                        ) 
                      ),
                      child:TextFormField(
                        style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green),
                        controller: _searchController,
                        cursorColor: AppColors.green,
                        onFieldSubmitted: (value){
                          setState(() {
                            cLickData = true;
                          });
                        },
                        decoration: InputDecoration(
                          counterText: '',
                          hintText: "Search Branch",
                          hintStyle: TextStyle(color: AppColors.green),
                          focusColor: AppColors.green,
                          hoverColor: AppColors.green,
                          fillColor: AppColors.green,
                          contentPadding: EdgeInsets.fromLTRB(20, 5, 50, 5),
                          disabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide.none),
                          border: OutlineInputBorder(borderSide: BorderSide.none),
                        ),
                      )
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      bottom: 0,
                      child: Container(
                        width: 50,
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Icon(Icons.search, color: AppColors.green)
                      )
                    )
                  ],
                ),
              )
            )
          ],
        ),
      ),
    );
  }

  List<Branch> _filterBranches(Branches branches) {
    final List<Branch> branchesCopy = List<Branch>.from(branches.branches)
      .where((branch) =>
      branch.branchName.toLowerCase().contains(
        _searchController.text.toLowerCase()
      )).toList();
    return branchesCopy;
  }

  void _onSearch() {
    setState(() {
      cLickData = false;
      if(_searchController.text != "")
        mapController.animateCamera(
          CameraUpdate.newLatLngZoom(
            LatLng(lat,long),
            12.0
          ),
        );
    });
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
    ImageConfiguration(devicePixelRatio: 2.5),
    'assets/image/location.png');
  }

  goToWebsite() async {
    const url = 'https://now.krispykreme.com.ph/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  initiateCall(String _phoneNumber) {
    if (_phoneNumber?.isNotEmpty == true) {
      setState(() {
        FlutterPhoneState.startPhoneCall(_phoneNumber);
      });
    }
    else{
      errorMessage("Unable to make a call. Please try again later!", context);
    }
  }

  checkPermission() async{
    
    Location location = new Location();    
    bool _serviceEnabled;
    
    if (await perm.Permission.location.request().isGranted) {
      if (await perm.Permission.locationWhenInUse.serviceStatus.isEnabled) {
      }else{
        _serviceEnabled = await location.serviceEnabled();
        if (!_serviceEnabled) {
          await location.requestService().then((value) => value);
        }else{
          errorMessage("Please turn on location to use this feature!", context);
        }
      }
    }else{
      errorMessage("Please enable location permission in the \nApplication Setting >Krispy Kreme>Permission and open location to use this feature!", context);
    }
  }

  Future<void> getCurrentLocation() async {
    Location location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();
    print(_locationData);
    if(_locationData != null){
      setState(() {
        lat = _locationData.latitude;
        long = _locationData.longitude;
        currentLocationlat = _locationData.latitude;
        currentLocationlong = _locationData.longitude;
      });
      
    }
  }
}