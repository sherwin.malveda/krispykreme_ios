import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
class AboutUsPage extends StatefulWidget {
  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: AppColors.green,
        title: Text(
          "ABOUT US",
          style: TextStyle(fontFamily: 'Quicksand'),
        ),
        centerTitle: true,
      ),
      body: Container(
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(30.0),
                    child: Text(
                      "Krispy Kreme & Prints is a ready-to-go wear women's apparel brand with the "+
                      "modern wowen in mind. Its distinct yet stylish pieces has made Krispy Kreme & "+
                      "Prints one of the top local brand in the Philippines.\n\n"+
                      "The brand specializes in casual tops, bottoms, dresses, playsuits and"+
                      " accessories for that fashion forward sophisticated woman.\n\n"+
                      "Today, Krispy Kreme & Prints is one of the leading fashion brands in the country. "+
                      "But its humble beginnings trace back to a young college girl's dream of starting "+
                      "her own clothing line.\n\n"+
                      "In 1994, the first Krispy Kreme & Prints boutique was born at Shoppersville Plus in "+
                      "Greenhills. An 11-square meter stall manned by two staffs. It was then that Krispy Kreme "+
                      "& Prints began its trade mark creation of timeless pieces that never went out of style.\n\n"+
                      "In the years that followed, the Krispy Kreme & Prints vision of creating elegant, quality women's"+
                      " apparel flourished and business continued to expand.\n\n",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        color: AppColors.green,
                        fontSize: 15.0
                      ),
                        textAlign: TextAlign.center
                    ),
                  )
                ]
              )
            ),
    );
  }
}
