import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/screens/branch_details_page.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart' as perm;

class BranchesPage extends StatefulWidget {
  @override
  _BranchesPageState createState() => _BranchesPageState();
}

class _BranchesPageState extends State<BranchesPage> {

  @override
  void initState() {
    super.initState();
    checkPermission();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.fromLTRB(0, 90, 0, 0),
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).size.height * .1),
            Container(
              child: Text(
                "Krispy Kreme Store Locator",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Quicksand',
                  color: AppColors.green
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * .05),
            Container(
              height: 90.0,
              child: Image.asset("assets/image/location.png", fit: BoxFit.contain,)
            ), 
            SizedBox(height: MediaQuery.of(context).size.height * .05),
            Container(
              child: Text(
                "Let's make your day sweeter!",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Quicksand',
                  color: AppColors.green
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * .03),
            Container(
              child: Text(
                "Turn on your location services\nand get to see the Krispy Kreme Store\nnearest you!",
                style: TextStyle(
                  fontSize: 12,
                  fontFamily: 'Quicksand',
                  color: AppColors.black
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * .05),
            Container(
              margin: EdgeInsets.symmetric(horizontal:80, vertical: 5),
              child: Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(15.0),
                color: AppColors.green,
                child: MaterialButton(
                  height: 50,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child:Text("Turn On Location", 
                    style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                  onPressed: () {
                    Navigator.push(
                      context, MaterialPageRoute(
                        builder: (_) {
                          return BranchDetailsPage();
                        }
                      )
                    );
                  }
                )
               ),
             )
           ],
        )
              
      ),
    );
  }

  checkPermission() async{
    
    Location location = new Location();    
    bool _serviceEnabled;
    
    if (await perm.Permission.location.request().isGranted) {
      if (await perm.Permission.locationWhenInUse.serviceStatus.isEnabled) {
        Navigator.push(
          context, MaterialPageRoute(
            builder: (_) {
              return BranchDetailsPage();
            }
          )
        );
      }else{
        _serviceEnabled = await location.serviceEnabled();
        if (!_serviceEnabled) {
          await location.requestService().then((value) => value);
        }
      }
    }
  }
}
