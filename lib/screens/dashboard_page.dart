import 'dart:async';
import 'dart:convert';
import 'package:after_layout/after_layout.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/card_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/blocs/store_bloc.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/controllers/navigation_controller.dart';
import 'package:krispykreme/db/insert.dart';
import 'package:krispykreme/master_state.dart';
import 'package:krispykreme/model/hotlight.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/model/session.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/branches_page.dart';
import 'package:krispykreme/screens/card_page.dart';
import 'package:krispykreme/screens/home_page.dart';
import 'package:krispykreme/screens/notification_page.dart';
import 'package:krispykreme/screens/vouchers_page.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends MasterState<DashboardPage>
    with SingleTickerProviderStateMixin, AfterLayoutMixin<DashboardPage> {
  
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final NavigationController _navigationController = NavigationController();
  StreamSubscription _sessionSubscriber;

  int _currentIndex = 0;
  TabController _tabController;

  CardBloc _cardBloc;
  AuthBloc _authBloc;
  StoreBloc _storeBloc;
  bool hotlight = false;
  
  @override
  void didChangeDependencies() {
    _initListeners();
    _initBlocs();
    super.didChangeDependencies();
  }


  @override
  void dispose() {
    _sessionSubscriber.cancel();
    _navigationController.removeListener(_onNavigated);
    super.dispose();
  }

  void _listenToAuth() {
    _sessionSubscriber = _authBloc.session.listen(_onSession);
  }

  void _onSession(Session session) {
    try{
    if (session != null)
      _cardBloc.getAccountDetails(session.accountId);
      _cardBloc.getCardDetails(_authBloc.session.value.accountId);
      _cardBloc.getHistory(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _cardBloc.getVouchers(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _cardBloc.getCities();
      _storeBloc.getNotification(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getNewsLetter(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getPromos(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getPromosVideos(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getBranches(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: INFINITE_ROW_LIST,
        page: 1)
      );
    }catch(Exception){}
  }

  void _initBlocs() {
    _cardBloc = MasterProvider.card(context);
    _storeBloc = MasterProvider.store(context);
    _authBloc = MasterProvider.auth(context);
  }

  @override
  void initState() {
    init();
    
     flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(message);
        setState(() {
          notifImage = "assets/icons/newnotif.png";
        });
        final notif = message['notification'];
        _showNotification(notif['title'],notif['body']);
        refreshNotif();
        checkNotif(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        setState(() {
          notifImage = "assets/icons/newnotif.png";
        });
        onSelectNotification(message.toString());
        refreshNotif();
        checkNotif(message);
      },
      onResume: (Map<String, dynamic> message) async  {
        setState(() {
          notifImage = "assets/icons/newnotif.png";
        });
        onSelectNotification(message.toString());
        refreshNotif();
        checkNotif(message);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true)
    );

    super.initState();
  }

  void _initListeners() {
    _navigationController.addListener(_onNavigated);
  }

  void _onNavigated() {
    setState(() {
      _currentIndex = _navigationController.mainIndex;
      _tabController.index = _navigationController.mainIndex;
    });
  }

  void init() {
    _tabController = TabController(length: 4, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _currentIndex = _tabController.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      body: Stack(
        children: <Widget>[
          TabBarView(
            controller: _tabController,
            children: [
              HomePage(navigationController: _navigationController),
              CardPage(),
              VouchersPage(),
              BranchesPage()
            ]
          ),
          Positioned(
            top: 0,
            child: Container(
              height: 90,
              width: MediaQuery.of(context).size.width * 1,
              color: AppColors.green,
              padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 40,
                    width: 40,
                    child: RawMaterialButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed(Routes.NOTIFICATION);
                      },
                      elevation: 5.0,
                      child: Image.asset(notifImage, fit: BoxFit.cover,),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                  SizedBox(width:5),
                  SizedBox(
                    height: 40,
                    width: 40,
                    child: RawMaterialButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed(Routes.PROFILEPAGE);
                      },
                      elevation: 5.0,
                      child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                  SizedBox(width:10)
                ]
              )
            )
          ),
          Positioned(
            top: 65,
            left: 0,
            right: 0,
            child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onNavigate,
        currentIndex: _currentIndex,
        unselectedItemColor: Colors.white38,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: AppColors.green,
        backgroundColor: AppColors.white,
        selectedIconTheme: IconThemeData(size: 25),
        unselectedIconTheme: IconThemeData(size: 17,color: Colors.black38),
        selectedLabelStyle: TextStyle( fontWeight: FontWeight.bold, fontSize: 11, color: AppColors.green),
        unselectedLabelStyle: TextStyle( fontSize: 9, color: Colors.black38),
        items: [
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icons/home.png")),
            title: Text("Home", 
              style: TextStyle(color: AppColors.green)
            )
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icons/card.png")),
            title: Text("OG Card", 
              style: TextStyle(color: AppColors.green)
            )
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icons/voucher.png")),
            title: Text("E-Vouchers", 
              style: TextStyle(color: AppColors.green)
            )
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/icons/store.png")),
            title: Text("Store Location", 
              style: TextStyle(color: AppColors.green)
            )
          )  
        ]
      )
    );
  }

  void _onNavigate(int position) {
    setState(() {
      _currentIndex = position;
      _tabController.index = position;
    });
  }

  Future<void> _showNotification(String title, String body) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, title, body, platformChannelSpecifics,
        payload: body);
  }

  Future<void> onSelectNotification(String payload) async {
    if (payload != null) {
      print(payload);
    }
    if(hotlight == false){
      Navigator.push(
        context, MaterialPageRoute(
          builder: (_) {
            return NotificationPage();
          }
        )
      );
    }else{
      hotlight = false;
      setState(() {    
        _onNavigate(0);
      });
    }
  }

  Future<void> onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: title != null ? Text(title) : null,
        content: body != null ? Text(body) : null,
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop(); 
              if(hotlight == false){
                Navigator.push(
                  context, MaterialPageRoute(
                    builder: (_) {
                      return NotificationPage();
                    }
                  )
                );
              }else{
                hotlight = false;
                setState(() {    
                  _onNavigate(0);
                });
              }
            },
          )
        ],
      ),
    );
  }

  Future<void> checkNotif(Map<String, dynamic> message) async {
    try{
      List<Hotlight> hl = new List<Hotlight>();
      jsonDecode(message['data']['hotlight'].toString()).forEach((v) {
        hl.add(new Hotlight.fromJson(v));
      });
      if(hl.length > 0){
        notifImage = "assets/icons/notif.png";
        hotlight = true;
        dbHelper.delete("Hotlight");
        dbHelper.delete("HotlightBranches");
        Insert.insertHotLight(hl[0].expiration);
        for(int i = 0; i< hl.length; i++){
          Insert.inserHotlightBranch(hl[i]);
        }
      }
    }catch(Exception){}
  }
  
  refreshNotif(){
    _storeBloc.getNotification(ListRequest(
      accountId: _authBloc.session.value.accountId,
      rows: MAX_ROW_LIST,
      page: 1)
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    print("afterFirstLayout");
    _listenToAuth();
  }
}