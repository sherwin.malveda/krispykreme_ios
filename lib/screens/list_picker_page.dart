import 'package:flutter/material.dart';
import 'package:krispykreme/model/item.dart';
import 'package:krispykreme/widgets/list_picker.dart';

class ListPickerPage extends StatelessWidget {
  
  final String title;
  final List<Item> items;

  const ListPickerPage({Key key, @required this.title, this.items})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(title, style: TextStyle(fontFamily: 'Quicksand',color: Colors.black54)),
        elevation: 0.0,
        brightness: Brightness.light,
        leading: IconButton(
          icon: Icon(Icons.chevron_left, size: 40.0, color: Colors.grey),
          onPressed: () => Navigator.pop(context)),
        ),
      body: ListPicker(items: items)
    );
  }
}
