import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/card_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/voucher.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/voucher_row.dart';

class VoucherDetailsPage extends StatefulWidget {
  final int voucherId;
  const VoucherDetailsPage({Key key, @required this.voucherId}) : super(key: key);
  @override
  _VoucherDetailsPageState createState() => _VoucherDetailsPageState();
}

class _VoucherDetailsPageState extends State<VoucherDetailsPage>{

  CardBloc _cardBloc;
  AuthBloc _authBloc;

  @override
  void didChangeDependencies() {
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    _cardBloc.getVoucherDetails(_authBloc.session.value.accountId, widget.voucherId);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            StreamBuilder<Voucher>(
              stream: _cardBloc.voucherDetails,
              builder: (context, snapshot) {
                return StreamHandler(
                  snapshot: snapshot,
                  loading: Center(child: CircularProgressIndicator()),
                  withError: (error) => Padding(
                    padding: EdgeInsets.all(16),
                    child: Center(
                      child: Text("Data not available",
                        textAlign: TextAlign.center,
                      )
                    )
                  ),
                  noData: Padding(
                    padding: EdgeInsets.all(16),
                    child: Center(
                      child: Text("Data not available",
                        textAlign: TextAlign.center,
                      )
                    )
                  ),
                  withData: (Voucher voucher) { 
                    return Container(
                          padding: EdgeInsets.fromLTRB(10, 190, 10, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Container(
                                height: MediaQuery.of(context).size.height * .20,
                                width: MediaQuery.of(context).size.width * 1,
                                decoration: BoxDecoration(
                                  color: AppColors.blue2,
                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0))
                                ),
                                child: Stack(
                                  children: [
                                    Container(
                                      height: MediaQuery.of(context).size.height * .20,
                                      width: MediaQuery.of(context).size.width * 1,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0)),
                                        child:Image.asset("assets/image/voucher.png", fit: BoxFit.cover)
                                      )
                                    ),
                                    Positioned(
                                      left:  MediaQuery.of(context).size.width * .135,
                                      top: MediaQuery.of(context).size.height * .03,
                                      child:Container(
                                        height: MediaQuery.of(context).size.height * .133,
                                        width: MediaQuery.of(context).size.width * .276,
                                        child:voucherImage(voucher.voucherType),
                                      )
                                    )
                                  ],
                                )
                              ),
                              SizedBox(height: 10),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal:10.0),
                                child: Text(voucher.description,
                                  style: TextStyle(
                                    fontSize: 17,
                                    fontFamily: 'Quicksand',
                                    color: AppColors.black,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              SizedBox(height: 15.0),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal:10.0),
                                child: Text("Mechanics",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: 'Quicksand',
                                    color: AppColors.black,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              SizedBox(height: 15.0),
                              Expanded(
                                child :ListView(
                                  shrinkWrap: true,
                                  padding: EdgeInsets.zero,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.symmetric(horizontal:10.0),
                                      child: Text(voucher.campainDescription,
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Quicksand',
                                          color: AppColors.black,
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 15.0),
                                    Container(
                                      height: 40,
                                      margin: EdgeInsets.symmetric(horizontal:10, vertical: 5),
                                      alignment: Alignment.bottomLeft,
                                      child: Material(
                                        elevation: 5.0,
                                        borderRadius: BorderRadius.circular(10.0),
                                        color: AppColors.green,
                                        child: MaterialButton(
                                          height: 40,
                                          shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
                                          child:Text("REDEEM E-VOUCHER", 
                                            style: TextStyle(fontSize: 12.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                          onPressed: () {
                                            _showConfirmDialog(voucher.voucherCode, voucher.expiration);
                                          },
                                        ),
                                      )
                                    ),
                                    SizedBox(height: 20.0),
                                  ],
                                )
                              ),
                            ],
                          )
                        );
                      }
                    );
                  }
                ),
                Positioned(
                  top: 140,
                  left: 0,
                  right: 0,
                  child: Center(
                    child: Container(
                      child: Text("E-VOUCHERS",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: AppColors.green
                        ),
                      )
                    )
                  )
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    height: 90,
                    width: MediaQuery.of(context).size.width * 1,
                    color: AppColors.green,
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 40,
                          width: 40,
                          child: RawMaterialButton(
                            onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.NOTIFICATION);},
                            elevation: 5.0,
                            child: Image.asset(notifImage, fit: BoxFit.cover,),
                            padding: EdgeInsets.all(0.0),
                            shape: CircleBorder(),
                          )
                        ),
                        SizedBox(width:5),
                        SizedBox(
                          height: 40,
                          width: 40,
                          child: RawMaterialButton(
                            onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.PROFILEPAGE);},
                            elevation: 5.0,
                            child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                              padding: EdgeInsets.all(0.0),
                            shape: CircleBorder(),
                          )
                        ),
                        SizedBox(width:10)
                      ]
                    )
                  )
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    height: 90,
                    width: MediaQuery.of(context).size.width * .5,
                    color: AppColors.green,
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(width:15),
                        SizedBox(
                          height: 30,
                          width: 30,
                          child: RawMaterialButton(
                            onPressed: () {Navigator.pop(context);},
                            elevation: 5.0,
                            child: Image.asset("assets/icons/back.png"),
                            padding: EdgeInsets.all(0.0),
                            shape: CircleBorder(),
                          )
                        ),
                      ]
                    )
                  )
                ),
                Positioned(
                  top: 65,
                  left: 0,
                  right: 0,
                  child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
                )
              ],
        )
      )
    );
  }

  Widget voucherImage(int type){
    if(type == 1){
      return Image.asset("assets/image/donut.png", fit: BoxFit.contain);
    }else if(type == 2){
      return Image.asset("assets/image/boxes.jpg", fit: BoxFit.contain);
    } else if(type == 3){
      return Image.asset("assets/image/donuts.jpg", fit: BoxFit.contain);
    }else{
      return Image.asset("assets/image/logo.png", fit: BoxFit.contain);
    }
  }

  void _showConfirmDialog(String voucherCode, String expiryDate) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .35,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Text("Are you sure you want to redeem\nthis voucher?",
                  style: TextStyle(
                  fontSize: 14.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .05),
                Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 40,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
                      child:Text("Yes", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){
                        Navigator.pop(context);
                        _showVoucher(voucherCode, expiryDate);
                      },
                    ),
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  color: Colors.transparent,
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                  height: 40,
                  child: Material(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.transparent,
                    child: MaterialButton(
                      child: Container(
                        decoration: new BoxDecoration(
                          border: Border(bottom: BorderSide(color: AppColors.green))
                        ),
                        child: Text("Maybe Later",
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            color: AppColors.green,
                            fontSize: 15.0
                          )
                        ),
                      ),
                      onPressed: (){Navigator.pop(context);},
                    ),
                  )
                ),
              ]
            )
          ),
        );
      },
    );
  }

  void _showVoucher(String voucherCode, String expiryDate) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .7,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Text("Screenshot then present\nthis e-voucher upon checkout!\n(Expires in "+expiryDate+" )",
                  style: TextStyle(
                  fontSize: 14.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10),
                Container(
                  margin: EdgeInsets.symmetric(horizontal:20.0),
                  child: VoucherRow(
                    code: voucherCode
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 40,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
                      child:Text("Ok", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){Navigator.pop(context);},
                    ),
                  )
                ),
                SizedBox(height: 10),
              ]
            )
          ),
        );
      },
    );
  }
}