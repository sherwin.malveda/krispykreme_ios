import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/routes.dart';

class TermsAndConditionPage extends StatefulWidget {
  @override
  _TermsAndConditionPageState createState() => _TermsAndConditionPageState();
}

class _TermsAndConditionPageState extends State<TermsAndConditionPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            Container(
              padding: EdgeInsets.fromLTRB(0, 190, 0, 0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage('assets/image/bg.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 10.0),
                      child: Text(
                        "This loyalty program, which has been designed for use with mobile devices (collectively, the 'Mobile Service'),"+
                        " is controlled by and/or maintained by The Real American Doughnut Company Inc. and/or one or more of its"+
                        " subsidiaries or affiliates (individually and collectively, 'The Real American Doughnut Company Inc.,' 'we,' 'us,'"+
                        " or 'our'). Read these Terms and Conditions before using our Mobile Service. By using our Mobile Service,"+
                        " participants ('Participant', 'you,' 'your') agree to be bound by each of the Terms and Conditions set forth"+
                        " below and any amendments thereto ('Terms and Conditions'). Review these Terms and Conditions from time"+
                        " to time to ensure that you have read and agree with the current policy, as it may be amended without notice."+
                        " The date of our Terms and Conditions is set forth at the top of the document. Any changes to these Terms and"+
                        " Conditions will be effective immediately upon the posting of the revised Terms and Conditions for this Mobile"+
                        " Service. Your use of certain features of our Mobile Service may be subject to additional Terms and Conditions."+
                        " By using these features, you also agree to be bound by such additional Terms and Conditions."+
                        " \n\nThe Real American Doughnut Company Inc. may terminate, change, suspend or discontinue any aspect of the"+
                        " Mobile Service, including the availability of any features of the Mobile Service, at any time. The Real American"+
                        " Doughnut Company Inc. may also impose limits on certain features and services or restrict your access to parts"+
                        " or to the entire website without notice or liability. The Real American Doughnut Company Inc. may terminate"+
                        " the authorization, rights and license given herein.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "THE REAL AMERICAN DOUGHNUT COMPANY INC. ONLINE SERVICES",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "The Real American Doughnut Company Inc. reserves the right, in its sole discretion, without limitations, conditions and/or qualifications, and without need of prior notification, to limit, change, modify or cancel any transaction, and/or to refuse any service to any customer for any reason attributable to the unavailability of the items ordered, or other circumstances and conditions beyond the control of The Real American Doughnut Company Inc. which would render impossible or delay the fulfillment of its obligations; Provided that such events shall not be caused by any negligence on the part of The Real American Doughnut Company Inc.. Notwithstanding the receipt of any order confirmation, whether via electronic mail, facsimile, phone call or otherwise, it is understood by you that the transaction of your order by The Real American Doughnut Company Inc. and receipt of the payment corresponding to the same, shall constitute the sole act of final acceptance by The Real American Doughnut Company Inc. of such order. Verification of any information may be required prior to the acceptance of any transaction. "+
                        "\n\nThe Real American Doughnut Company Inc. will not be liable for incorrect information and/or non-existent recipient during transaction."+
                        "\n\nIf you wish to cancel activation and registration, you may email onlinesupport@krispykreme.com.ph. The Real American Doughnut Company Inc. however, reserves the right to refuse cancellation of registration and refund of payment given that information has been processed. If you are not satisfied with our program, you may direct your concerns to the said contact details given below.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "RESTRICTION OF LIABILITY",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "The Real American Doughnut Company Inc. will not be liable for damages of any kind or nature or injury whatsoever caused by, including but not limited to, any failure of performance, error, omission, interruption, defect, delay in operation of transmission, computer virus, or line failure. The Real American Doughnut Company Inc. will not be liable for any damages or injury, including but not limited to, special or consequential damages that result from the use of, or the inability to use, the materials in this mobile service, even if there is negligence or The Real American Doughnut Company Inc. or an authorized The Real American Doughnut Company Inc. representative has been advised of the possibility of such damages, or both. The Real American Doughnut Company Inc.'s total liability to you for all losses, damages, and causes of action (in contract, tort (including without limitation, negligence), or otherwise) will not be greater than the amount you paid to access this mobile service, should any such liability be established.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "SUBMISSIONS",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "All remarks, suggestions, ideas, graphics, or other information communicated to The Real American Doughnut Company Inc. through this mobile service (together, the 'Submission') will forever be the property of The Real American Doughnut Company Inc. The Real American Doughnut Company Inc. will not be required to treat any Submission as confidential, and will not be liable for any ideas for its business (including without limitation, product, or advertising ideas) and will not incur any liability as a result of any similarities that may appear in future The Real American Doughnut Company Inc. operations. Without limitation, The Real American Doughnut Company Inc. will have exclusive ownership of all present and future existing rights to the Submission of every kind and nature everywhere. Except as noted below in this paragraph, The Real American Doughnut Company Inc. will be entitled to use the Submission for any commercial or other purpose whatsoever without compensation to you or any other person sending the Submission. Personally identifiable information that may be received at this mobile service is provided voluntarily by a visitor to this mobile service. This information is for internal purposes only and is not sold or otherwise transferred to third parties of The Real American Doughnut Company Inc. or to other entities who are not involved in the operation of this mobile service. Information submitted via a number of areas in this mobile service, for instance, Customer Feedback, is not retained. Therefore, the above right to use Submissions is subject to this limited use of this information and excludes non-retained information. You acknowledge that you are responsible for whatever material you submit, and you, not The Real American Doughnut Company Inc. have full responsibility for the message, including its legality, reliability, appropriateness, originality, and copyright.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "JURISDICTION",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "Except as described otherwise, all materials in the The Real American Doughnut Company Inc. site are made available only to provide information about The Real American Doughnut Company Inc.. The Real American Doughnut Company Inc. controls and operates this site from its head office in Makati Metro Manila, Philippines and makes no representation that these materials are appropriate or available for use in other locations. If you use this site from other locations you are responsible for compliance with applicable local laws. Some software from this site may be subject to export controls imposed by the proper jurisdictions and may not be downloaded or otherwise exported or re-exported: (a) into (or to a national or resident of) any country to which the U.S. has placed an embargo, including without limitation, Cuba, Iran, Iraq, Libya, North Korea, Syria, or Yugoslavia; (b) to everyone on the US Treasury Department's Specially Designated Nationals list, or (c) the US Commerce Department's Table of Denial Orders. If you download or use the Software, you represent and warrant that you are not located in, or under the control of, or a national or any such country or on any such list."+
                        "\n\nWe make no representations that the Content on our Mobile Service is appropriate or available for use in locations outside of the Philippines. Those who choose to access our Mobile Service from locations outside the Philippines do so at their own initiative and are responsible for complying with all local laws and regulations regarding online conduct and acceptable content.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "DISCLAIMER",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "OUR MOBILE SERVICE AND ALL CONTENT AND SERVICES INCLUDED ON OR AVAILABLE THROUGH OUR MOBILE SERVICE ARE PROVIDED 'AS IS' AND 'AS AVAILABLE' FOR YOUR USE. YOU AGREE THAT YOUR USE OF OUR MOBILE SERVICE IS AT YOUR SOLE RISK. THE REAL AMERICAN DOUGHNUT COMPANY INC. AND ITS SUBSIDIARIES, PARTNERS, AGENTS, AFFILIATES, LICENSORS AND ADVERTISERS EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. ALTHOUGH WE INTEND THAT THE CONTENT OF OUR MOBILE SERVICE BE ACCURATE, COMPLETE, AND CURRENT, WE MAKE NO WARRANTY: (I) THAT USE OF OUR MOBILE SERVICE OR ANY CONTENT HEREIN (INCLUDING ANY SOFTWARE) WILL BE UNINTERRUPTED, TIMELY, ACCURATE, SECURE OR ERROR FREE; (II) THAT OUR MOBILE SERVICE WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION; (III) THAT ANY ERRORS ON OUR MOBILE SERVICE WILL BE CORRECTED; OR (IV) THAT THE SERVERS ON WHICH OUR MOBILE SERVICE AND CONTENT ARE AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS"+
                        "\n\nThe Real American Doughnut Company Inc. is an equal opportunity employer committed to a diverse workforce. The Real American Doughnut Company Inc. hire their own employees and establish their own Terms and Conditions of employment, which may differ from those described.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "LIMITATION OF LIABILITY",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "UNDER NO CIRCUMSTANCES SHALL THE REAL AMERICAN DOUGHNUT COMPANY INC., OR ITS SUBSIDIARIES, PARTNERS, AGENTS, AFFILIATES, LICENSORS AND ADVERTISERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR EXEMPLARY DAMAGES (EVEN IF SUCH PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), INCLUDING, WITHOUT LIMITATION, ANY LOST PROFITS, REVENUE, OR BUSINESS INTERRUPTION, OR LOSS OF COMPUTER PROGRAMS OR INFORMATION ARISING FROM (I) YOUR USE OF OR INABILITY TO USE OUR MOBILE SERVICE, OR (II) CONTENT AND/OR SERVICES INCLUDED ON OR AVAILABLE THROUGH OUR MOBILE SERVICE."+
                        "\n\nThe Real American Doughnut Company Inc. will not be liable for damages of any kind or nature or injury whatsoever caused by, including but not limited to, any failure of performance, error, omission, interruption, defect, delay in operation of transmission, computer virus, or line failure. The Real American Doughnut Company Inc. will not be liable for any damages or injury, including but not limited to, special or consequential damages that result from the use of, or the inability to use, the materials in this mobile service, even if there is negligence or The Real American Doughnut Company Inc. or an authorized The Real American Doughnut Company Inc. representative has been advised of the possibility of such damages, or both. The Real American Doughnut Company Inc.’ total liability to you for all losses, damages, and causes of action (in contract, tort (including without limitation, negligence), or otherwise) will not be greater than the amount you paid to access this mobile service, should any such liability be established.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "TERMINATION",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "The Real American Doughnut Company Inc. or you may terminate this agreement at any time. You may terminate this agreement by destroying: (a) all materials obtained from all The Real American Doughnut Company Inc. sites, and (b) all related documentation and all copies and installations (together, the 'Materials'). The Real American Doughnut Company Inc. may terminate this agreement immediately without notice if, in its sole judgment, you breach any term or condition of this agreement. Upon termination, you must destroy all materials.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "MISCELLANEOUS",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "This is the entire agreement between the parties relating to the use of this site. The Real American Doughnut Company Inc. can revise these Terms and Conditions at any time by updating this posting. The Real American Doughnut Company Inc. products and services are available in many parts of the world. However, the The Real American Doughnut Company Inc. site may describe products and services that are not available worldwide. This site may be linked to other sites that are not maintained by The Real American Doughnut Company Inc.. The Real American Doughnut Company Inc. is not responsible for the content of those sites. The inclusion of any link to such sites does not imply endorsement by The Real American Doughnut Company Inc. of the sites.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "TRADEMARK INFORMATION",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "All intellectual property, including but not limited to trademarks, service marks, trade names (collectively the 'Marks') are in this site are proprietary to The Real American Doughnut Company Inc., its licensors or to other respective owners. They may not be used, in any form or manner, or for any purpose without the prior written consent of The Real American Doughnut Company Inc.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "IMPORTANT REMINDER:",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "Without prejudice to criminal prosecution under Republic Act No. 8484, The Real American Doughnut Company Inc. has the sole right to cancel and/or reject a confirmed transaction if The Real American Doughnut Company Inc. finds any valid reason to believe that a transaction order has been purchased using fraudulent credit card.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "CHARGES FOR THE MOBILE SERVICE",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "The Real American Doughnut Company Inc. does not charge you to use the Mobile Service; however, please check your wireless plan, because your carrier's per-minute, text messaging, and data or other charges may apply. You must provide at your own expense the equipment and wireless connections needed for you to use the Mobile Service, and you are solely responsible for any costs you incur to access the Mobile Service. Use of the Mobile Service to send content to another person via e-mail, social media posting or SMS (Short Message Service, or 'text messaging') may result in wireless charges to both the sender and the receiver.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "RULES OF CONDUCT",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "By using our Mobile Service, Participant accepts the following rules of conduct:"+
                        "\n\nParticipant will not post to or transmit through our Mobile Service any material that would violate or infringe in any way upon the rights of others, including but not limited to any threatening, harmful, defamatory, obscene or pornographic material; material that is fraudulent or contains false, deceptive, or misleading statements, claims, or representations; or any material that violates any law or regulation, or would give rise to civil or criminal liability."+
                        "\n\nFurther, Participant shall not use our Mobile Service (a) in violation of any applicable law, regulation, or judicial order; (b) in a manner that violates the terms of these Terms and Conditions, the terms of any applicable agreement with The Real American Doughnut Company Inc., or any other The Real American Doughnut Company Inc. policy applicable through the Terms and Conditions; or (c) that interferes with or adversely affects the operation of this Mobile Service or the hardware, software, networks, systems, technology, and facilities upon and within which it operates (“Infrastructure”). Participants shall cooperate with The Real American Doughnut Company Inc. in investigating and correcting any apparent breach of these Terms and Conditions. "+
                        "\n\nParticipants shall be solely responsible for any material that they maintain, transmit, download, view, post, distribute (“Participant Material”) or otherwise access or make available using the Mobile Service or Infrastructure. "+
                        "\n\nIn the event that The Real American Doughnut Company Inc. becomes aware of any such Participant Material or of any activity engaged in by Participant that may violate these Terms and Conditions and/or expose The Real American Doughnut Company Inc. to civil or criminal liability, The Real American Doughnut Company Inc. reserves the right to investigate such Participant Material, block access to such material and suspend or terminate any features of or Content on the Mobile Service without liability. The Real American Doughnut Company Inc. further reserves the right to cooperate with legal authorities and third parties in investigating any alleged violations of these Terms and Conditions, including disclosing the identity of any Participant that The Real American Doughnut Company Inc. believes is responsible for such violation. The Real American Doughnut Company Inc. also reserves the right to implement technical mechanisms to prevent violations of these Terms and Conditions. Nothing in these Terms and Conditions shall limit in any way The Real American Doughnut Company Inc.'s rights and remedies at law or in equity that may otherwise be available.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "PRIVACY",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "We are committed to respecting your privacy and protecting your personally identifiable information. Any information we may collect regarding users of our Mobile Service is subject to our Privacy Policy, the full text of which is available in this mobile application also.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "INDEMNIFICATION",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "You agree to defend, indemnify and hold The Real American Doughnut Company Inc., and its subsidiaries, partners, agents, affiliates, licensors and advertisers and their respective officers, directors, shareholders, agents and representatives harmless from and against any and all claims, demands, liabilities, losses, damages, costs and expenses (including, without limitation, reasonable attorneys' fees) arising from your breach of your agreements under these Terms and Conditions",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "CONTACT INFORMATION",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "If you have questions or comments about these Terms and Conditions or our privacy practices, or if you wish to review or amend any personally identifiable information you have provided, you can contact us at:"+
                        "\n\nTHE REAL AMERICAN DOUGHNUT COMPANY INC."+
                        "\n\nKrispy Kreme Philippines"+
                        "\n11th Floor Eco Plaza building,"+
                        "\n2305 Chino Roces Avenue, Makati City 1231"+
                        "\nPhilippines"+
                        "\n\nAttention: Marketing Department"+
                        "\nPhone: (02) 784-9000"+
                        "\nE-Mail: onlinesupport@krispykreme.com.ph",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    SizedBox(height: 40,),
                  ]
                )
              )
            ),  
            Positioned(
              top: 140,
              left: 0,
              right: 0,
              child: Center(
                child: Container(
                  child: Text("TERMS AND CONDITION",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: AppColors.green
                    ),
                  )
                )
              )
            ), 
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            )
          ],
        ) 
      )
    );
  }
}