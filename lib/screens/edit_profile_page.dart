import 'dart:async';
import 'dart:convert';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:krispykreme/api/session_invalidation_notifier.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/card_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/form_validators.dart';
import 'package:krispykreme/model/city.dart';
import 'package:krispykreme/model/item.dart';
import 'package:krispykreme/model/request/link_account.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/profile_page.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/input_field.dart';
import 'package:krispykreme/widgets/radioButton.dart';
import 'package:krispykreme/widgets/searchable_list_picker.dart';
import 'package:krispykreme/widgets/selector_field.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/success.dart';
import 'package:http/http.dart' as http;
import 'package:krispykreme/widgets/text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfilePage extends StatefulWidget {

   @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
 
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();

  final FocusNode usernameFocusNode = new FocusNode();
  final FocusNode passwordFocusNode = new FocusNode();
  final FocusNode confirmpasswordFocusNode = new FocusNode();
  final FocusNode addressFocusNode = new FocusNode();

  AuthBloc _authBloc;
  CardBloc _cardBloc;
  StreamSubscription _stateSubs;
  StreamSubscription stateSubs;

  DateTime _birthday;
  String gender = "Male";
  String _address;
  Item _cities;
  int _city, _province;
  String _username, _firstName, _lastName, _mobileNun, _middleName;
  int _radioValue = 0;
  bool checkBox = false;
  bool passwordOn = true;
  bool linked = false;
  bool changes = false;
  bool locChange = false;
  String facebookUrl = "";
  String url = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcROsg5Z7zuvts1CnGRPYNP4OB_m9W009hdnjA&usqp=CAU";
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  SessionInvalidationNotifier _sessionInvalidationNotifier;

  @override
  void didChangeDependencies() {
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    if (_cardBloc.updateState.hasListener) _stateSubs?.cancel();
    _stateSubs = _cardBloc.updateState.listen(onStateChanged);
    stateSubs = _authBloc.linkAccountState.listen(onStateChangedLinking);
    _sessionInvalidationNotifier = MasterProvider.sessionInvalidator(context);
    super.didChangeDependencies();
    _cardBloc.getCities();
  }

  @override
  void dispose() {
    _stateSubs.cancel();
    stateSubs.cancel();
    super.dispose(); 
  }

  @override
  void initState(){
    super.initState();
    imageUrl();
    getFacebookLinked();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            Container(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: 50.0,),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top:90),
                      height: 150.0,
                      width: 150.0,
                      child: CircularProfileAvatar(
                        url, //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
                        radius: 70, // sets radius, default 50.0              
                        backgroundColor: Colors.transparent, // sets background color, default Colors.white
                        borderWidth: 2,  // sets border, default 0.0
                        borderColor: AppColors.green, // sets border color, default Colors.white
                        elevation: 5.0, // sets elevation (shadow of the profile picture), default value is 0.0
                        cacheImage: true, // allow widget to cache image against provided url
                      )
                    ),
                    SizedBox(height:10),
                    Container(
                      child: Text(
                        "Hi "+_authBloc.session.value.accountId+"!",
                        style: TextStyle(
                          fontSize: 28,
                          fontWeight: FontWeight.bold,
                          color: AppColors.green
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Let's get to know each other!",
                        style: TextStyle(
                          fontSize: 18,
                          color: AppColors.black
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    StreamBuilder<User>(
                      stream: _cardBloc.currentUser,
                      builder: (context, snapshot) {
                        return StreamHandler(
                          snapshot: snapshot,
                          loading: Center(child: CircularProgressIndicator()),
                          noData: Center(child:Container(child: Text("\n\nFailed to load user data"))),
                          withError: (error) => Center(child:Container(child: Text("\n\nFailed to load user data"))),
                          withData: (User user) {
                            _birthday = user.birthDate;
                            _username = user.username;
                            _address = user.address;
                            _firstName = user.firstname;
                            _lastName = user.lastName;
                            _middleName = user.middleName;
                            _mobileNun = user.mobile;
                            if(locChange == false){
                              _city = user.city;
                              _province = user.province;
                            }
                            return Expanded(
                              child: Container(
                                child: ListView(
                                  padding: EdgeInsets.symmetric(horizontal:25),
                                  children: <Widget>[
                                    SizedBox(height: 20.0),
                                    Container(
                                      child: Text(
                                        "Login Details",
                                        style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Quicksand',
                                          color: AppColors.green
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(height: 15.0),
                                    Column(
                                      children: [
                                        InputField(
                                          focusNode: usernameFocusNode,
                                          hintText: "Username",
                                          autoValidate: true,
                                          validator: _validateUsername,
                                          keyboardType: TextInputType.text,
                                          controller: _usernameController,
                                          submit: (value){
                                            FocusScope.of(context).requestFocus(passwordFocusNode);
                                          },
                                          change: (value){
                                            changes = true;
                                          },
                                        )
                                      ]
                                    ),
                                    SizedBox(height: 8.0),
                                    Stack(
                                      children: [
                                        Container(
                                          height: 50,
                                          child:InputField(
                                            focusNode: passwordFocusNode,
                                            hintText: "Password ",
                                            keyboardType: TextInputType.text,
                                            controller: _passwordController,
                                            masked: passwordOn,
                                            submit: (value){
                                              FocusScope.of(context).requestFocus(confirmpasswordFocusNode);
                                            },  
                                            change: (value){
                                              changes = true;
                                            },
                                          )
                                        ),
                                        Positioned(
                                          right: 0,
                                          child: Container(
                                            width: 50,
                                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: RawMaterialButton(
                                              onPressed: () {
                                                setState(() {
                                                  if(passwordOn == false){
                                                    passwordOn = true;
                                                  }else{
                                                    passwordOn = false;
                                                  }
                                                });
                                              },
                                              elevation: 5.0,
                                              child: getImage(),
                                              shape: CircleBorder(),                              
                                            )
                                          )
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 8.0),
                                    Stack(
                                      children: [
                                        Container(
                                          child: Column(
                                            children:[
                                              InputField(
                                                focusNode: confirmpasswordFocusNode,
                                                hintText: "Confirm Password",
                                                autoValidate: true,
                                                validator: _validateConfirmPassword,
                                                keyboardType: TextInputType.text,
                                                controller: _confirmPasswordController,
                                                masked: passwordOn,
                                                submit: (value){
                                                  FocusScope.of(context).requestFocus(addressFocusNode);
                                                },
                                                change: (value){
                                                  changes = true;
                                                },
                                              )
                                            ]
                                          )
                                        ),
                                        Positioned(
                                          right: 0,
                                          child: Container(
                                            width: 50,
                                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: RawMaterialButton(
                                              onPressed: () {
                                                setState(() {
                                                  if(passwordOn == false){
                                                    passwordOn = true;
                                                  }else{
                                                    passwordOn = false;
                                                  }
                                                });
                                              },
                                              elevation: 5.0,
                                              child: getImage(),
                                              shape: CircleBorder(),                              
                                            )
                                          )
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 30.0),
                                    SizedBox(
                                      child: Divider(thickness: 1,indent: 50, endIndent: 50)
                                    ),
                                    SizedBox(height: 10.0),
                                    Container(
                                      child: Text(
                                        "Connect Your\nFacebook Account:",
                                        style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Quicksand',
                                          color: AppColors.green
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(height: 5.0),
                                    linked == true
                                      ? Container(
                                        height: 60.0,
                                        child: StreamBuilder<ScreenState>(
                                          stream: _authBloc.linkAccountState,
                                          builder: (context, snapshot) {
                                            return _buildUnlinkButton(snapshot);
                                          }
                                        ),
                                      )
                                      : Container(
                                        height: 60.0,
                                        child: StreamBuilder<ScreenState>(
                                          stream: _authBloc.linkAccountState,
                                          builder: (context, snapshot) {
                                            return _buildLinkButton(snapshot);
                                          }
                                        ),
                                      ),
                                    SizedBox(height: 15.0),
                                    SizedBox(
                                      child: Divider(thickness: 1,indent: 50, endIndent: 50)
                                    ),
                                    SizedBox(height: 20.0),
                                    Container(
                                      child: Text(
                                        "Personal Information",
                                        style: TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Quicksand',
                                          color: AppColors.green
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(height: 15.0),
                                    StreamBuilder<Cities>(
                                      stream: _cardBloc.cities,
                                      builder: (context, snapshot) {
                                        return StreamHandler(
                                          snapshot: snapshot,
                                          loading: Center(child: CircularProgressIndicator()),
                                          noData: Center(child:Container(child: Text("\n\nFailed to province and city]\n\n"))),
                                          withError: (error) => Center(child:Container(child: Text("\n\nFailed to province and city]\n\n"))),
                                          withData: (Cities cities) {
                                            for(int i=0; i<cities.totalRecords; i++){
                                              provinceId[cities.cities[i].cityId] = cities.cities[i].stateRegionId;
                                              provinceList[cities.cities[i].stateRegionId] = cities.cities[i].stateRegionName;
                                              cityList[cities.cities[i].cityId] = cities.cities[i].cityName;
                                            }
                                            return Column(
                                              children: [
                                                Container(
                                                  height: 45,
                                                  child: DisplayField(
                                                      child: Text("  " +provinceList[_province],
                                                          style: TextStyle(
                                                              fontFamily: 'Schyler',
                                                              fontSize: 13.0,
                                                              color: Colors.black
                                                          )
                                                      ),
                                                  ),
                                                ),
                                                SizedBox(height: 10.0),
                                                Container(
                                                  height: 45,
                                                  child: SelectorField(
                                                    onPressed: () {_cityPicker();},
                                                    child: Text("  " +cityList[_city],
                                                        style: TextStyle(
                                                            fontFamily: 'Schyler',
                                                            fontSize: 13.0,
                                                            color: Colors.black
                                                        )
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            );
                                          }
                                        );
                                      }
                                    ),
                                    SizedBox(height: 18.0),
                                    Container(
                                      child: Text(
                                        "Gender",
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Quicksand',
                                          color: AppColors.black
                                        ),
                                        textAlign: TextAlign.start,
                                      ),
                                    ),
                                    Container(
                                      height: 100,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(
                                            height: 20,
                                            child: LabeledRadio(
                                              label: 'Male',
                                              padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                                              value: 0,
                                              groupValue: _radioValue,
                                              onChanged: _handleRadioValueChange
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                            child:LabeledRadio(
                                              label: 'Female',
                                              padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                                              value: 1,
                                              groupValue: _radioValue,
                                              onChanged: _handleRadioValueChange
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                            child:LabeledRadio(
                                              label: 'I do not want to specify',
                                              padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                                              value: 2,
                                              groupValue: _radioValue,
                                              onChanged: _handleRadioValueChange
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 8.0),
                                    Container(
                                      height: 45,
                                      child: DisplayField(
                                      child: Text("  " +changedateFormat(_birthday))
                                      ),
                                    ), 
                                    SizedBox(height: 15.0),
                                    SizedBox(
                                      height: 20,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Checkbox(
                                            value: checkBox,
                                            activeColor: AppColors.green,
                                            onChanged: (value) {
                                              setState(() {
                                                checkBox = value;
                                              });
                                            },
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Text("I have read and accepted the",
                                                style: TextStyle(color:Colors.black,fontSize:8, fontFamily: 'Quicksand'),),
                                              FlatButton(
                                                padding: EdgeInsets.all(0.0),
                                                child: Text(" Terms & Conditions",
                                                  style: TextStyle(color:AppColors.green,
                                                    fontSize: 10,
                                                    fontFamily: 'Quicksand',
                                                    fontWeight: FontWeight.bold
                                                  ),
                                                ),
                                                onPressed: () => Navigator.of(context).pushNamed(Routes.TERMS_AND_CONDITIONS)
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 40.0),
                                    Container(
                                      height: 60.0,
                                      child: StreamBuilder<ScreenState>(
                                        stream: _cardBloc.updateState,
                                        builder: (context, snapshot) {
                                          return _buildSubmitButton(snapshot);
                                        }
                                      ),
                                    ),
                                    SizedBox(height: MediaQuery.of(context).size.height * .05),
                                  ],
                                ),
                              )
                            );
                          }
                        );
                      }
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.NOTIFICATION);},
                        elevation: 5.0,
                        child: Image.asset(notifImage, fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:5),
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.PROFILEPAGE);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:10)
                  ]
                )
              )
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context,
                            new MaterialPageRoute(builder: (context) => new ProfilePage()),
                          )
                          .then((value) {
                            setState(() {
                            });
                          });
                        },
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            )
          ],
        ) 
      )
    );
  }

  Widget _buildSubmitButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            child:CircularProgressIndicator(),
            onPressed: (){},
          ),
        )
      );
    } else {
      if(checkBox == true){
        return Container(
          height: 50,
          margin: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
          child: Material(
            elevation: 5.0,
            borderRadius: BorderRadius.circular(15.0),
            color: AppColors.green,
            child: MaterialButton(
              height: 50,
              padding: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child:Text("Save Changes", 
                    style: TextStyle(fontSize: 24.0, color: AppColors.white),textAlign: TextAlign.center),
              onPressed: _onSubmit,
            ),
          )
        );
      }else{
        return Container(
          height: 50,
          margin: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
          child: Material(
            elevation: 5.0,
            borderRadius: BorderRadius.circular(15.0),
            color: Colors.grey,
            // ignore: missing_required_param
            child: MaterialButton(
              height: 50,
              padding: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child:Text("Save Changes", 
                    style: TextStyle(fontSize: 24.0, color: AppColors.white),textAlign: TextAlign.center),
            ),
          )
        );
      }
    }
  }

  Widget _buildLinkButton(AsyncSnapshot<ScreenState> snapshot){
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 40, vertical: 8),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: Colors.blueAccent,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child: CircularProgressIndicator(backgroundColor: Colors.white,),
          onPressed: (){},
          ),
        )
      );
    } else {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 40, vertical: 8),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: Colors.blueAccent,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(width: 10.0), 
                Container(
                  height: 20,
                  child: Image.asset("assets/icons/facebook.png", fit: BoxFit.contain),
                ),
                SizedBox(width: 15.0), 
                Expanded(
                  child: Text("Connect to Facebook", 
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.0, color: AppColors.white, fontWeight: FontWeight.bold),),
                )
              ],
            ),
          onPressed: _onLink,
          ),
        )
      );
    }
  }

  Widget _buildUnlinkButton(AsyncSnapshot<ScreenState> snapshot){
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 40, vertical: 8),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: Colors.redAccent,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child: CircularProgressIndicator(backgroundColor: Colors.white,),
          onPressed: (){},
          ),
        )
      );
    } else {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 40, vertical: 8),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: Colors.redAccent,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(width: 10.0), 
                Container(
                  height: 20,
                  child: Image.asset("assets/icons/facebook.png", fit: BoxFit.contain),
                ),
                SizedBox(width: 15.0), 
                Expanded(
                  child: Text("Disconnect to Facebook", 
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16.0, color: AppColors.white, fontWeight: FontWeight.bold),),
                )
              ],
            ),
          onPressed: _onUnlink,
          ),
        )
      );
    }
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _cardBloc.getAccountDetails(_authBloc.session.value.accountId);
        Navigator.pop(context);
        _firebaseMessaging.deleteInstanceID();
        _sessionInvalidationNotifier.notifyListeners();
        successMessage("Successfully updating account. Please login again!", context);
        break;
      case States.ERROR:
        errorMessage(state.error.toString(), context);
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void onStateChangedLinking(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        if(linked == false){
          successMessage("Successfully linking facebook account.", context);
          savedtoSharedPref(true, facebookUrl);
        }else{
          successMessage("Successfully unlinking facebook account.", context);
          savedtoSharedPref(false, "");
        }
        break;
      case States.ERROR:
        errorMessage(state.error.toString(), context);
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  String changedateFormat(DateTime date){
    final DateFormat formatter = DateFormat('MMM dd, yyyy');
    final String formatted = formatter.format(date);
    return formatted;
  }

  String _validateUsername(value) =>
      FormValidators.isAlphanumeric(value,
          "Username must be at least 3 characters long,\nmaximum of 16 characters and must be alpha-numeric.");

 String _validateConfirmPassword(value) =>
      FormValidators.isIdentical(
          _passwordController.text, value, "Confirm password does not match.");

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
      changes = true;
      switch (_radioValue) {
        case 0:
          gender = "Male";
          break;
        case 1:
          gender = "Female";
          break;
        case 2:
          gender = "I don't want to specify";
          break;
      }
    });
  }

  Widget getImage(){
    if(passwordOn == false){
      return Image.asset(
        "assets/icons/eyeon.png",
        color: AppColors.green,
        width:35,
        height:35
      );
    }else{
      return Image.asset(
        "assets/icons/eyeoff.png", 
        width:35,
        height:35
      );
    }
  }

  void _onSubmit() {
    
    String userName;
    if(_usernameController.text == "" || _usernameController.text == null){
     userName = _username;
    }else{
      userName = _usernameController.text;
    }
    final String password = _passwordController.text;
    String address;
    if(_addressController.text == "" || _addressController.text == null){
      address = _address;
    }else{
      address = _addressController.text;
    }
    final DateTime birthday = _birthday;
    int sex = _radioValue + 1;

    if(sex == 3){
      sex = 0;
    }

    final User user = User(
      address: address,
      username: userName,
      birthDate: birthday,
      password: password,
      gender: sex,
      title: 1,
      maritalStatus: 1,
      city: _city,
      zipCode: "0000",
      province: _province,
      firstname: _firstName,
      middleName:  _middleName,
      lastName: _lastName,
      mobile: _mobileNun
    );

    if(_passwordController.text != _confirmPasswordController.text){
      errorMessage("Confirm password does not match", context);
      return;
    }

    if(userName.length < 3){
       errorMessage("Invalid Username", context);
      return;
    }

    if(checkBox == false){
       errorMessage("Please accept the terms and condition!", context);
      return;
    }

    if(changes == false){
      errorMessage("No changes made!", context);
      return;
    }

    if (_formKey.currentState.validate()) {
      _cardBloc.updateAccount(_authBloc.session.value.accountId,user);
    }
  }

  Future<void> _onLink() async {
    
    final facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.nativeOnly;  
    final facebookLoginResult = await facebookLogin.logIn(['email']);

    try{
      switch (facebookLoginResult.status) {
        case FacebookLoginStatus.loggedIn:
          var graphResponse = await http.get(
          'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.width(800).height(800)&access_token=${facebookLoginResult.accessToken.token}');
          var profile = json.decode(graphResponse.body);
          facebookUrl = profile['picture']['data']['url'];
          _showConfirmDialog(profile['email'], profile['name'], profile['picture']['data']['url']);
          break;
        case FacebookLoginStatus.cancelledByUser:
          errorMessage("Facebook connect is cancelled by the user!", context);
          break;
        case FacebookLoginStatus.error:
          errorMessage('Something went wrong with the connecting process.\n'
            'Here\'s the error Facebook gave us: ${facebookLoginResult.errorMessage}', context);
          break;
      }
    }catch(Exception){
      errorMessage(Exception.toString(), context);
    }
  }

  Future<void> _onUnlink() async {
    final LinkAccountRequest user = LinkAccountRequest(
      username: _authBloc.session.value.accountId,
      account: "facebook",
    );
    _authBloc.unlinkAccount(user);
  }

  void _showConfirmDialog(String email, String username, String url) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .55,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .01),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(
                    height: 30,
                    width: 30,
                    child: RawMaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                        errorMessage("Linking Facebook have been cancelled!", context);
                      },
                      elevation: 5.0,
                      child: Image.asset("assets/icons/close.png", fit: BoxFit.cover,),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                  SizedBox(width: 15),
                  ],
                ),
                Container(
                  alignment: Alignment.center,
                  height: 120.0,
                  width: 120.0,
                  child: CircularProfileAvatar(
                    url, //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
                    radius: 100, // sets radius, default 50.0              
                    backgroundColor: Colors.transparent, // sets background color, default Colors.white
                    borderWidth: 2,  // sets border, default 0.0
                    borderColor: AppColors.green, // sets border color, default Colors.white
                    elevation: 5.0, // sets elevation (shadow of the profile picture), default value is 0.0
                    cacheImage: true, // allow widget to cache image against provided url
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  child: Text(
                    username,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  child: Text(
                    "Continue linking using \n"+ email,
                    style: TextStyle(
                      fontSize: 15,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  height:50,
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child:Text("Link", 
                        style: TextStyle(fontSize: 18.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){        
                        Navigator.pop(context);   
                        final LinkAccountRequest user = LinkAccountRequest(
                          username: _authBloc.session.value.accountId,
                          account: "facebook",
                          url: url,
                          email: email
                        );
                        _authBloc.linkAccount(user);
                      },
                    ),
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
              ]
            )
          ),
        );
      },
    );
  }

  imageUrl() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try{
      setState(() {
        if(prefs.getBool('IsFacebookUrl') == true){
          url = prefs.getString('FacebookUrl');
        }else{
          url = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcROsg5Z7zuvts1CnGRPYNP4OB_m9W009hdnjA&usqp=CAU";
        }
      });
    }catch(Exception){}
  }

  getFacebookLinked() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();  
    try{
      setState(() {
        if(prefs.getBool('FacebookLink') == true){
          linked = true;
        }else{
          linked = false;
        }
      });
    }catch(Exception){}
  }

  savedtoSharedPref(bool connected, String link) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try{
      setState(() {
        if(connected == false){
          prefs.setBool("IsFacebookUrl", false);
          prefs.setBool("FacebookLink", false);
          prefs.setString("FacebookUrl", "");
          linked = false;
          url = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcROsg5Z7zuvts1CnGRPYNP4OB_m9W009hdnjA&usqp=CAU";
        }else{
          prefs.setBool("IsFacebookUrl", true);
          prefs.setBool("FacebookLink", true);
          prefs.setString("FacebookUrl", link);
          linked = true;
          url = link;
        }
      });
    }catch(Exception){}
  }

  List<Item> _getCity() =>
      cityList.keys.map((key) => Item(id: key, value: cityList[key])).toList();

  void _cityPicker() async {
    final Item item = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>SearchableListPicker(title: "City", items: _getCity())));
    if (item != null)
      setState(() {
        locChange = true;
        changes = true;
        _cities = item;
        _city = _cities?.id;
        _province = provinceId[_cities?.id];
      });
  }
}