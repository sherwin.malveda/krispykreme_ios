import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/card_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/history.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/success.dart';
import 'package:krispykreme/widgets/txn_list.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:intl/intl.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  
  AuthBloc _authBloc;
  CardBloc _cardBloc;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  final doc2 = pw.Document();
  
  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _cardBloc = MasterProvider.card(context);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            Container(
              padding: EdgeInsets.fromLTRB(0, 210, 0, 0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage('assets/image/bg.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child:StreamBuilder(
                stream: _cardBloc.history,
                builder: (BuildContext context, AsyncSnapshot<History> snapshot) {
                  return StreamHandler(
                    snapshot: snapshot,
                    loading: Center(child: CircularProgressIndicator()),
                    noData:SmartRefresher(
                        enablePullDown: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand'),
                        ),
                        controller: _refreshController,
                        onRefresh: () => _onRefresh(0),
                        onLoading: () => _onLoading(0),
                        child: Center(
                        child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                          child: Text("No transaction history at the moment")
                        )
                      ),
                    ),
                    withError: (error) => SmartRefresher(
                        enablePullDown: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand'),
                        ),
                        controller: _refreshController,
                        onRefresh: () => _onRefresh(0),
                        onLoading: () => _onLoading(0),
                        child: Center(
                        child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                          child: Text("No transaction history at the moment")
                        )
                      ),
                    ),
                    withData: (History hist) {
                      return Column(
                        children: [
                          Expanded(
                            child: SmartRefresher(
                              enablePullDown: true,
                              enablePullUp: true,
                              header: ClassicHeader(
                                refreshingText: "Refreshing",
                                textStyle: TextStyle(fontFamily: 'Quicksand'),
                              ),
                              footer: CustomFooter(
                                builder: (BuildContext context,LoadStatus mode){
                                  Widget body ;
                                  if(mode==LoadStatus.loading){body =  CupertinoActivityIndicator();}
                                  else{body = Text("No more items", style: TextStyle(fontFamily: 'Quicksand'),);}
                                  return Container(
                                    height: 55.0,
                                    child: Center(child:body),
                                  );
                                },
                              ),       
                              controller: _refreshController,
                              onRefresh:()=> _onRefresh(hist.transactions.length),
                              onLoading:()=> _onLoading(hist.transactions.length),
                              child: ListView.builder(
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemCount: hist.transactions.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return TxnList(hist: hist.transactions[index]);
                                }
                              ),
                            ),
                          ),
                          SizedBox(height:10),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 50, vertical: 8),
                            decoration: BoxDecoration(
                              color: AppColors.green,
                              borderRadius: BorderRadius.all(Radius.circular(15.0)),
                            ),
                            child: Material(
                              color: AppColors.green,
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(15.0),
                              child: MaterialButton(
                              height:50,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(width: 10.0), 
                                    Expanded(
                                      child: Text("Download PDF", 
                                      style: TextStyle(fontSize: 25.0, color: AppColors.white),textAlign: TextAlign.center,),
                                    ),
                                    SizedBox(width: 10.0), 
                                    Container(
                                      height: 20,
                                      child: Icon(Icons.file_download, color: Colors.white)
                                    ),
                                  ],
                                ),
                                onPressed: () async {
                                  if(hist.totalTransactions > 0){
                                    var storage = await downloadPdf(hist);
                                    successMessage(storage.toString(), context);
                                  }else{
                                    errorMessage("Unable to download pdf!\nTransaction History is empty", context);
                                  }
                                }
                              ),
                            )
                          ),
                          SizedBox(height:30),
                        ],
                      );
                    },
                  );
                }
              )  
            ),  
            Positioned(
              top: 140,
              left: 0,
              right: 0,
              child: Column(children: [
                Center(
                  child: Container(
                    child: Text("E-VOUCHER HISTORY",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: AppColors.green
                      ),
                    )
                  )
                ),
                Center(
                  child: Container(
                    child: Text("See your past transaction for the last 3 months!",
                      style: TextStyle(
                        fontSize: 15,
                        color: AppColors.black
                      ),
                    )
                  )
                )
              ],) 
            ), 
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.NOTIFICATION);},
                        elevation: 5.0,
                        child: Image.asset(notifImage, fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:5),
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.PROFILEPAGE);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:10)
                  ]
                )
              )
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            )
          ],
        ) 
      )
    );
  }

  void _onRefresh(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      _cardBloc.getHistory(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: (currentCount ~/ MAX_ROW_LIST) + 1)
      );
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      _cardBloc.getHistory(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: (currentCount ~/ MAX_ROW_LIST) + 1)
      );
    });
    _refreshController.loadComplete();
  }

  Future<String> downloadPdf(History hist) async{

    doc2.addPage(
      pw.MultiPage(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) => 
          <pw.Widget> [
            pw.Center(
              child: pw.Container(
                child: pw.Text("E-VOUCHER HISTORY",
                  style: pw.TextStyle(
                    fontSize: 24,
                    fontWeight: pw.FontWeight.bold,
                  ),
                )
              )
            ),
            pw.Center(
              child: pw.Container(
                child: pw.Text("See your past transaction for the last 3 months!",
                  style: pw.TextStyle(
                    fontSize: 15,
                  ),
                )
              )
            ),
            pw.SizedBox(height: 40),
            pw.Column(
              children: [
                getDocument(hist, 0),
                getDocument(hist, 1),
                getDocument(hist, 2),
                getDocument(hist, 3),
                getDocument(hist, 4),
                getDocument(hist, 5),
                getDocument(hist, 6),
                getDocument(hist, 7),
                getDocument(hist, 8),
                getDocument(hist, 9),
                getDocument(hist, 10),
                getDocument(hist, 11),
                getDocument(hist, 12),
                getDocument(hist, 13),
                getDocument(hist, 14),
                getDocument(hist, 15),
                getDocument(hist, 16),
                getDocument(hist, 17),
                getDocument(hist, 18),
                getDocument(hist, 19),
                getDocument(hist, 20),
                getDocument(hist, 21),
                getDocument(hist, 22),
                getDocument(hist, 23),
                getDocument(hist, 24),
                getDocument(hist, 25),
                getDocument(hist, 26),
                getDocument(hist, 27),
                getDocument(hist, 28),
                getDocument(hist, 29),
                getDocument(hist, 30),
                getDocument(hist, 31),
                getDocument(hist, 32),
                getDocument(hist, 33),
                getDocument(hist, 34),
                getDocument(hist, 35),
                getDocument(hist, 36),
                getDocument(hist, 37),
                getDocument(hist, 38),
                getDocument(hist, 39),
                getDocument(hist, 40),
                getDocument(hist, 41),
                getDocument(hist, 42),
                getDocument(hist, 43),
                getDocument(hist, 44),
                getDocument(hist, 45),
                getDocument(hist, 46),
                getDocument(hist, 47),
                getDocument(hist, 48),
                getDocument(hist, 49),
                getDocument(hist, 50),
                getDocument(hist, 51),
                getDocument(hist, 52),
                getDocument(hist, 53),
                getDocument(hist, 54),
                getDocument(hist, 55),
                getDocument(hist, 56),
                getDocument(hist, 57),
                getDocument(hist, 58),
                getDocument(hist, 59),
                getDocument(hist, 60)
              ]
            ),
          ],
        ) 
      );

    try{
      DateFormat dateFormat = DateFormat("yyyyMMddHHmmss");
      String filename = "kkHistory_"+ dateFormat.format(DateTime.now()).toString()+".pdf";
      final File file = File('/sdcard/KKMobile/'+filename);
      new Directory("/sdcard/KKMobile/").create(recursive: true);
      file.writeAsBytesSync(doc2.save());
      return "Successfully saved file in " +file.toString();
    }catch(Exception){
      print(Exception);
      DateFormat dateFormat = DateFormat("yyyyMMddHHmmss");
      String filename = "kkHistory_"+ dateFormat.format(DateTime.now()).toString()+".pdf";
      final File file = File('/sdcard/'+filename);
      file.writeAsBytesSync(doc2.save());
      return "Successfully saved file in " +file.toString();
    }
  }

  getDocument(History hist, int index){
    try{
      String date = "";
      String type = "";

      if(hist.transactions[index].transactionStatus == 0){
        type = "Processing";
      }else if(hist.transactions[index].transactionStatus == 1){
        type = "Success";
      }else if(hist.transactions[index].transactionStatus == 2){
        type = "Failed";
      }else if(hist.transactions[index].transactionStatus == 3){
        type = "Reversed";
      }else{
        type = "Voided";
      }

      final DateFormat formatter = DateFormat('MMM dd, yyyy');
      date = formatter.format(hist.transactions[index].transactionDate);

      return pw.Container(
        margin: pw.EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: pw.Column(
          mainAxisAlignment: pw.MainAxisAlignment.start,
          crossAxisAlignment: pw.CrossAxisAlignment.start,
          children: [
            pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
              children: [
                pw.Text(hist.transactions[index].transactionType,
                  style: pw.TextStyle(
                    fontSize: 15.0
                  )
                ),
                pw.Text(date + " " + hist.transactions[index].transactionDate.toString().substring(11,19),
                  style: pw.TextStyle(
                    fontSize: 13.0
                  )
                ),
              ],
            ),
            pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
              children: [
                pw.Text("RRN. "+hist.transactions[index].transactionId,
                  style: pw.TextStyle(
                  fontSize: 15.0
                  )
                ),
                pw.Text(hist.transactions[index].voucherCode,
                  style: pw.TextStyle(
                    fontSize: 13.0
                  )
                ),
              ],
            ),
            pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
              children: [
                pw.Text("Claimed at "+ hist.transactions[index].storeLocation,
                  style: pw.TextStyle(
                  fontSize: 15.0
                  )
                ),
                pw.Text(hist.transactions[index].invoiceNumber,
                  style: pw.TextStyle(
                  fontSize: 13.0
                  )
                ),
              ],
            ),
            pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
              children: [
                pw.Text("Php. "+ hist.transactions[index].transactionAmount.toString(),
                  style: pw.TextStyle(
                  fontSize: 12.0
                  )
                ),
                pw.Text(type,
                  style: pw.TextStyle(
                  fontSize: 13.0
                  )
                ),
              ],
            ),
            pw.SizedBox(height: 10),
            new pw.Divider()
          ],
        ),
      ); 
    }catch(Exception){
      return pw.Container();
    }
  }  
}
