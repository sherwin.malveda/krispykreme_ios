import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/api/session_invalidation_notifier.dart';
import 'dart:ui';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/notification_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>  with WidgetsBindingObserver{

  AuthBloc _authBloc;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  SessionInvalidationNotifier _sessionInvalidationNotifier;
  String url = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcROsg5Z7zuvts1CnGRPYNP4OB_m9W009hdnjA&usqp=CAU";

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _sessionInvalidationNotifier = MasterProvider.sessionInvalidator(context);
    super.didChangeDependencies();
  }
  
  @override
  void initState(){
    imageUrl();
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("alalalal" +state.toString());
    if (state == AppLifecycleState.resumed) {
      setState(() {});
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top:150),
              height: 120.0,
              width: 120.0,
              child: CircularProfileAvatar(
                url, //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
                radius: 100, // sets radius, default 50.0              
                backgroundColor: Colors.transparent, // sets background color, default Colors.white
                borderWidth: 2,  // sets border, default 0.0
                borderColor: AppColors.green, // sets border color, default Colors.white
                elevation: 5.0, // sets elevation (shadow of the profile picture), default value is 0.0
                cacheImage: true, // allow widget to cache image against provided url
              )
            ),
            ListView(
              padding: EdgeInsets.only(top:300),
              children: <Widget>[
                Container(
                  child: Text(
                    "Hi, "+_authBloc.session.value.accountId+"!",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 30.0),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal:50, vertical: 0),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child:Text("Edit Profile", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){Navigator.of(context).pushReplacementNamed(Routes.EDIT_PROFILE);},
                    ),
                  )
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal:50, vertical: 0),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child:Text("History", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){Navigator.of(context).pushNamed(Routes.HISTORY);},
                    ),
                  )
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal:50, vertical: 0),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child:Text("FAQs", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){Navigator.of(context).pushNamed(Routes.FAQS);},
                    ),
                  )
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal:50, vertical: 0),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.pink,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child:Text("Logout", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){
                        _showConfirmDialog();},
                    ),
                  )
                ),
              ],
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context, MaterialPageRoute(
                              builder: (_) {
                                return NotificationPage();
                              }
                            )
                          );
                        },
                        elevation: 5.0,
                        child: Image.asset(notifImage, fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:5),
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:10)
                  ]
                )
              )
            ),
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            )
          ],
        ),
      ),
    );
  }

  void _showConfirmDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .35,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Text("Are you sure you want to leave\nthe app?",
                  style: TextStyle(
                  fontSize: 18.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .05),
                Container(
                  height: 50,
                  padding: EdgeInsets.symmetric(horizontal:50, vertical: 0),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.pink,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child:Text("Logout", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){
                        Navigator.pop(context);
                        _firebaseMessaging.deleteInstanceID();
                        _sessionInvalidationNotifier.notifyListeners();
                      },
                    ),
                  )
                )
              ]
            )
          ),
        );
      },
    );
  }

  imageUrl() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();  
    try{
      setState(() {
        if(prefs.getBool('IsFacebookUrl') == true){
          url = prefs.getString('FacebookUrl');
        }else{
          url = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcROsg5Z7zuvts1CnGRPYNP4OB_m9W009hdnjA&usqp=CAU";
        }
      });
    }catch(Exception){}
  }
}