import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/form_validators.dart';
import 'package:krispykreme/model/request/member_request.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/arguments/registration_arguments.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/input_field.dart';

class OGCardPage extends StatefulWidget {
  @override
  _OGCardPageState createState() => _OGCardPageState();
}

class _OGCardPageState extends State<OGCardPage> {

  TextEditingController cardNum = new TextEditingController();
  TextEditingController email = new TextEditingController();
  final FocusNode fnCardNum = new FocusNode();
  final FocusNode fnEmail = new FocusNode();
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            ListView(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.height * .04,
                MediaQuery.of(context).size.height * .15,
                MediaQuery.of(context).size.height * .04,
                MediaQuery.of(context).size.height * .00
              ),
              children: <Widget>[
                Container(
                  height: 80.0,
                  child: Image.asset("assets/image/logo.png", fit: BoxFit.contain,)
                ), 
                SizedBox(height: MediaQuery.of(context).size.height * .1),
                Container(
                  child: Text(
                    "Activate your OG Card",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .04),
               Column(
                  children: [
                    InputField(
                      focusNode: fnCardNum,
                      hintText: "Card Number",
                      keyboardType: TextInputType.text,
                      controller: cardNum,
                      validator: _validateNumber,
                      autoValidate: true,
                      submit: (value){
                        FocusScope.of(context).requestFocus(fnEmail);
                      },
                    )
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Column(
                  children: [
                    InputField(
                      focusNode: fnEmail,
                      hintText: "Email Address",
                      validator: _validateEmail,
                      autoValidate: true,
                      keyboardType: TextInputType.text,
                      controller: email,
                      submit: (value){
                        _navigateToRegistration(cardNum.text, email.text);
                      },
                    )
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .15),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal: 50, vertical: 0),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 50,
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Text("Next Step", 
                              style: TextStyle(fontSize: 18.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                bottomRight:  const  Radius.circular(15.0),
                                topRight: const  Radius.circular(15.0)
                              ),
                              color: Colors.black12
                            ),
                            alignment: Alignment.center,
                            child: Text("1 of 2", 
                              style: TextStyle(fontSize: 13.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center),            
                          )
                        ],
                      ),
                      onPressed: (){_navigateToRegistration(cardNum.text, email.text);},
                    ),
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  height: 30,
                  child: Material(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.transparent,
                    child: MaterialButton(
                      child: Container(
                        decoration: new BoxDecoration(
                          border: Border(bottom: BorderSide(color: AppColors.green))
                        ),
                        child: Text("Back to Login",
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            color: AppColors.green,
                            fontSize: 15.0
                          )
                        ),
                      ),
                      onPressed: (){_navigateToLoginPage(context);},
                    ),
                  )
                ),
              ],
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png", color: AppColors.green),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            )
          ],
        ) 
      )
    );
  }

  String _validateNumber(String value) => 
    FormValidators.isNumber(value, "Enter a valid number");

  String _validateEmail(String value) =>
    FormValidators.isValidEmail(value, "Enter a valid email");

  void _navigateToLoginPage(BuildContext context) {
     Navigator.of(context).pushReplacementNamed(Routes.LOGIN);
  }

  void _navigateToRegistration(String cardNumber, String email) {
    if(cardNumber == null || cardNumber == ""){
      errorMessage("Card Number must not be null or empty.", context);
    }else if(_validateNumber(cardNumber) != null){
      errorMessage("Card number is not valid.", context);
    }else if(email == null || email == ""){
      errorMessage("Email must not be null or empty.", context);
    }else if(_validateEmail(email) != null){
      errorMessage("Email is not valid.", context);
    }else{
      final MemberRequest request = MemberRequest(
        cardNumber : cardNumber,
        email : email);
        RegistrationArguments args = RegistrationArguments(email,cardNumber,request);
        Navigator.of(context).pushReplacementNamed(Routes.REGISTRATION, arguments: args);
      }
    }
}
