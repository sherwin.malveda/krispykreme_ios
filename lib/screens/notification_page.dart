import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/blocs/store_bloc.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/notifications.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/screens/notificationbody_page.dart';
import 'package:krispykreme/screens/profile_page.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/success.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {

  StoreBloc _storeBloc;
  AuthBloc _authBloc;
  StreamSubscription stateSubs;

  RefreshController _refreshController = RefreshController(
      initialRefresh: false);

  void initState() {
    super.initState();
    notifImage = "assets/icons/notif.png";
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _storeBloc = MasterProvider.store(context);
    stateSubs = _storeBloc.updateState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.green2,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0,90, 0, 0),
            child: StreamBuilder(
                stream: _storeBloc.notification,
                builder: (BuildContext context, AsyncSnapshot<Notifications> snapshot) {
                  return StreamHandler(
                    snapshot: snapshot,
                    loading: Center(child: CircularProgressIndicator()),
                    noData: SmartRefresher(
                        enablePullDown: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand',color: Colors.white),
                        ),
                        controller: _refreshController,
                        onRefresh: () => _onRefresh(),
                        onLoading: () => _onLoading(),
                        child: Center(
                        child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                          child: Text("No notifications at the moment",
                            style: TextStyle(color: Colors.white),)
                        )
                      ),
                    ),
                    withError: (error) => SmartRefresher(
                        enablePullDown: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand',color: Colors.white),
                        ),
                        controller: _refreshController,
                        onRefresh: () => _onRefresh(),
                        onLoading: () => _onLoading(),
                        child: Center(
                        child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                          child: Text("No notifications at the moment",
                            style: TextStyle(color: Colors.white),)
                        )
                      ),
                    ),
                    withData: (Notifications notification) {
                      return SmartRefresher(
                        enablePullDown: true,
                        enablePullUp: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand',color: Colors.white),
                        ),
                        footer: CustomFooter(
                            builder: (BuildContext context,LoadStatus mode){
                              Widget body ;
                              if(mode==LoadStatus.loading){body =  CupertinoActivityIndicator();}
                              else{body = Text("No more items", style: TextStyle(fontFamily: 'Quicksand',color: Colors.white),);}
                              return Container(
                                height: 55.0,
                                child: Center(child:body),
                              );
                            },
                          ), 
                        controller: _refreshController,
                        onRefresh: () => _onRefresh(),
                        onLoading: () => _onLoading(),
                        child: ListView.builder(
                          padding: EdgeInsets.fromLTRB(0,40,0,0),
                          itemCount: notification.message.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                                padding: EdgeInsets.fromLTRB(10, 5,10, 5),
                                child: Material(
                                  borderRadius: BorderRadius.circular(15.0),
                                  color: AppColors.white,
                                  child: MaterialButton(
                                    shape:  RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                                      child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          child: SizedBox(
                                            height: 50,
                                            width: 50,
                                            child: CachedNetworkImage(
                                              imageUrl: notification.message[index].messageIcon,
                                              fit: BoxFit.contain,
                                              placeholder: (context, url) => new Center (
                                                child: CircularProgressIndicator(backgroundColor: AppColors.green,)
                                              ),
                                              errorWidget: (context, url, error) => new Container(
                                                child:Center(
                                                  child: Image.asset('assets/image/logo.png', fit: BoxFit.contain)
                                                )
                                              )
                                            )
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(10.0, 15.0, 3.0, 0.0),
                                                child: Text(notification.message[index].messageTitle,
                                                  style: TextStyle(
                                                    fontFamily: 'Quicksand',
                                                    color: AppColors.green,
                                                    fontSize: 12.0,
                                                    fontWeight: FontWeight.bold
                                                  ),
                                                  textAlign: TextAlign.start
                                                )
                                              ),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(20.0, 2.0, 10.0, 15.0),
                                                child: Text(notification.message[index].messageBody,
                                                  style: TextStyle(
                                                    fontFamily: 'Quicksand',
                                                    color: AppColors.black,
                                                    fontSize: 10.0
                                                  ),
                                                  maxLines: 2,
                                                )
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          child: SizedBox(
                                            height: 25,
                                            width: 25,
                                            child: RawMaterialButton(
                                              onPressed: () {
                                                _showConfirmDialog(notification, index);
                                              },
                                              elevation: 2.0,
                                              child: Image.asset("assets/icons/close.png"),
                                              shape: CircleBorder(),
                                            )
                                          ),
                                        ),
                                      ],
                                    ),  
                                    onPressed: (){
                                      Navigator.push(
                                        context, MaterialPageRoute(
                                        builder: (_) {
                                          return NotificationBodyPage(
                                            notifbody: notification.message[index].messageContent,
                                            notiftitle: notification.message[index].messageTitle,
                                            notifimage: notification.message[index].messageImage,
                                          );
                                        }
                                      ));
                                    }
                                  )    
                                )
                            );
                          }
                        )
                      );
                    },
                  );
                }
              ),
          ),
          Positioned(
            top: 0,
            child: Container(
              height: 90,
              width: MediaQuery.of(context).size.width * 1,
              color: AppColors.green,
              padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 40,
                    width: 40,
                    child: RawMaterialButton(
                      onPressed: () {},
                      elevation: 5.0,
                      child: Image.asset(notifImage, fit: BoxFit.cover,),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                  SizedBox(width:5),
                  SizedBox(
                    height: 40,
                    width: 40,
                    child: RawMaterialButton(
                      onPressed: () {
                        Navigator.pushReplacement(
                          context, MaterialPageRoute(
                            builder: (_) {
                              return ProfilePage();
                            }
                          )
                        );
                      },
                      elevation: 5.0,
                      child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                  SizedBox(width:10)
                ]
              )
            )
          ),
          Positioned(
            top: 0,
            child: Container(
              height: 90,
              width: MediaQuery.of(context).size.width * .5,
              color: AppColors.green,
              padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width:15),
                  SizedBox(
                    height: 30,
                    width: 30,
                    child: RawMaterialButton(
                      onPressed: () {Navigator.pop(context);},
                      elevation: 5.0,
                      child: Image.asset("assets/icons/back.png"),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                ]
              )
            )
          ),
          Positioned(
            top: 65,
            left: 0,
            right: 0,
            child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
          )
        ],
      )
    );
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        Navigator.pop(context);
        _onRefresh();
        successMessage("Successfull deleting notification!", context);
        break;
      case States.ERROR:
        Navigator.pop(context);
        errorMessage(state.error.toString(), context);
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    if (mounted)
      setState(() {
        _storeBloc.getNotification(ListRequest(accountId: _authBloc.session.value.accountId, rows: INFINITE_ROW_LIST, page: 1));
      });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    if (mounted)
      setState(() {
        _storeBloc.getNotification(ListRequest(accountId: _authBloc.session.value.accountId, rows: INFINITE_ROW_LIST, page: 1));
      });
    _refreshController.loadComplete();
  }

void _showConfirmDialog(Notifications notif, int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .35,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Text("Are you sure you want to delete\nthis notification?",
                  style: TextStyle(
                  fontSize: 14.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .05),
                Container(
                  height: 60.0,
                  child:StreamBuilder<ScreenState>(
                    stream: _authBloc.loginState,
                    builder: (context, snapshot) => _buildDeleteButton(snapshot, notif, index)
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  color: Colors.transparent,
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                  height: 40,
                  child: Material(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.transparent,
                    child: MaterialButton(
                      child: Container(
                        decoration: new BoxDecoration(
                          border: Border(bottom: BorderSide(color: AppColors.green))
                        ),
                        child: Text("Cancel",
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            color: AppColors.green,
                            fontSize: 15.0
                          )
                        ),
                      ),
                      onPressed: (){Navigator.pop(context);},
                    ),
                  )
                ),
              ]
            )
          ),
        );
      },
    );
  }

  Widget _buildDeleteButton(AsyncSnapshot<ScreenState> snapshot, Notifications notification,int index){
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height: 40,
        margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          // ignore: missing_required_param
          child: MaterialButton(
            height: 40,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
            child:CircularProgressIndicator()
          ),
        )
      );
    } else {
      return Container(
        height: 40,
        margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 40,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
            child:Text("Yes", 
              style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
            onPressed: (){
              deleteNotification(notification.message[index].messageId, notification.message[index].taskId);
            },
          ),
        )
      );
    }
  }

  deleteNotification(int page, int row){
    _storeBloc.deleteNotification(ListRequest(accountId: _authBloc.session.value.accountId, rows: row, page: page));
  }
  
}