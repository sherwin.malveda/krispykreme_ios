import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/request/reset_password_request.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/input_field.dart';
import 'package:krispykreme/widgets/success.dart';

class ForgotPasswordResetPage extends StatefulWidget {
  
  final String username;
  const ForgotPasswordResetPage({Key key, @required this.username}) : super(key: key);
  
  @override
  _ForgotPasswordResetPageState createState() =>
      _ForgotPasswordResetPageState();
}

class _ForgotPasswordResetPageState extends State<ForgotPasswordResetPage> {

  final TextEditingController _code = TextEditingController();
  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();
  final FocusNode codeFocusNode = new FocusNode();
  final FocusNode newpassFocusNode = new FocusNode();
  final FocusNode confirmnewpassFocusNode = new FocusNode();

  StreamSubscription _stateSubs;

  AuthBloc _authBloc;

  bool passwordOn = true;

  @override
  void initState(){
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _stateSubs = _authBloc.resetPasswordState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _stateSubs.cancel();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            ListView(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.height * .04,
                MediaQuery.of(context).size.height * .15,
                MediaQuery.of(context).size.height * .04,
                MediaQuery.of(context).size.height * .02,
              ),
              children: <Widget>[
                Container(
                  height: 80.0,
                  child: Image.asset("assets/image/logo.png", fit: BoxFit.contain,)
                ), 
                SizedBox(height: 50.0),
                Container(
                  child: Text(
                    "Reset your password!",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 30.0),
                Container(
                  height: 50,
                  child:InputField(
                    hintText: "Code",
                    keyboardType: TextInputType.text,
                    controller: _code,
                  )
                ),
                SizedBox(height: 10.0),
                Stack(
                  children: [
                    Container(
                      height: 50,
                      child:InputField(
                        hintText: "New Password ",
                        autoValidate: true,
                        keyboardType: TextInputType.text,
                        controller: _newPassword,
                        masked: passwordOn,
                      )
                    ),
                    Positioned(
                      right: 0,
                      child: Container(
                        width: 50,
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: RawMaterialButton(
                          onPressed: () {
                            setState(() {
                              if(passwordOn == false){
                                passwordOn = true;
                              }else{
                                passwordOn = false;
                              }
                            });
                          },
                          elevation: 5.0,
                          child: getImage(),
                          shape: CircleBorder(),                              
                        )
                      )
                    )
                  ],
                ),
                SizedBox(height: 10.0),
                Stack(
                  children: [
                    Container(
                      height: 50,
                      child:InputField(
                        hintText: "Confirm Password ",
                        autoValidate: true,
                        keyboardType: TextInputType.text,
                        controller: _confirmPassword,
                        masked: passwordOn,
                      )
                    ),
                    Positioned(
                      right: 0,
                      child: Container(
                        width: 50,
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: RawMaterialButton(
                          onPressed: () {
                            setState(() {
                              if(passwordOn == false){
                                passwordOn = true;
                              }else{
                                passwordOn = false;
                              }
                            });
                          },
                          elevation: 5.0,
                          child: getImage(),
                          shape: CircleBorder(),                              
                        )
                      )
                    )
                  ],
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 60.0,
                  child: StreamBuilder<ScreenState>(
                    stream: _authBloc.forgotPasswordState,
                    builder: (context, snapshot) => _buildResetPasswordButton(snapshot)
                  )
                ),
              ],
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png", color: AppColors.green),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            )
          ],
        ) 
      )
    );
  }

  Widget _buildResetPasswordButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child:CircularProgressIndicator(),
            onPressed: (){},
          ),
        )
      );
    } else {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal:30, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child:Text("Reset Password", 
              style: TextStyle(fontSize: 20.0, color: AppColors.white),textAlign: TextAlign.center,),
            onPressed: _resetPassword,
          ),
        )
      );
    }
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _navigateToLandingPage(context);
        break;
      case States.ERROR:
        errorMessage(state.error.toString(), context);
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  Widget getImage(){
    if(passwordOn == false){
      return Image.asset(
        "assets/icons/eyeon.png",
        color: AppColors.green,
        width:35,
        height:35
      );
    }else{
      return Image.asset(
        "assets/icons/eyeoff.png", 
        width:35,
        height:35
      );
    }
  }

  void _navigateToLandingPage(BuildContext context) {
      Navigator.of(context).pushReplacementNamed(Routes.LANDING);
      successMessage("Successful changing password!", context);
  }

  void _resetPassword() {
    final String code = _code.text;
    final String newPassword = _newPassword.text;
    final String confirmPassword = _confirmPassword.text;

    if (code.isEmpty || newPassword.isEmpty || confirmPassword.isEmpty) {
      errorMessage("Fill all the fields",context);
      return;
    }

    if(_confirmPassword.text != _newPassword.text){
      errorMessage("Confirm password does not match",context);
      return;
    }

    final ResetPasswordRequest request = ResetPasswordRequest(
      code: code, 
      newPassword: newPassword, 
      confirmPassword: confirmPassword,
      username: widget.username
    );
    _authBloc.resetPassword(request);
  }

}