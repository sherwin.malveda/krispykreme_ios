import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/routes.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/dom.dart' as dom;

class NotificationBodyPage extends StatefulWidget {
  final String notiftitle, notifbody, notifimage;
  const NotificationBodyPage({Key key, @required this.notifbody, @required this.notiftitle, @required this.notifimage}) : super(key: key);
  @override
  _NotificationBodyPageState createState() => _NotificationBodyPageState();
}

class _NotificationBodyPageState extends State<NotificationBodyPage> {
  
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height: 90),
                Container(
                  height: MediaQuery.of(context).size.height * .25,
                  width: MediaQuery.of(context).size.width * 1,
                  decoration: BoxDecoration(
                    color: AppColors.blue2,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0))
                  ),
                  child: Container(
                    child: CachedNetworkImage(
                      imageUrl: widget.notifimage,
                      fit: BoxFit.cover,
                      placeholder: (context, url) => new Center (
                        child: CircularProgressIndicator(backgroundColor: AppColors.green,)
                      ),
                      errorWidget: (context, url, error) => new Container(
                        child:Center(
                          child: Image.asset('assets/image/logo.png', fit: BoxFit.contain)
                        )
                      )
                    )
                  )
                ),
                SizedBox(height: 10.0),
                Container(
                  margin: EdgeInsets.symmetric(horizontal:10.0),
                  child: Text(widget.notiftitle,
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Quicksand',
                      color: AppColors.black,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.all(0),
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
                        child: Html(
                          data: ''+widget.notifbody+'',
                          defaultTextStyle: TextStyle(
                            color: AppColors.black,
                            height: 1.5,
                            fontSize: 13
                            ),
                            linkStyle: const TextStyle(
                            decoration: TextDecoration.underline
                          ),
                          onLinkTap: (url) async{
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                          onImageTap: (src) {
                            print(src);
                          },
                          customRender: (node, children) {
                            if (node is dom.Element) {
                              switch (node.localName) {
                                case "custom_tag":
                                  return Column(children: children);
                              }
                            }
                            return null;
                          },
                          customTextAlign: (dom.Node node) {
                            if (node is dom.Element) {
                              switch (node.localName) {
                                case "p":
                                  return TextAlign.start;
                                case "h":
                                  return TextAlign.start;
                              }
                            }
                            return null;
                          },
                          customTextStyle: (dom.Node node, TextStyle baseStyle) {
                            if (node is dom.Element) {
                              switch (node.localName) {
                                case "p":
                                  return baseStyle.merge(TextStyle(color: AppColors.black, height: 1.5, fontSize: 13));
                                case "h":
                                  return baseStyle.merge(TextStyle(color: AppColors.black, height: 1.5, fontSize: 13));
                              }
                            }
                            return baseStyle;
                          },
                        ),
                      ),
                      SizedBox(height: 50.0),
                    ],
                  ),
                ),
              ],
            ), 
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.NOTIFICATION);},
                        elevation: 5.0,
                        child: Image.asset(notifImage, fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:5),
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushReplacementNamed(Routes.PROFILEPAGE);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:10)
                  ]
                )
              )
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            )   
          ]
        )
      )
    );
  }
}