import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';

class ContactUsPage extends StatefulWidget {
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {

@override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: AppColors.green,
        title: Text("CONTACT US",
          style: TextStyle(fontFamily: 'Quicksand'),
        ),
        centerTitle: true,
      ),
      body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 80.0,),
                  Text("Krispy Kreme & Prints Head Office",
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                      color: AppColors.green
                    )
                  ),
                  SizedBox(height: 30),
                  Text("From Monday to Friday",
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 15.0,
                      color: AppColors.green
                    )
                  ),
                  Text("10:00 a.m. to 05:00 p.m.",
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 15.0,
                      color: AppColors.green
                    )
                  ),
                  Text("Telephone No.: 372-6190",
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 15.0,
                      color: AppColors.green
                    )
                  ),
                  Text("Email: customersupport@plainsandprints.com",
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 15.0,
                      color: AppColors.green
                    )
                  ),
                  SizedBox(height: 30.0),
                  Text("www.plainsandprints.com",
                    style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 15.0,
                      color: AppColors.green
                    )
                  ),
                ],
              )
            )
    );
  }
}