import 'package:flutter/material.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/slider/intro_slider.dart';
import 'package:krispykreme/slider/slide_object.dart';

class GettingStarted extends StatefulWidget {
  @override
  GettingStartedState createState() => new GettingStartedState();
}

class GettingStartedState extends State<GettingStarted> {

  List<Slide> slides = new List();
  Function goToTab;

  @override
  void initState() {
    super.initState();
    slides.add(new Slide(pathImage: "assets/images/bg1.jpg"));
    slides.add(new Slide(pathImage: "assets/images/bg2.jpg"));
    slides.add(new Slide(pathImage: "assets/images/bg4.jpg"));
    slides.add(new Slide(pathImage: "assets/images/bg4.jpg"));
    slides.add(new Slide(pathImage: "assets/images/bg4.jpg"));
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: Colors.white,
    );
  }

  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = new List();
    tabs.add(
      Container(
        child: GestureDetector(child: Image.asset("assets/image/sp1.png", fit: BoxFit.fitHeight)))
    );
    tabs.add(
      Container(
        child: GestureDetector(child: Image.asset("assets/image/sp2.png", fit: BoxFit.fitHeight)))
    );
    tabs.add(
      Container(
        child: GestureDetector(child: Image.asset("assets/image/sp3.png", fit: BoxFit.fitHeight)))
    );
    tabs.add(
      Container(
        child: GestureDetector(child: Image.asset("assets/image/sp4.png", fit: BoxFit.fitHeight)))
    );
    tabs.add(
      Container(
        child: GestureDetector(child: Image.asset("assets/image/sp5.png", fit: BoxFit.fitHeight)))
    );
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: IntroSlider(
          slides: this.slides,
          onactivatePress: this.onActivatePress,
          onloginPress: this.onLoginPress,
          sizeDot: 10.0,
          listCustomTabs: this.renderListCustomTabs(),
          refFuncGoToTab: (refFunc) {
            this.goToTab = refFunc;
          },
        )
      )
    );
  }

  void onActivatePress() {
    Navigator.pushNamed(context, Routes.REGISTRATION_QUESTION);
  }

  void onLoginPress() {
    Navigator.pushNamed(context, Routes.LOGIN_QUESTION_PAGE);
  }

}