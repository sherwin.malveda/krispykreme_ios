import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/request/forgot_password_password.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/forgot_password_reset_page.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/input_field.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  final TextEditingController _emailController = TextEditingController();
  StreamSubscription stateSubs;
  AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    stateSubs = _authBloc.forgotPasswordState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    stateSubs.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            ListView(
              padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.height * .04,
                MediaQuery.of(context).size.height * .15,
                MediaQuery.of(context).size.height * .04,
                MediaQuery.of(context).size.height * .02,
              ),
              children: <Widget>[
                Container(
                  height: 80.0,
                  child: Image.asset("assets/image/logo.png", fit: BoxFit.contain,)
                ), 
                SizedBox(height: 50.0),
                Container(
                  child: Text(
                    "Forgot your password?",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 10.0),
                Container(
                  child: Text(
                    "We'll reset your password and\n send it to your email!",
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Quicksand',
                      color: AppColors.black
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 30.0),
                Container(
                  height: 50,
                  child:InputField(
                    hintText: "Username",
                    keyboardType: TextInputType.emailAddress,
                    controller: _emailController,
                    submit: (value){
                    },
                  )
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 60.0,
                  child: StreamBuilder<ScreenState>(
                    stream: _authBloc.forgotPasswordState,
                    builder: (context, snapshot) => _buildResetButton(snapshot)
                  )
                ),
                SizedBox(height: 10.0),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                  height: 40,
                  child: Material(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.transparent,
                    child: MaterialButton(
                      child: Container(
                        decoration: new BoxDecoration(
                          border: Border(bottom: BorderSide(color: AppColors.green))
                        ),
                        child: Text("Back to Login",
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            color: AppColors.green,
                            fontSize: 15.0
                          )
                        ),
                      ),
                      onPressed: (){Navigator.pop(context);},
                    ),
                  )
                ),
              ],
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png", color: AppColors.green),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            )
          ],
        ) 
      )
    );
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _navigateToPasswordSuccessPage();
        break;
      case States.ERROR:
        errorMessage(state.error.toString(), context);
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  Widget _buildResetButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child:CircularProgressIndicator(),
            onPressed: (){},
          ),
        )
      );
    } else {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal:30, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child:Text("Reset Password", 
              style: TextStyle(fontSize: 20.0, color: AppColors.white),textAlign: TextAlign.center,),
            onPressed: _resetPassword,
          ),
        )
      );
    }
  }

  void _navigateToPasswordSuccessPage() =>
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) =>
        ForgotPasswordResetPage(
          username: _emailController.text,
        )
      )
    );

  void _resetPassword() {
    if (_emailController.text.isEmpty) return;
    final ForgotPasswordRequest request = ForgotPasswordRequest(email: _emailController.text);
    _authBloc.forgotPassword(request);
  }

}