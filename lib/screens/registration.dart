// import 'dart:async';
// import 'package:flushbar/flushbar.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:krispykreme/app_colors.dart';
// import 'package:krispykreme/blocs/auth_bloc.dart';
// import 'package:krispykreme/blocs/master_provider.dart';
// import 'package:krispykreme/constants.dart';
// import 'package:krispykreme/form_validators.dart';
// import 'package:krispykreme/model/request/verification_request.dart';
// import 'package:krispykreme/model/user.dart';
// import 'package:krispykreme/routes.dart';
// import 'package:krispykreme/screens/screen_state.dart';
// import 'package:krispykreme/widgets/error.dart';
// import 'package:krispykreme/widgets/input_field.dart';
// import 'package:krispykreme/widgets/radioButton.dart';
// import 'package:krispykreme/widgets/selector_field.dart';

// class RegistrationPage extends StatefulWidget {
//   @override
//   _RegistrationPageState createState() => _RegistrationPageState();
// }

// class _RegistrationPageState extends State<RegistrationPage> {

//   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
 
//    final TextEditingController _usernameController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final TextEditingController _confirmPasswordController = TextEditingController();
//   final TextEditingController _addressController = TextEditingController();
//   final TextEditingController _cardNumberController = TextEditingController();
//   final TextEditingController _fullNameController = TextEditingController();
//   final TextEditingController _mobileNumberController = TextEditingController();
//   final TextEditingController _emailController = TextEditingController();

//   final FocusNode usernameFocusNode = new FocusNode();
//   final FocusNode passwordFocusNode = new FocusNode();
//   final FocusNode confirmpasswordFocusNode = new FocusNode();
//   final FocusNode addressFocusNode = new FocusNode();
//   final FocusNode cardNumberFocusNode = new FocusNode();
//   final FocusNode fullNameFocusNode = new FocusNode();
//   final FocusNode mobileNumberFocusNode = new FocusNode();
//   final FocusNode emailFocusNode = new FocusNode();
  
//   AuthBloc _authBloc;
//   StreamSubscription _stateSubs;
//   StreamSubscription _registrationCredsSub;

//   DateTime _birthday;
//   String gender = "Male";
//   bool checkBox = false;
//   int _radioValue = 0;

//   @override
//   void didChangeDependencies() {
//     _authBloc = MasterProvider.auth(context);
//     if (_authBloc.registerState.hasListener) _stateSubs?.cancel();
//     _stateSubs = _authBloc.registerState.listen(onStateChanged);
//     if (_authBloc.registrationCredentials.hasListener)
//       _registrationCredsSub?.cancel();
//     _registrationCredsSub =
//         _authBloc.registrationCredentials.listen(onRegistrationCredentials);
//     super.didChangeDependencies();
//   }

//   @override
//   void dispose() {
//     _stateSubs.cancel();
//     super.dispose(); 
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: Container(
//         child: Form(
//           key: _formKey,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: <Widget>[
//               SizedBox(height: 50.0,),
//               Container(
//                 height: 130.0,
//                 child: Image.asset("assets/images/logo.png", fit: BoxFit.contain,)
//               ),
//               Expanded(
//                 child: Container(
//                   padding: EdgeInsets.symmetric(horizontal:25),
//                   child: ListView(
//                     children: <Widget>[
//                       SizedBox(height: 5.0),
//                       Container(
//                         child: Text(
//                           "Login Details",
//                           style: TextStyle(
//                             fontSize: 17,
//                             fontWeight: FontWeight.bold,
//                             fontFamily: 'Quicksand',
//                             color: AppColors.green
//                           ),
//                           textAlign: TextAlign.center,
//                         ),
//                       ),
//                       SizedBox(height: 15.0),
//                       Expanded(
//                         child: InputField(
//                           focusNode: usernameFocusNode,
//                           hintText: "Username",
//                           autoValidate: true,
//                           validator: _validateUsername,
//                           keyboardType: TextInputType.text,
//                           controller: _usernameController,
//                           submit: (value){
//                             FocusScope.of(context).requestFocus(passwordFocusNode);
//                           },
//                         )
//                       ),
//                       SizedBox(height: 8.0),
//                      Expanded(
//                         child:InputField(
//                           focusNode: passwordFocusNode,
//                           hintText: "Password",
//                           autoValidate: true,
//                           keyboardType: TextInputType.text,
//                           controller: _passwordController,
//                           masked: true,
//                           submit: (value){
//                             FocusScope.of(context).requestFocus(confirmpasswordFocusNode);
//                           },
//                         )
//                       ),
//                       SizedBox(height: 8.0),
//                       Expanded(
//                         child:InputField(
//                           focusNode: confirmpasswordFocusNode,
//                           hintText: "Confirm Password",
//                           autoValidate: true,
//                           validator: _validateConfirmPassword,
//                           keyboardType: TextInputType.text,
//                           controller: _confirmPasswordController,
//                           masked: true,
//                           submit: (value){
//                             FocusScope.of(context).requestFocus(cardNumberFocusNode);
//                           },
//                         )
//                       ),
//                       SizedBox(height: 25.0),
//                       Container(
//                         child: Text(
//                           "OG Card Details",
//                           style: TextStyle(
//                             fontSize: 17,
//                             fontWeight: FontWeight.bold,
//                             fontFamily: 'Quicksand',
//                             color: AppColors.green
//                           ),
//                           textAlign: TextAlign.center,
//                         ),
//                       ),
//                       SizedBox(height: 15.0),
//                       Expanded(
//                         child: InputField(
//                           focusNode: cardNumberFocusNode,
//                           hintText: "Card Number",
//                           autoValidate: true,
//                           validator: _validateNumber,
//                           keyboardType: TextInputType.number,
//                           controller: _cardNumberController,
//                           submit: (value){
//                             FocusScope.of(context).requestFocus(fullNameFocusNode);
//                           },
//                         )
//                       ),
//                       SizedBox(height: 8.0),
//                       Expanded(
//                         child: InputField(
//                           focusNode: fullNameFocusNode,
//                           hintText: "Full Name",
//                           autoValidate: true,
//                           validator: _validateName,
//                           keyboardType: TextInputType.text,
//                           controller: _fullNameController,
//                           submit: (value){
//                             FocusScope.of(context).requestFocus(mobileNumberFocusNode);
//                           },
//                         )
//                       ),
//                       SizedBox(height: 8.0),
//                       Expanded(
//                         child: InputField(
//                           focusNode: mobileNumberFocusNode,
//                           hintText: "Mobile Number",
//                           autoValidate: true,
//                           validator: _validateNumber,
//                           maxLength: 11,
//                           keyboardType: TextInputType.text,
//                           controller: _mobileNumberController,
//                           submit: (value){
//                             FocusScope.of(context).requestFocus(emailFocusNode);
//                           },
//                         )
//                       ),
//                       SizedBox(height: 8.0),
//                       Expanded(
//                         child:InputField(
//                           focusNode: emailFocusNode,
//                           hintText: "Email Address",
//                           autoValidate: true,
//                           validator: _validateEmail,
//                           keyboardType: TextInputType.emailAddress,
//                           controller: _emailController,
//                           submit: (value){
//                             FocusScope.of(context).requestFocus(addressFocusNode);
//                           },
//                         )
//                       ),
//                       SizedBox(height: 25.0),
//                       Container(
//                         child: Text(
//                           "Personal Information",
//                           style: TextStyle(
//                             fontSize: 17,
//                             fontWeight: FontWeight.bold,
//                             fontFamily: 'Quicksand',
//                             color: AppColors.green
//                           ),
//                           textAlign: TextAlign.center,
//                         ),
//                       ),
//                       SizedBox(height: 15.0),
//                       Container(
//                         height: 100,
//                         child:TextFormField(
//                           focusNode: addressFocusNode,
//                           style: TextStyle(fontFamily: 'Quicksand'),
//                           controller: _addressController,
//                           maxLines: 10,
//                           decoration: InputDecoration(
//                             counterText: '',
//                             labelText: "Address",
//                             contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
//                             border:
//                               OutlineInputBorder(borderRadius: BorderRadius.circular(20.0),
//                             )
//                           ),
//                         ),
//                       ),
//                       SizedBox(height: 18.0),
//                       Container(
//                         child: Text(
//                           "Gender",
//                           style: TextStyle(
//                             fontSize: 14,
//                             fontWeight: FontWeight.bold,
//                             fontFamily: 'Quicksand',
//                             color: AppColors.black
//                           ),
//                           textAlign: TextAlign.start,
//                         ),
//                       ),
//                       Container(
//                         height: 100,
//                         child: Column(
//                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             SizedBox(
//                               height: 20,
//                               child: LabeledRadio(
//                                 label: 'Male',
//                                 padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
//                                 value: 0,
//                                 groupValue: _radioValue,
//                                 onChanged: _handleRadioValueChange
//                               ),
//                             ),
//                             SizedBox(
//                               height: 20,
//                               child:LabeledRadio(
//                                 label: 'Female',
//                                 padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
//                                 value: 1,
//                                 groupValue: _radioValue,
//                                 onChanged: _handleRadioValueChange
//                               ),
//                             ),
//                             SizedBox(
//                               height: 20,
//                               child:LabeledRadio(
//                                 label: 'I do not want to specify',
//                                 padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
//                                 value: 2,
//                                 groupValue: _radioValue,
//                                 onChanged: _handleRadioValueChange
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                       SizedBox(height: 8.0),
//                       Container(
//                         height: 45,
//                         child: SelectorField(
//                         onPressed: () { _showDatetimePicker();},
//                         child: Text("${_birthday == null ? "Birthday" : _birthday.toString().substring(0,10)}")
//                         ),
//                       ), 
//                       SizedBox(height: 15.0),
//                       SizedBox(
//                         height: 20,
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: <Widget>[
//                             Checkbox(
//                               value: checkBox,
//                               activeColor: AppColors.green,
//                               onChanged: (value) {
//                                 setState(() {
//                                   checkBox = value;
//                                 });
//                               },
//                             ),
//                             Row(
//                               children: <Widget>[
//                                 Text("I have read and accepted the",
//                                   style: TextStyle(color:Colors.black,fontSize:8, fontFamily: 'Quicksand'),),
//                                 FlatButton(
//                                   padding: EdgeInsets.all(0.0),
//                                   child: Text(" Terms & Conditions",
//                                     style: TextStyle(color:Colors.black,
//                                       fontSize: 9,
//                                       fontFamily: 'Quicksand',
//                                       fontStyle: FontStyle.italic,
//                                       fontWeight: FontWeight.bold
//                                     ),
//                                   ),
//                                   onPressed: () => Navigator.of(context).pushNamed(Routes.TERMS_AND_CONDITIONS)
//                                 )
//                               ],
//                             ),
//                           ],
//                         ),
//                       ),
//                       SizedBox(height: 40.0),
//                       Container(
//                         height: 60.0,
//                         child: StreamBuilder<ScreenState>(
//                           stream: _authBloc.registerState,
//                           builder: (context, snapshot) {
//                             return _buildSubmitButton(snapshot);
//                           }
//                         ),
//                       ),
//                       SizedBox(height: MediaQuery.of(context).size.height * .02),
//                       Container(
//                         height: 40,
//                         child: Material(
//                           borderRadius: BorderRadius.circular(15.0),
//                           color: AppColors.white,
//                           child: MaterialButton(
//                             child: Container(
//                               decoration: new BoxDecoration(
//                                 border: Border(bottom: BorderSide(color: AppColors.green))
//                               ),
//                               child: Text("Back to Login",
//                                 style: TextStyle(
//                                   fontFamily: 'Quicksand',
//                                   color: Colors.black,
//                                   fontSize: 15.0
//                                 )
//                               ),
//                             ),
//                             onPressed: (){_navigateToLoginPage(context);},
//                           ),
//                         )
//                       ),
//                       SizedBox(height: MediaQuery.of(context).size.height * .05),
//                     ],
//                   ),
//                 )
//               )
//             ],
//           ),
//         ),
//       )
//     ); 
//   }

//   Widget _buildSubmitButton(AsyncSnapshot<ScreenState> snapshot) {
//     if (snapshot.hasData && snapshot.data.state == States.WAITING) {
//       return Container(
//         margin: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
//         child: Material(
//           elevation: 5.0,
//           borderRadius: BorderRadius.circular(15.0),
//           color: AppColors.green,
//           child: MaterialButton(
//             child:CircularProgressIndicator(),
//             onPressed: (){},
//           ),
//         )
//       );
//     } else {
//       return Container(
//         height: 50,
//         margin: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
//         child: Material(
//           elevation: 5.0,
//           borderRadius: BorderRadius.circular(15.0),
//           color: AppColors.green,
//           child: MaterialButton(
//             height: 50,
//             padding: EdgeInsets.zero,
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(15.0),
//             ),
//             child:Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Expanded(
//                 child: Text("Activate Card", 
//                   style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center),
//                 ),
//                 Container(
//                   height: 50,
//                   width: 50,
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.only(
//                       bottomRight:  const  Radius.circular(15.0),
//                       topRight: const  Radius.circular(15.0)
//                     ),
//                     color: Colors.black12
//                   ),
//                   alignment: Alignment.center,
//                   child: Text("1 of 2", 
//                     style: TextStyle(fontSize: 13.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center),            
//                   )
//                 ],
//               ),
//               onPressed: _onSubmit,
//           ),
//         )
//       );
//     }
//   }

//   void onStateChanged(ScreenState state) {
//     switch (state.state) {
//       case States.DONE:
//         break;
//       case States.ERROR:
//         errorMessage(state.error.toString(), context);
//         break;
//       case States.IDLE:
//         break;
//       case States.WAITING:
//         break;
//     }
//   }

//   String _validateUsername(value) =>
//       FormValidators.isAlphanumeric(value,
//           "Username must be at least 3 characters long,\nmaximum of 16 characters and must be alpha-numeric.");

//  String validatePassword(value) =>
//       FormValidators.isPassword(
//           _passwordController.text, "Password must be 4 characters long,\n and must not be weak.");

//  String _validateConfirmPassword(value) =>
//       FormValidators.isIdentical(
//           _passwordController.text, value, "Confirm password does not match.");

//   String _validateName(String value) =>
//       FormValidators.isLettersOnly(value, "Enter a valid name");

//   String _validateNumber(String value) => 
//       FormValidators.isMobileNumber(value, "Enter a valid mobile number");
  
//   String _validateEmail(String value) =>
//       FormValidators.isValidEmail(value, "Enter a valid email");

// void _handleRadioValueChange(int value) {
//     setState(() {
//       _radioValue = value;
 
//       switch (_radioValue) {
//         case 0:
//           gender = "Male";
//           break;
//         case 1:
//           gender = "Female";
//           break;
//         case 2:
//           gender = "I don't want to specify";
//           break;
//       }
//     });
//   }

//   void _showDatetimePicker() async {
//     if(_birthday == null){
//       _showDateDialog(DateTime.parse('2000-06-15'));
//     }else{
//       _showDateDialog(DateTime.parse(DateFormat('yyyy-MM-dd').format(_birthday)));
//     }
//   }

//   void _showDateDialog(DateTime initDateTime) {
//     // showDialog(
//     //   context: context,
//     //   builder: (BuildContext context) {
//     //     return AlertDialog(
//     //       contentPadding: EdgeInsets.all(10),
//     //       shape: RoundedRectangleBorder(
//     //         borderRadius: new BorderRadius.circular(10.0),
//     //         side: BorderSide(color: Colors.white)
//     //       ),
//     //       content: Container(
//     //         height: 280,
//     //         child: DatePickerWidget(
//     //           minDateTime: DateTime.parse(DateTime.now().subtract(Duration(days: 36500)).toString()),
//     //           maxDateTime: DateTime.parse(DateTime.now().subtract(Duration(days: 6570)).toString()),
//     //           initialDateTime: initDateTime,
//     //           dateFormat: "MMM dd,yyyy",
//     //           pickerTheme: DateTimePickerTheme(
//     //             backgroundColor: Colors.white,
//     //             cancelTextStyle: TextStyle(color: Colors.red),
//     //             confirmTextStyle: TextStyle(color: AppColors.green),
//     //             itemTextStyle: TextStyle(color: AppColors.green, fontSize: 16.0),
//     //             pickerHeight: 250.0,
//     //             titleHeight: 25.0,
//     //             itemHeight: 30.0,
//     //           ),
//     //           onConfirm: (dateTime, selectedIndex) {
//     //             setState(() {
//     //               _birthday = dateTime;
//     //             });
//     //           },
//     //         ),
//     //       ),
//     //     );
//     //   },
//     // );
//   }

//   void _onSubmit() {
//     final String fullName = _fullNameController.text;
//     final String mobileNumber = _mobileNumberController.text.isNotEmpty
//         ? "+63${_mobileNumberController.text}"
//         : "";
//     final String email = _emailController.text;
//     final String userName = _usernameController.text;
//     final String password = _passwordController.text;
//     final DateTime birthday = _birthday;
//     final bool subscription =  checkBox;

//     final User user = User(
//       firstname: fullName,
//       lastName: "lastName",
//       middleName: "middleName",
//       mobile: mobileNumber,
//       address: "null",
//       zipCode: "null",
//       username: userName,
//       password: password,
//       email: email,
//       birthDate: birthday,
//       gender: 1,
//       title: 1,
//       maritalStatus: 1,
//       city: 1,
//       province: 1,
//       cardDigits: "",
//       subscription: subscription,
//     );

//     if (!user.hasNoNullsForRequiredField()) {
//       errorMessage("Fill up all the required fields", context);
//       return;
//     }

//     if(_passwordController.text != _confirmPasswordController.text){
//       errorMessage("Confirm password does not match", context);
//       return;
//     }

//     if(checkBox == false){
//       errorMessage("Please accept the terms and conditions.", context);
//       return;
//     }

//     if (_formKey.currentState.validate()) {
//       _authBloc.register(user);
//     }
//   }


//   void _navigateToLoginPage(BuildContext context) {
//      Navigator.of(context).pushReplacementNamed(Routes.LOGIN);
//   }

//   void onRegistrationCredentials(VerificationRequest credentials) {
//     //_navigateToEmailConfirmation(credentials);
//   }

// }
