import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/card_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/db/dbHelper.dart';
import 'package:krispykreme/db/insert.dart';
import 'package:krispykreme/main.dart';
import 'package:krispykreme/model/offline_data_model/voucheroffline.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/model/voucher.dart';
import 'package:krispykreme/model/vouchers.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/arguments/voucher_details_arguments.dart';
import 'package:krispykreme/screens/voucher_details_page.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/voucher_list.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class VouchersPage extends StatefulWidget {
  @override
  _VouchersPageState createState() => _VouchersPageState();
}

class _VouchersPageState extends State<VouchersPage> {

  CardBloc _cardBloc;
  AuthBloc _authBloc;
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  void didChangeDependencies() {
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            Container(
              padding: EdgeInsets.fromLTRB(0, 190, 0, 0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage('assets/image/bg.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child:StreamBuilder(
                stream: cardBloc.vouchers,
                builder: (BuildContext context, AsyncSnapshot<Vouchers> snapshot) {
                  return StreamHandler(
                    snapshot: snapshot,
                    loading: Center(child: CircularProgressIndicator()),
                    noData: SmartRefresher(
                      enablePullDown: true,
                      header: ClassicHeader(
                        refreshingText: "Refreshing",
                        textStyle: TextStyle(fontFamily: 'Quicksand'),
                      ),   
                      controller: _refreshController,
                      onRefresh:()=> _onRefresh(0),
                      onLoading:()=> _onLoading(0),
                      child: Center(child: Text("No vouchers at the moment.",style: TextStyle(fontFamily: 'Quicksand'))),
                    ),
                    withError: (error) => SmartRefresher(
                      enablePullDown: true,
                      header: ClassicHeader(
                        refreshingText: "Refreshing",
                        textStyle: TextStyle(fontFamily: 'Quicksand'),
                      ),
                      controller: _refreshController,
                      onRefresh:()=> _onRefresh(0),
                      onLoading:()=> _onLoading(0),
                      child: Center(child: Text("No vouchers at the moment.",style: TextStyle(fontFamily: 'Quicksand'))),
                    ),
                    withData: (Vouchers vouchers) {
                      return SmartRefresher(
                        enablePullDown: true,
                        enablePullUp: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand'),
                        ),
                        footer: CustomFooter(
                          builder: (BuildContext context,LoadStatus mode){
                            Widget body ;
                            if(mode==LoadStatus.loading){body =  CupertinoActivityIndicator();}
                            else{body = Text("No more items", style: TextStyle(fontFamily: 'Quicksand'),);}
                            return Container(
                              height: 55.0,
                              child: Center(child:body),
                            );
                          },
                        ),       
                        controller: _refreshController,
                        onRefresh:()=> _onRefresh(vouchers.vouchers.length),
                        onLoading:()=> _onLoading(vouchers.vouchers.length),
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: vouchers.vouchers.length,
                          itemBuilder: (BuildContext context, int index) {
                            return  VoucherList(
                              voucher: vouchers.vouchers[index].description,
                              type: vouchers.vouchers[index].voucherType,
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) =>
                                    VoucherDetailsPage(
                                      voucherId: vouchers.vouchers[index].voucherId,
                                    )
                                  )
                                );
                              } 
                            );
                          }
                        ),
                      );
                    },
                  );
                }
              )  
            ),  
            Positioned(
              top: 140,
              left: 0,
              right: 0,
              child: Center(
                child: Container(
                  child: Text("E-VOUCHERS",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: AppColors.green
                    ),
                  )
                )
              )
            )
          ],
        ) 
      )
    );
  }

  void _onRefresh(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      try{
        _cardBloc.getVouchers(ListRequest(
            accountId: _authBloc.session.value.accountId,
            rows: MAX_ROW_LIST,
            page: (currentCount ~/ MAX_ROW_LIST) + 1)
        );
      }catch(Exception){}
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      try{
        _cardBloc.getVouchers(ListRequest(
            accountId: _authBloc.session.value.accountId,
            rows: MAX_ROW_LIST,
            page: (currentCount ~/ MAX_ROW_LIST) + 1)
        );
      }catch(Exception){}
    });
    _refreshController.refreshCompleted();
  }

}
