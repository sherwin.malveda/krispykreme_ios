import 'dart:async';
import 'dart:convert';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/request/login_request.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginQuestionPage extends StatefulWidget {
  @override
  _LoginQuestionPageState createState() => _LoginQuestionPageState();
}

class _LoginQuestionPageState extends State<LoginQuestionPage> {

  bool fbLogin = false;
  String facebookEmail;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  StreamSubscription stateSubs;
  AuthBloc _authBloc;

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    stateSubs = _authBloc.loginwithFbState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    stateSubs.cancel();
    super.dispose();
  }

  @override
  void initState(){
    getFCMToken();
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            ListView(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.height * .04,
                vertical: MediaQuery.of(context).size.height * .15
              ),
              children: <Widget>[
                Container(
                  height: 80.0,
                  child: Image.asset("assets/image/logo.png", fit: BoxFit.contain,)
                ), 
                SizedBox(height: MediaQuery.of(context).size.height * .1),
                Container(
                  child: Text(
                    "Already an OG Member?",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .07),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(width: 10.0), 
                          Container(
                            height: 20,
                            child: Image.asset("assets/icons/email.png", fit: BoxFit.contain),
                          ),
                          SizedBox(width: 15.0), 
                          Text("Login with Email", 
                            style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                        ],
                      ),
                      onPressed: (){
                        _navigateToLoginPage();
                      },
                    ),
                  )
                ),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.blue,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(width: 10.0), 
                          Container(
                            height: 20,
                            child: Image.asset("assets/icons/facebook.png", fit: BoxFit.contain),
                          ),
                          SizedBox(width: 15.0), 
                          Text("Login with Facebook", 
                            style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                        ],
                      ),
                      onPressed: (){
                        signInWithFacebook();
                      },
                    ),
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                  height: 40,
                  child: Material(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.transparent,
                    child: MaterialButton(
                      child: Container(
                        decoration: new BoxDecoration(
                          border: Border(bottom: BorderSide(color: AppColors.green))
                        ),
                        child: Text("or Activate your OG Card",
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            color: AppColors.green,
                            fontSize: 15.0
                          )
                        ),
                      ),
                      onPressed: (){_navigateToRegistrationPage();},
                    ),
                  )
                ),
              ],
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png", color: AppColors.green),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            )
          ],
        ) 
      )
    );
  }

  void onStateChanged(ScreenState state) {
  switch (state.state) {
      case States.DONE:
        if(fbLogin == true){  
          Navigator.pop(context);
          _navigateToDashboardPage();
        }
        break;
      case States.ERROR:
        if(fbLogin == true){
          _handleError(state);
        }
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _handleError(ScreenState state) {
    Navigator.pop(context);
    _showErrorMessage();
  }

  void signInWithFacebook() async {  

    final facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.nativeOnly;  
    final facebookLoginResult = await facebookLogin.logIn(['email']);

    try{
      switch (facebookLoginResult.status) {
        case FacebookLoginStatus.loggedIn:
          var graphResponse = await http.get(
          'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.width(800).height(800)&access_token=${facebookLoginResult.accessToken.token}');
          var profile = json.decode(graphResponse.body);
          print(profile.toString());
          facebookEmail = profile['email'];
          _showConfirmDialog(profile['email'], profile['name'], profile['picture']['data']['url'],);
          break;
        case FacebookLoginStatus.cancelledByUser:
          errorMessage("Facebook login cancelled by the user!", context);
          break;
        case FacebookLoginStatus.error:
          errorMessage('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${facebookLoginResult.errorMessage}', context);
          break;
      }
    }catch(Exception){
      errorMessage(Exception.toString(), context);
    }
  }  

  void _showConfirmDialog(String email, String username, String url) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .55,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .01),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(
                    height: 30,
                    width: 30,
                    child: RawMaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      elevation: 5.0,
                      child: Image.asset("assets/icons/close.png", fit: BoxFit.cover,),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                  SizedBox(width: 15),
                  ],
                ),
                Container(
                  alignment: Alignment.center,
                  height: 120.0,
                  width: 120.0,
                  child: CircularProfileAvatar(
                    url, //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
                    radius: 100, // sets radius, default 50.0              
                    backgroundColor: Colors.transparent, // sets background color, default Colors.white
                    borderWidth: 2,  // sets border, default 0.0
                    borderColor: AppColors.green, // sets border color, default Colors.white
                    elevation: 5.0, // sets elevation (shadow of the profile picture), default value is 0.0
                    cacheImage: true, // allow widget to cache image against provided url
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  child: Text(
                    username,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  child: Text(
                    "Continue login using \n"+ email,
                    style: TextStyle(
                      fontSize: 15,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  height: 40.0,
                  child:StreamBuilder<ScreenState>(
                    stream: _authBloc.loginwithFbState,
                    builder: (context, snapshot) => _buildLoginButton(snapshot)
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
              ]
            )
          ),
        );
      },
    );
  }

  void _showErrorMessage() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .35,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Text("Whoops!",
                  style: TextStyle(
                  fontSize: 18.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .01 ),
                Text("It seems like your Facebook account\nis not connected to an OG account"+
                "\n\nYou can try to activate it later\non the profile page!",
                  style: TextStyle(
                  fontSize: 12.0, 
                  color: AppColors.black,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 40,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
                      child:Text("Login via Email", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){
                        Navigator.pop(context);
                        _navigateToLoginPage();
                      },
                    ),
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
              ]
            )
          ),
        );
      },
    );
  }


  Widget _buildLoginButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height:50,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child: CircularProgressIndicator(),
            onPressed: (){},
          ),
        )
      );
    } else {
      return Container(
        height:50,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child:Text("Continue", 
              style: TextStyle(fontSize: 18.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
            onPressed: _login,
          ),
        )
      );
    }
  }

  void _navigateToDashboardPage() {
    savedtoSharedPref();
    Navigator.pushNamedAndRemoveUntil(context, Routes.DASHBOARD, ModalRoute.withName(Routes.DASHBOARD));
  }

  void _navigateToLoginPage() {
    Navigator.pushNamed(context, Routes.LOGIN);
  }

  void _navigateToRegistrationPage() {
    Navigator.pushNamed(context, Routes.REGISTRATION_QUESTION);
  }

  void _login() async{
    fbLogin = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('username', facebookEmail);
    final LoginRequest request = LoginRequest(
      username: facebookEmail, password: "", grantType: "password"
    );
    _authBloc.loginwithFb(request, "facebook", prefs.getString('token'));
  }

  void getFCMToken(){
    _firebaseMessaging.getToken().then((token) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', token);
    });
  }

  savedtoSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(_authBloc.session.value.facebookEmail == "" || _authBloc.session.value.facebookEmail ==null){
      prefs.setBool("IsFacebookUrl", false);
      prefs.setBool("FacebookLink", false);
      prefs.setString("FacebookUrl", "");
    }else{
      prefs.setBool("IsFacebookUrl", true);
      prefs.setBool("FacebookLink", true);
      prefs.setString("FacebookUrl", _authBloc.session.value.facebookUrl);
    }
  }
}
