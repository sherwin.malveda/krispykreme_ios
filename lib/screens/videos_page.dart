import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/blocs/store_bloc.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/promos.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/promos_detailed_page.dart';
import 'package:krispykreme/screens/videos_detailed_page.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/promo_list.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/videos_list.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

class VideosPage extends StatefulWidget {
  @override
  _VideosPageState createState() => _VideosPageState();
}

class _VideosPageState extends State<VideosPage> {

  StoreBloc _storeBloc;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  
  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            Container(
              padding: EdgeInsets.fromLTRB(0, 190, 0, 0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage('assets/image/bg.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child:StreamBuilder(
                stream: _storeBloc.promosvideos,
                builder: (BuildContext context, AsyncSnapshot<Promos> snapshot) {
                  return StreamHandler(
                    snapshot: snapshot,
                    loading: Center(child: CircularProgressIndicator()),
                    noData: SmartRefresher(
                        enablePullDown: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand'),
                        ),
                        controller: _refreshController,
                        onRefresh: () => _onRefresh(0),
                        onLoading: () => _onLoading(0),
                        child: Center(
                        child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                          child: Text("No videos at the moment")
                        )
                      ),
                    ),
                    withError: (error) => SmartRefresher(
                        enablePullDown: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand'),
                        ),
                        controller: _refreshController,
                        onRefresh: () => _onRefresh(0),
                        onLoading: () => _onLoading(0),
                        child: Center(
                        child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                          child: Text("No videos at the moment")
                        )
                      ),
                    ),
                    withData: (Promos promos) {
                      return SmartRefresher(
                        enablePullDown: true,
                        enablePullUp: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Quicksand'),
                        ),
                        footer: CustomFooter(
                          builder: (BuildContext context,LoadStatus mode){
                            Widget body ;
                            if(mode==LoadStatus.loading){body =  CupertinoActivityIndicator();}
                            else{body = Text("No more items", style: TextStyle(fontFamily: 'Quicksand'),);}
                            return Container(
                              height: 55.0,
                              child: Center(child:body),
                            );
                          },
                        ),       
                        controller: _refreshController,
                        onRefresh:()=> _onRefresh(promos.promotions.length),
                        onLoading:()=> _onLoading(promos.promotions.length),
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: promos.promotions.length,
                          itemBuilder: (BuildContext context, int index) {
                            return VideosList(
                              description: promos.promotions[index].promoName,
                              image: promos.promotions[index].promoImage,
                              id: promos.promotions[index].promoId,
                              onTap: () {
                                goToWebsite(promos.promotions[index].promosVideo);
                              } 
                            );
                          }
                        ),
                      );
                    },
                  );
                }
              )  
            ),  
            Positioned(
              top: 140,
              left: 0,
              right: 0,
              child: Center(
                child: Container(
                  child: Text("VIDEOS",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: AppColors.green
                    ),
                  )
                )
              )
            ), 
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushNamed(Routes.NOTIFICATION);},
                        elevation: 5.0,
                        child: Image.asset(notifImage, fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:5),
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.of(context).pushNamed(Routes.PROFILEPAGE);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:10)
                  ]
                )
              )
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            )
          ],
        ) 
      )
    );
  }

  void _onRefresh(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      _storeBloc.getPromosVideos(ListRequest(
        rows: MAX_ROW_LIST,
        page: (currentCount ~/ MAX_ROW_LIST) + 1)
      );
    });
    _refreshController.refreshCompleted();
  }

  void _onLoading(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      _storeBloc.getPromosVideos(ListRequest(
        rows: MAX_ROW_LIST,
        page: (currentCount ~/ MAX_ROW_LIST) + 1)
      );
    });
    _refreshController.loadComplete();
  }

  goToWebsite(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      errorMessage('Could not launch $url', context);
    }
  }

}