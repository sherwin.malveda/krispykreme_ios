import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/db/insert.dart';
import 'package:krispykreme/routes.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {

  AuthBloc _authBloc;

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  void initState() {    
    super.initState();
    checkPermission();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 1.0,
            width: MediaQuery.of(context).size.height * 1.0,
            child: Image.asset('assets/image/sp_bg.png', fit: BoxFit.cover,)
          ),
          Positioned(
            bottom: 12.0,
            left: 0,
            right: 0,
            child: Text("v1.0.0\nSIT_13102020\nOnly in the Philippines",
              textAlign: TextAlign.center,
             style: TextStyle(
              fontSize: 11,
              fontWeight: FontWeight.bold,
              color: AppColors.black
            ),
          ),
          )
        ],
      )
    );
  }
  
  Future checkSession(BuildContext context) async {
    if (await _authBloc.hasSession()) {
      Navigator.of(context).pushReplacementNamed(Routes.DASHBOARD);
    } else {
      Navigator.of(context).pushReplacementNamed(Routes.LANDING);
    }
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 5), onDoneLoading);
  }

  onDoneLoading() async {
    final count = await dbHelper.selectAllData("KrispyKreme");
    if (count.length == 0) {
      Insert.insertSplashScreen();
      Navigator.of(context).pushReplacementNamed(Routes.GETTING_STARTED);
    } else {
      checkSession(context);
    }
  }

  checkPermission() async{
    if (await Permission.location.request().isGranted && await Permission.storage.request().isGranted) {
      Location location = new Location();    
      bool _serviceEnabled;

      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        await location.requestService().then((value) => value);
      } 
      return;
    }

    await [
      Permission.location,
      Permission.locationAlways,
      Permission.locationWhenInUse,
      Permission.storage
    ].request();
  }
}
