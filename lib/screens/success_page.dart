import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/screens/arguments/success_arguments.dart';
import 'package:krispykreme/widgets/accent_button.dart';

class SuccessPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final SuccessArguments successArguments =ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: AppColors.green,
        title: Text("Success", style: TextStyle(fontFamily: 'Quicksand')),
        elevation: 0.0,
        brightness: Brightness.light,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 80.0),
          Icon(Icons.done, size: 200.0,color: AppColors.green,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 24.0),
              successArguments.text,
              SizedBox(height: 24.0),
              AccentButton(
                child: successArguments.buttonText,
                onPressed: () => Navigator.of(context).pushReplacementNamed(successArguments.route),
              )
            ]
          )
        ],
      ),
    );
  }
}