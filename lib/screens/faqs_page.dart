import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/widgets/faqs_item.dart';

class FAQsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            Container(
              padding: EdgeInsets.fromLTRB(0, 190, 0, 0),
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: AssetImage('assets/image/bg.png'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                child: ListView(
                  padding: EdgeInsets.all(0),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 0.0),
                      child: Text(
                        "OG Card",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "Glaze into the happy world of Krispy Kreme with the OG Card! OG Card is a lifestyle card that will give you access to the most awesome experiences and will let you score lots of exclusive treats made for an Original like you.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "Pre-OG Card Registration FAQs",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "\t•	How can I avail the OG Card?",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tThe OG Card is available in all Krispy Kreme stores nationwide for only P195.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	What are the perks of the OG Card?",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\t OG Card members can enjoy the following:"+
                            "\n\t\t\t\t\t\t\t\t\t\t\t-	Get a box of 6 Original Glazed® Doughnuts when you purchase a dozen Original Glazed® Doughnuts *Maximum of six (6) free box of 6 Original Glazed® Doughnuts per calendar month"+
                            "\n\t\t\t\t\t\t\t\t\t\t\t-	Get a Box of 3 Original Glazed® Doughnuts on your birthday month"+
                            "\n\t\t\t\t\t\t\t\t\t\t\t-	Receive exclusive invites to Krispy Kreme happenings"+
                            "\n\t\t\t\t\t\t\t\t\t\t\t-	Get first dibs on item releases and store updates"+
                            "\n\t\t\t\t\t\t\t\t\t\t\t-	Avail special product promotions and offers"+
                            "\n\t\t\t\t\t\t\t\t\t\t\t-	Get a box of 6 Original Glazed® Doughnuts when you register your account on originals.krispykreme.com.ph"+
                            "",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Where can I buy the OG Card?",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tThe OG Card is available in all Krispy Kreme stores nationwide. NOW Available through delivery in https://now.krispykreme.com.ph/, 888-79000, GrabFood, LalaFood and FoodPanda.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Where can I use the OG Card?",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tIt can be used in all Krispy Kreme stores except airport stores, Makati Medical Center, Manila Doctors Hospital and SM MOA Arena.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                        ]
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "Post-OG Card Activation/Registration FAQs",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "\t• What if I input the wrong information (Name, Contact number and email address) during in-store activation? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tKindly leave your OG Card number and correct details so we can rectify it.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Until when can I register my activated card? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\t It can be registered anytime you wish.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Until when can I redeem my free Box of 6 OG upon registration? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tIt can be claimed anytime you wish.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Where can I redeem my free Box of 6 OG? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tIt can be claimed in all Krispy Kreme stores except airport stores, Makati Medical Center, Manila Doctors Hospital and SM MOA Arena. ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Can I redeem my freebie even with the OG Card? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tCard must be shown during transaction.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                        ]
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "Normal Perk FAQs",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "\t•	When can I claim their free Box of 6 OG normal perk? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tThe guest CAN NOT claim their free box of 6 OG on the day of dozen OG purchase. It can be claimed within the calendar month of purchase.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Does the free Box of 6 OG normal perk expires?",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tThe sales voucher code strictly follows calendar-month so when we crossover to a new month, the QR code will expire. Rewards are not accumulative and cannot be carried over to the next calendar month.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	How many times can I avail of the OG Card normal perk? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tMaximum of 6 free box of 6 OG per month. If guest exceeds the said quantity, the terminal will automatically block any sales, redemption transactions.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Can I upgrade their doughnut to Assorted?  ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tNo, there should be no changes in our offer, only Original Glazed® Doughnuts.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	How many dozens can I redeem in 1 transaction? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tWe have a maximum of 3 sets per transactions.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Can I use Senior Citizen, PWD or MGI discount together with OG Card? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tNo, this is not valid with other promotions and discounts.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Will I be able to redeem the same voucher twice? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tNo, our system is real time, internet based so if the guest claims in another store the system will automatically say redeemed. ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Can I still claim the free Box of 6 OG during limited-time promotions? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tNo, this is not valid in conjunction with other promotions and discounts.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Can I avail of the OG Card perks without the actual card? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tCard should be shown in all OG Card transactions.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Can I avail my OG Card perks through delivery? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tYes, you can do that through https://now.krispykreme.com.ph/. ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                        ]
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "Birthday Treat FAQs",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "\t•	How can I get their birthday treat? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tSame process as to our redemption process. An email will be sent with a QR voucher and we should scan it in our terminal.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	When is the birthday treat given to the members? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tThe birthday voucher email is sent every first week of the month.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Until when is the birthday treat valid? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tThe birthday treat can only be claimed within the birth month.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Can I redeem the birthday treat without the actual card? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tCard should be shown in all OG Card transactions.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\n\t•	Can I avail my birthday treat through delivery? ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0, fontWeight: FontWeight.bold),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\tNo, it is valid for in-store redemption only.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                        ]
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "TERMS & CONDITIONs",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "\t•	Purchases of gift certificates, debit and credit cards, GCash, PayMaya and Sodexo are considered as cash payment and are eligible to claim the reward. ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	Members can earn rewards to a maximum of six (6) Box of 6 Original Glazed® doughnuts within the calendar month.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	OG Card is valid for dine-in, take out, pick up, drive thru and selected delivery transactions only. ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	Rewards are not accumulative and cannot be carried over to the next calendar month.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	Rewards may not be earned in conjunction with the use of government-mandated discounts such as Senior Citizen and Persons with Disability discounts. Special discounts such as employee or brand-specific programs are ineligible to earn rewards. ",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	If the lost card is not yet registered, we will not retrieve their account details and should be advised to purchase a new card. The lost card should automatically be blocked by the system.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	If the member lost their rewards voucher/s, the terminal should show the remaining balance when the member swipes the card for balance inquiry.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                        ]
                      )
                    ),
                    
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "LOST CARD",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "\t•	If the card is lost, member should pay P195 and their account can be retrieved, including their balance rewards.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t\t\t\t\t\t-	Members who lost their card must go to the store, advise the cashier and leave their Krispy Kreme Originals User ID, First and Last Name and email address for verification."+
                            "\n\t\t\t\t\t\t-	Once all details are verified, the guest will be notified either via email that they can claim their new card in the nearest store."+
                            "\n\t\t\t\t\t\t-	If there is a new card activation, the lost card should automatically be blocked by the system.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                        ]
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "CARD RENEWAL",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "\t•	The card is valid for 1 year upon activation.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	Fourteen days (14) before card expiration, the member shall be notified via email.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	Member shall pay P195 to renew their card and the cashier must swipe the card in the terminal.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                          Text(
                            "\t•	The member will be notified of its renewal via email.",
                            style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green,fontSize: 15.0),textAlign: TextAlign.justify,
                          ),
                        ]
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                      child: Text(
                        "CONTACT INFO",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: Text(
                        "For other concerns and questions, please email ogcard@krispykreme.com.ph.",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          color: AppColors.green,
                          fontSize: 15.0
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    SizedBox(height: 40,),
                  ]
                )
              )
            ),  
            Positioned(
              top: 140,
              left: 0,
              right: 0,
              child: Center(
                child: Container(
                  child: Text("FAQS",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: AppColors.green
                    ),
                  )
                )
              )
            ), 
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            )
          ],
        ) 
      )
    );
  }
}