import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/routes.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/sp5.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
            padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.height * .04,
              vertical: MediaQuery.of(context).size.height * .1
            ),
            children: <Widget>[
              Container(
                height: 80.0,
                child: Image.asset("assets/image/logo.png", fit: BoxFit.contain,)
              ), 
              SizedBox(height: MediaQuery.of(context).size.height * .01),
              Container(
                child: Text(
                  "Let's get started!",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: AppColors.green
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * .02),
              Container(
                child: Text(
                  "For new user",
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: AppColors.green
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * .007),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(15.0),
                  color: AppColors.green,
                  child: MaterialButton(
                   height:50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(width: 10.0), 
                        Container(
                          height: 20,
                          child: Image.asset("assets/icons/card.png", fit: BoxFit.contain, color: Colors.white,)
                        ),
                        SizedBox(width: 15.0), 
                        Text("Activate OG Card", 
                          style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      ],
                    ),
                    onPressed: (){ _navigateToRegistrationPage(context);},
                  ),
                )
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(child: Container(height: 2, color: Colors.black26,)),
                    SizedBox(width: 10,),
                    Text("OR",
                      style: TextStyle(fontSize: 12,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Quicksand',
                        color: Colors.black26
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(width: 10,),
                    Expanded(child: Container(height: 2, color: Colors.black26,)),
                  ],
                )
              ),
              SizedBox(height: MediaQuery.of(context).size.height * .01),
              Container(
                child: Text(
                  "For registered user",
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand',
                    color: AppColors.green
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * .007),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  border: Border.all(
                    color: AppColors.green,
                    width: 2.0,
                  ) 
                ),
                child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(15.0),
                  child: MaterialButton(
                  height:50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(width: 10.0), 
                        Container(
                          height: 20,
                          child: Image.asset("assets/icons/login.png", fit: BoxFit.contain, color: Colors.green,)
                        ),
                        SizedBox(width: 15.0), 
                        Text("Proceed to Login", 
                          style: TextStyle(fontSize: 15.0, color: AppColors.green, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      ],
                    ),
                    onPressed: (){_navigateToLoginPage(context);}
                  ),
                )
              ),
            ],
          )
        )
    );
  }

  void _navigateToLoginPage(BuildContext context) {
    Navigator.pushNamed(context, Routes.LOGIN_QUESTION_PAGE);
  }

  void _navigateToRegistrationPage(BuildContext context) {
    Navigator.pushNamed(context, Routes.REGISTRATION_QUESTION);
  }

}