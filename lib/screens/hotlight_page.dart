import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_phone_state/flutter_phone_state.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/blocs/store_bloc.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/db/dbHelper.dart';
import 'package:krispykreme/model/branches.dart';
import 'package:krispykreme/model/hotlight.dart';
import 'package:krispykreme/model/hotlightoffline.dart';
import 'package:krispykreme/screens/notification_page.dart';
import 'package:krispykreme/screens/profile_page.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:permission_handler/permission_handler.dart' as perm;

class HotlightPage extends StatefulWidget {

  @override
  _HotlightPageState createState() => _HotlightPageState();
}

class _HotlightPageState extends State<HotlightPage> {
  
    
  final TextEditingController _searchController = TextEditingController();
  List<HotlightOffline> hotlightBranch = new List<HotlightOffline>();
  final dbHelper = DatabaseHelper.instance;
  bool cLickData = false;
  String branchname = "", branchAddress ="", branchTime = "", mobileNo = "";
  GoogleMapController mapController;
  BitmapDescriptor pinLocationIcon;
  CameraPosition initialLocation;
  Set<Marker> _markers = {};
  var currentLocation = LocationData;
  var location = new Location();
  double lat = 12.4128, lang = 122.5599;
  
  @override
  void initState() {
    super.initState();
    _searchController.addListener(_onSearch);
    getBranches();
    checkPermission();
    setCustomMapPin();
    initialLocation = CameraPosition(
      target: LatLng(lat , lang),
      zoom: 5.5,
    );
  }

  @override
  Widget build(BuildContext context) { 
    setState(() {
      if(_searchController.text != "")
        initialLocation = CameraPosition(
          target: LatLng(lat, lang),
          zoom: 12
        ); 
    });
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0, 90, 0, 0),
              child: hotlightBranch.length == 0
                ? Padding(
                    padding: EdgeInsets.all(16),
                    child: Center(
                      child: Text("Search Branch does not exist",
                        textAlign: TextAlign.center,
                      )
                    )
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                            Expanded(
                              child: GoogleMap(
                                markers: _markers,
                                mapType: MapType.normal,
                                initialCameraPosition: initialLocation,
                                onMapCreated: (GoogleMapController controller) {
                                  mapController = controller;
                                  setState(() {
                                  for(int i = 0 ; i< hotlightBranch.length; i++){
                                    print(hotlightBranch[i].latitude.toString()+ " df " + hotlightBranch[i].longitude.toString());
                                      _markers.add(
                                          Marker(
                                            markerId: MarkerId(hotlightBranch[i].branchName),
                                            position: LatLng(hotlightBranch[i].latitude, hotlightBranch[i].longitude),
                                            icon: pinLocationIcon,
                                            onTap: () {
                                              setState(() {
                                                cLickData = true;
                                                branchname = hotlightBranch[i].branchName;
                                                branchAddress = hotlightBranch[i].branchAddress;
                                                branchTime = hotlightBranch[i].startDate + " - " + hotlightBranch[i].endDate;
                                                mobileNo = hotlightBranch[i].mobileNo;
                                              });
                                            }
                                          )
                                      );
                                    }
                                  });
                                },              
                              ),
                            ),
                            cLickData == true
                              ? Container(
                              height: MediaQuery.of(context).size.height * .27,
                              width: MediaQuery.of(context).size.width * 1,
                              color: Colors.white,
                              padding: EdgeInsets.all(10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      branchname,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Quicksand',
                                        color: AppColors.green
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      SizedBox(width: 5,),
                                      Image.asset("assets/icons/loc.png"),
                                      SizedBox(width: 10,),
                                      Expanded(
                                        child: Text(
                                          branchAddress,
                                          maxLines: 2,
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Quicksand',
                                            color: AppColors.black
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 5,),
                                  Row(
                                    children: <Widget>[
                                      SizedBox(width: 3,),
                                      Image.asset("assets/icons/time.png"),
                                      SizedBox(width: 7,),
                                      Expanded(
                                        child: Text(
                                          branchTime,
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontFamily: 'Quicksand',
                                            color: AppColors.black
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 20),
                                  Row(
                                    children: <Widget>[
                                      SizedBox(width: 10,),
                                      Expanded(
                                        child: Material(
                                          elevation: 5.0,
                                          borderRadius: BorderRadius.circular(10.0),
                                          color: AppColors.white,
                                          child: MaterialButton(
                                            height: 50,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(10.0),
                                              side: BorderSide(
                                                color: AppColors.green, //Color of the border
                                                style: BorderStyle.solid, //Style of the border
                                                width: 2, //width of the border
                                              ),
                                            ),
                                            child:Row(
                                              children: <Widget>[
                                                SizedBox(width: 5,),
                                                Image.asset("assets/icons/call.png"),
                                                SizedBox(width: 10,),
                                                Text("Call Hotline", 
                                                  style: TextStyle(fontSize: 15.0, color: AppColors.green, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                              ],
                                            ),
                                            onPressed: () {initiateCall("888-79000");}
                                          )
                                        ),
                                      ),
                                      SizedBox(width: 10,),
                                      Expanded(
                                        child: Material(
                                          elevation: 5.0,
                                          borderRadius: BorderRadius.circular(10.0),
                                          color: AppColors.green,
                                          child: MaterialButton(
                                            height: 50,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(10.0),
                                            ),
                                            child:Row(
                                              children: <Widget>[
                                                SizedBox(width: 5,),
                                                Image.asset("assets/icons/order.png"),
                                                SizedBox(width: 10,),
                                                Text("Order Now", 
                                                  style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                              ],
                                            ),
                                            onPressed: () {goToWebsite();}
                                          )
                                        ),
                                      ),
                                      SizedBox(width: 10,),
                                    ],
                                  )
                                ],
                              ),
                            )
                            : Container()
                          ],
                        )
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * 1,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context, MaterialPageRoute(
                              builder: (_) {
                                return NotificationPage();
                              }
                            )
                          );
                        },
                        elevation: 5.0,
                        child: Image.asset(notifImage, fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:5),
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: RawMaterialButton(
                        onPressed: () {
                          Navigator.pushReplacement(
                            context, MaterialPageRoute(
                              builder: (_) {
                                return ProfilePage();
                              }
                            )
                          );
                        },
                        elevation: 5.0,
                        child: Image.asset("assets/icons/profile.png", fit: BoxFit.cover,),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                    SizedBox(width:10)
                  ]
                )
              )
            ),
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                color: AppColors.green,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png"),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            ),
            Positioned(
              top: 65,
              left: 0,
              right: 0,
              child: Image.asset("assets/image/logo.png", fit: BoxFit.contain, height: 50)
            ),
            Positioned(
              top: 140,
              left: 0,
              right: 0,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child :Stack(
                  children: [
                    Container(
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        border: Border.all(
                          color: AppColors.green,
                          width: 1.0,
                        ) 
                      ),
                      child:TextFormField(
                        style: TextStyle(fontFamily: 'Quicksand',color: AppColors.green),
                        controller: _searchController,
                        cursorColor: AppColors.green,
                        decoration: InputDecoration(
                          counterText: '',
                          hintText: "Search Branch",
                          hintStyle: TextStyle(color: AppColors.green),
                          focusColor: AppColors.green,
                          hoverColor: AppColors.green,
                          fillColor: AppColors.green,
                          contentPadding: EdgeInsets.fromLTRB(20, 5, 50, 5),
                          disabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide.none),
                          border: OutlineInputBorder(borderSide: BorderSide.none),
                        ),
                      )
                    ),
                    Positioned(
                      right: 0,
                      top: 0,
                      bottom: 0,
                      child: Container(
                        width: 50,
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Icon(Icons.search, color: AppColors.green)
                      )
                    )
                  ],
                ),
              )
            )
          ],
        ),
      ),
    );
  }

  void _onSearch() {
    setState(() {
      getBranches();
      if(_searchController.text != "")
        mapController.animateCamera(
          CameraUpdate.newLatLngZoom(
            LatLng(
              lat,
              lang,
            ),
            12
          ),
        );
    });
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
    ImageConfiguration(devicePixelRatio: 3),
    'assets/image/hotlighOn.png');
  }

  goToWebsite() async {
    const url = 'https://now.krispykreme.com.ph/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  initiateCall(String _phoneNumber) {
    if (_phoneNumber?.isNotEmpty == true) {
      setState(() {
        FlutterPhoneState.startPhoneCall(_phoneNumber);
      });
    }
    else{
      errorMessage("Unable to make a call. Please try again later!", context);
    }
  }

  checkPermission() async{
    
    Location location = new Location();    
    bool _serviceEnabled;
    
    if (await perm.Permission.location.request().isGranted) {
      if (await perm.Permission.locationWhenInUse.serviceStatus.isEnabled) {
      }else{
        _serviceEnabled = await location.serviceEnabled();
        if (!_serviceEnabled) {
          await location.requestService().then((value) => value);
        }else{
          errorMessage("Please turn on location to use this feature!", context);
        }
      }
    }else{
      errorMessage("Please enable location permission in the \nApplication Setting >Krispy Kreme>Permission and open location to use this feature!", context);
    }
  }

   getBranches() async {
    var result = await dbHelper.selectDataWHERE("HotLightBranches", _searchController.text);
    print(result);
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    hotlightBranch.clear();
    for(var items in imagelist){
      cLickData = false;
      setState(() {
        hotlightBranch.add(new HotlightOffline(
            int.parse("100"),
            items["branch_name"].toString(), 
            items["branch_code"].toString(),
            items["branch_address"].toString(),
            items["branch_description"].toString(),
            items["email"].toString(),
            items["landline_no"].toString(),
            items["mobile_no"].toString(),
            double.parse(items["longitude"]),
            double.parse(items["latitude"]),
            items["start_date"].toString(),
            items["end_date"].toString()
          )
        );
        if(_searchController.text != "")
          lat = double.parse(items["latitude"]);
          lang = double.parse(items["longitude"]);
      });
    }
  }
}