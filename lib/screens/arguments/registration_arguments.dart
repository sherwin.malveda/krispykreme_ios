import 'package:krispykreme/model/request/member_request.dart';

class RegistrationArguments {
  final String email;
  final String cardNumber;
  MemberRequest request;

  RegistrationArguments(this.email, this.cardNumber, this.request);
}
