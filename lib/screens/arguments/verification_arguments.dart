import 'package:krispykreme/model/request/verification_request.dart';

class VerificationArguments {
  final VerificationRequest credentials;

  VerificationArguments(this.credentials);
}
