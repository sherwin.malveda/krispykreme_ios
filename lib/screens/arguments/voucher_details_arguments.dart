class VoucherDetailsArguments {
  final String vouchercode, description, status, expiration;

  VoucherDetailsArguments(this.vouchercode, this.description, this.status, this.expiration);
}
