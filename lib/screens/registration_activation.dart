import 'dart:async';
import 'package:after_layout/after_layout.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/form_validators.dart';
import 'package:krispykreme/model/activation.dart';
import 'package:krispykreme/model/request/member_request.dart';
import 'package:krispykreme/model/request/verification_request.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/arguments/registration_arguments.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/input_field.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/success.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class ActivationPage extends StatefulWidget {
  @override
  _ActivationPageState createState() => _ActivationPageState();
}

class _ActivationPageState extends State<ActivationPage> 
    with AfterLayoutMixin<ActivationPage> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
 
  TextEditingController _cardNumberController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _mobileNumberController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  final TextEditingController _posRefNumber = TextEditingController();

  final FocusNode cardNumberFocusNode = new FocusNode();
  final FocusNode firstNameFocusNode = new FocusNode();
  final FocusNode lastNameFocusNode = new FocusNode();
  final FocusNode mobileNumberFocusNode = new FocusNode();
  final FocusNode emailFocusNode = new FocusNode();
  final FocusNode posRefNumber = new FocusNode();
  
  AuthBloc _authBloc;
  StreamSubscription _stateSubs;
  StreamSubscription _registrationCredsSub;

  bool checkBox = false, prn = true;

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    if (_authBloc.registerState.hasListener) _stateSubs?.cancel();
    _stateSubs = _authBloc.registerState.listen(onStateChanged);
    if (_authBloc.registrationCredentials.hasListener)
      _registrationCredsSub?.cancel();
    _registrationCredsSub =
        _authBloc.registrationCredentials.listen(onActivationCredentials);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _stateSubs.cancel();
    super.dispose(); 
  }

  void checkCardNumber(RegistrationArguments args) {
    final MemberRequest request = args.request;
    _authBloc.existingmember(request, request.cardNumber);
  }

  @override
  Widget build(BuildContext context) {
    final RegistrationArguments args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            StreamBuilder<User>(
              stream: _authBloc.existingMember,
              builder: (context, snapshot) {
                return StreamHandler(
                  snapshot: snapshot,
                  noData: Stack(
                    children: <Widget> [
                      Container(
                        child: Center(
                          child:Text("No network connection!\n Please try agan later!", style: TextStyle(fontSize: 15,color: Colors.black, fontFamily: 'Quicksand'),),
                        )
                      ),
                    ]
                  ),
                  loading: Center(child: CircularProgressIndicator()),
                  withError: (error) => Container(
                    child: Center(
                      child: error.toString() == "Card is already activated"
                        ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 120.0,
                              child: Image.asset("assets/image/donut.png", fit: BoxFit.contain,)
                            ),
                            SizedBox(height: 20.0,),
                            Text("Account is already Activated", textAlign: TextAlign.center, style: TextStyle(fontSize: 24, color: Colors.red, fontFamily: 'Quicksand'),),
                            Text("If you are encountering problems, you may message us at", textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.black, fontFamily: 'Quicksand'),),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 30),
                              height: 30,
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                color: Colors.transparent,
                                child: MaterialButton(
                                  child: Container(
                                    decoration: new BoxDecoration(
                                      border: Border(bottom: BorderSide(color: Colors.blue))
                                    ),
                                    child: Text("ogcard@krispykreme.com.ph",
                                      style: TextStyle(
                                        fontFamily: 'Quicksand',
                                        color: Colors.blue,
                                        fontSize: 15.0
                                      )
                                    ),
                                  ),
                                  onPressed: (){_launchURL();},
                                ),
                              )
                            ),
                            SizedBox(height: 80.0,),
                          ],
                        )
                        :  error.toString() == "Email address already exist"
                            ?Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                height: 120.0,
                                child: Image.asset("assets/image/donut.png", fit: BoxFit.contain,)
                              ),
                              SizedBox(height: 20.0,),
                              Text("Email address already exist",textAlign: TextAlign.center, style: TextStyle(fontSize: 24, color: Colors.red, fontFamily: 'Quicksand'),),
                              Text("If you are encountering problems, you may message us at",textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.black, fontFamily: 'Quicksand'),),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 30),
                                height: 30,
                                child: Material(
                                  borderRadius: BorderRadius.circular(15.0),
                                  color: Colors.transparent,
                                  child: MaterialButton(
                                    child: Container(
                                      decoration: new BoxDecoration(
                                        border: Border(bottom: BorderSide(color: Colors.blue))
                                      ),
                                      child: Text("ogcard@krispykreme.com.ph",
                                        style: TextStyle(
                                          fontFamily: 'Quicksand',
                                          color: Colors.blue,
                                          fontSize: 15.0
                                        )
                                      ),
                                    ),
                                    onPressed: (){_launchURL();},
                                  ),
                                )
                              ),
                              SizedBox(height: 80.0,),
                            ],
                          )
                          : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 120.0,
                              child: Image.asset("assets/image/donut.png", fit: BoxFit.contain,)
                            ),
                            SizedBox(height: 20.0,),
                            Text("Email Address or\nCard Number is Incorrect",textAlign: TextAlign.center, style: TextStyle(fontSize: 24, color: Colors.red, fontFamily: 'Quicksand'),),
                            Text("If you are encountering problems, you may message us at",textAlign: TextAlign.center, style: TextStyle(fontSize: 15, color: Colors.black, fontFamily: 'Quicksand'),),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 30),
                              height: 30,
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                color: Colors.transparent,
                                child: MaterialButton(
                                  child: Container(
                                    decoration: new BoxDecoration(
                                      border: Border(bottom: BorderSide(color: Colors.blue))
                                    ),
                                    child: Text("ogcard@krispykreme.com.ph",
                                      style: TextStyle(
                                        fontFamily: 'Quicksand',
                                        color: Colors.blue,
                                        fontSize: 15.0
                                      )
                                    ),
                                  ),
                                  onPressed: (){_launchURL();},
                                ),
                              )
                            ),
                            SizedBox(height: 80.0,),
                          ],
                        ),
                    )
                  ),
                  withData: (User user) => Container(
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: AssetImage('assets/image/bg.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(height: 80.0,),
                          Container(
                            height: 80.0,
                            child: Image.asset("assets/image/logo.png", fit: BoxFit.contain,)
                          ),
                          SizedBox(height: 20.0,),
                          Container(
                            child: Text(
                              "OG Card Details",
                              style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Quicksand',
                                color: AppColors.green
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Expanded( child: ListView(
                            shrinkWrap: true,
                              padding: EdgeInsets.symmetric(horizontal:25),
                                children: <Widget>[
                                  SizedBox(height: MediaQuery.of(context).size.height * .04),
                                  Column(
                                    children: [
                                      InputField(
                                        enabled: false,
                                        focusNode: cardNumberFocusNode,
                                        hintText: "Card Number",
                                        keyboardType: TextInputType.number,
                                        controller: _cardNumberController = 
                                                TextEditingController( text: args.cardNumber),
                                        submit: (value){
                                          FocusScope.of(context).requestFocus(firstNameFocusNode);
                                        },
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 8.0),
                                  Column(
                                    children: [
                                      InputField(
                                        focusNode: firstNameFocusNode,
                                        hintText: "First Name",
                                        autoValidate: true,
                                        validator: _validateName,
                                        keyboardType: TextInputType.text,
                                        controller: _firstNameController,
                                        submit: (value){
                                          FocusScope.of(context).requestFocus(lastNameFocusNode);
                                        },
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 8.0),
                                  Column(
                                    children: [
                                      InputField(
                                        focusNode: lastNameFocusNode,
                                        hintText: "Last Name",
                                        autoValidate: true,
                                        validator: _validateName,
                                        keyboardType: TextInputType.text,
                                        controller: _lastNameController,
                                        submit: (value){
                                          FocusScope.of(context).requestFocus(emailFocusNode);
                                        },
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 8.0),
                                  Column(
                                    children: [
                                      InputField(
                                        enabled: false,
                                        focusNode: emailFocusNode,
                                        hintText: "Email Address",
                                        keyboardType: TextInputType.emailAddress,
                                        controller: _emailController = 
                                                TextEditingController( text: args.email),
                                        submit: (value){
                                          FocusScope.of(context).requestFocus(mobileNumberFocusNode);
                                        },
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 8.0),
                                  Column(
                                    children: [
                                      InputField(
                                        focusNode: mobileNumberFocusNode,
                                        hintText: "Mobile Number",
                                        autoValidate: true,
                                        validator: _validateMobileNumber,
                                        maxLength: 11,
                                        keyboardType: TextInputType.text,
                                        controller: _mobileNumberController,
                                        submit: (value){
                                          FocusScope.of(context).requestFocus(posRefNumber);
                                        },
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 8.0),
                                  Column(
                                    children: [
                                      InputField(
                                        enabled: prn,
                                        focusNode: posRefNumber,
                                        hintText: "POS Ref Number",
                                        autoValidate: true,
                                        validator: _validateNumber,
                                        maxLength: 11,
                                        keyboardType: TextInputType.text,
                                        controller: _posRefNumber,
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 15.0),
                                  SizedBox(
                                    height: 25,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        SizedBox(width:10),
                                        Checkbox(
                                          value: checkBox,
                                          activeColor: AppColors.green,
                                          onChanged: (value) {
                                            setState(() {
                                              if(value == true){
                                                _posRefNumber.text = "";
                                                prn = false;
                                              }else{
                                                prn = true;
                                              }
                                              checkBox = value;
                                            });
                                          },
                                        ),
                                        Text("Register Card as Gift",
                                          style: TextStyle(color:Colors.black,fontSize:18, fontFamily: 'Quicksand'),),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 20.0),
                                  Container(
                                    height: 60.0,
                                    child: StreamBuilder<ScreenState>(
                                      stream: _authBloc.registerState,
                                      builder: (context, snapshot) {
                                        return _buildSubmitButton(snapshot);
                                      }
                                    ),
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height * .02),
                                  Container(
                                    height: 40,
                                    child: Material(
                                      borderRadius: BorderRadius.circular(15.0),
                                      color: Colors.transparent,
                                      child: MaterialButton(
                                        child: Container(
                                          decoration: new BoxDecoration(
                                            border: Border(bottom: BorderSide(color: AppColors.green))
                                          ),
                                          child: Text("Back to Login",
                                            style: TextStyle(
                                              fontFamily: 'Quicksand',
                                              color: AppColors.green,
                                              fontSize: 15.0
                                            )
                                          ),
                                        ),
                                        onPressed: (){_navigateToLoginPage(context);},
                                      ),
                                    )
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height * .05),
                                ],
                              )
                          )
                        ],
                      ),
                    ),
                  )
                );
              }
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png",color: AppColors.green),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            )
          ],
        ) 
      )
    );
  }

  Widget _buildSubmitButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(height: 50,
            padding: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child:CircularProgressIndicator(),
            onPressed: (){},
          ),
        )
      );
    } else {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            padding: EdgeInsets.zero,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child:Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                Expanded(
                  child: Text("Activate Card", 
                    style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center),
                ),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomRight:  const  Radius.circular(15.0),
                      topRight: const  Radius.circular(15.0)
                    ),
                    color: Colors.black12
                  ),
                  alignment: Alignment.center,
                  child: Text("2 of 2", 
                    style: TextStyle(fontSize: 13.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center),            
                  )
                ],
              ),
              onPressed: _onSubmit,
          ),
        )
      );
    }
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _navigateToLandingPage(context);
        break;
      case States.ERROR:
        if(state.error.toString() == "Mobile number has an invalid format"){
          errorMessage("Mobile number has an invalid format", context);
        }else if(state.error.toString() == "Mobile number already exists"){
          errorMessage("Mobile number already exists", context);
        }else{
          errorMessage("Error in Card Activation", context);
        }
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _navigateToEmailConfirmation(VerificationRequest credentials) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Routes.ACCOUNT_VERIFICATION2, ModalRoute.withName(Routes.LANDING),
        arguments: credentials);
  }

  String _validateName(String value) =>
      FormValidators.isLettersOnly(value, "Enter a valid name");

  String _validateMobileNumber(String value) => 
      FormValidators.isMobileNumber(value, "Mobile Number length must be {11} characters.");
    
  String _validateNumber(String value) => 
      FormValidators.isNumber(value, "Enter a valid number");

  String _validateEmail(String value) =>
      FormValidators.isValidEmail(value, "Enter a valid email");

  void _onSubmit() {
    final String firstName = _firstNameController.text;
    final String lastName = _lastNameController.text;
    final String cardPan = _cardNumberController.text;
    final String mobileNumber = "+63"+ _mobileNumberController.text.substring(1,11);
    final String email = _emailController.text;
    String prn;
    if(checkBox == true){
      prn = "Card as Gift";
    }else{
      prn = _posRefNumber.text;
    }
    final bool subscription =  checkBox;
    final format = new DateFormat("yyyy-MM-dd'T'HH:mm:ss");
    format.format(DateTime.now());
    final String txnDateTime = format.format(DateTime.now()).toString();
    final Activation activation = Activation(
      posRefNo: prn,
      firstName: firstName,
      lastName: lastName,
      emailAdd: email,
      mobileNo: mobileNumber,
      displayName: "",
      securityPin: "",
      transactionDate: txnDateTime
    );

    if (!activation.hasNoNullsForRequiredField()) {
      errorMessage("Fill up all the required fields", context);
      return;
    }
    
    print(subscription.toString() + " " + prn);
    if(subscription == false && (prn == null || prn == "")){
      errorMessage("Please input your POS Ref Number or Register Card as Gift!", context);
      return;
    }

    if(_validateNumber(prn) != null && checkBox == false){
      errorMessage("Input a valid POS Ref Number.", context);
      return;
    }

    if (_formKey.currentState.validate()) {
      _authBloc.activate(activation,cardPan);
    }
  }

  void _navigateToLoginPage(BuildContext context) {
     Navigator.of(context).pushReplacementNamed(Routes.LOGIN);
  }

  void _navigateToLandingPage(BuildContext context) {
    Navigator.of(context).pushReplacementNamed(Routes.LANDING);
    _showsuccessDialog();
  }

  void onActivationCredentials(VerificationRequest credentials) {
    _navigateToEmailConfirmation(credentials);
  }

  void _launchURL() async {
    final Uri params = Uri(
      scheme: 'mailto',
      path: 'ogcard@krispykreme.com.ph',
    );
    String  url = params.toString();
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print( 'Could not launch $url');
    }
  }

  void _showsuccessDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .30,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Text("Activation Success!",
                  style: TextStyle(
                  fontSize: 18.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .01 ),
                Text("Please check your email used in activation\nfor further instruction!",
                  style: TextStyle(
                  fontSize: 12.0, 
                  color: AppColors.black,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 40,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
                      child:Text("Ok", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ),
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
              ]
            )
          ),
        );
      },
    );
  }


  @override
  void afterFirstLayout(BuildContext context) {
    final RegistrationArguments args = ModalRoute.of(context).settings.arguments;
    checkCardNumber(args);
  }
}
