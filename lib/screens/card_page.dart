import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/card_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/model/cards.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/screens/branch_details_page.dart';
import 'package:krispykreme/widgets/card.dart';
import 'package:krispykreme/widgets/stream_handler.dart';
import 'package:krispykreme/widgets/voucher_row.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:intl/intl.dart';

class CardPage extends StatefulWidget {
  @override
  _CardPageState createState() => _CardPageState();
}

class _CardPageState extends State<CardPage> {


  RefreshController _refreshController = RefreshController(initialRefresh: false);
  CardBloc _cardBloc;
  AuthBloc _authBloc;

  @override
  void didChangeDependencies() {
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.fromLTRB(0, 90, 0, 0),
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: SmartRefresher(
              enablePullDown: true,
              header: ClassicHeader(
                refreshingText: "Refreshing",
                textStyle: TextStyle(fontFamily: 'Quicksand'),
              ),
              controller: _refreshController,
              onRefresh:()=> _onRefresh(),
              child: ListView(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                children: <Widget> [
                  StreamBuilder<User>(
                    stream: _cardBloc.currentUser,
                    builder: (context, snapshot) {
                      return StreamHandler(
                        snapshot: snapshot,
                        loading: Center(child: CircularProgressIndicator()),
                        noData: Container(),
                        withError: (error) => Container(),
                        withData: (User user) => StreamBuilder<Cards>(
                          stream: _cardBloc.cards,
                          builder: (context, snapshot) {
                            return StreamHandler(
                              snapshot: snapshot,
                              loading: Center(child: CircularProgressIndicator()),
                              noData: Container(),
                              withError: (error) => Padding(
                                padding: EdgeInsets.all(16),
                                child: Center(
                                  child: Text(
                                    "Failed to load your card info .\n\nPlease check your internet connection and try again!",
                                    style: TextStyle(fontFamily: 'Quicksand'),
                                    textAlign: TextAlign.center,
                                  )
                                )
                              ),
                              withData: (Cards cards) =>Padding(
                                padding: EdgeInsets.fromLTRB(30,0,20,30),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(5.0))
                                      ),
                                      alignment: Alignment.center,
                                      child: Text("OG CARD",
                                        style: TextStyle(
                                          fontSize: 24.0, 
                                          color: AppColors.green,
                                          fontWeight: FontWeight.bold
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                                      alignment: Alignment.center,
                                      width: MediaQuery.of(context).size.width * 1,
                                      child: FlatButton(  
                                        child: Stack(
                                          fit: StackFit.loose,
                                          alignment: Alignment.center,
                                          children: <Widget>[
                                            Container(
                                              child:Card(
                                                elevation: 5.0,
                                                child:Image.asset("assets/image/og_card.png",
                                                  fit: BoxFit.cover
                                                ),
                                              )
                                            ),
                                            Positioned(
                                              bottom: 15,
                                              right: 15,
                                              child: Text("View Card",
                                                style: TextStyle(
                                                  fontSize: 14.0, 
                                                  color: AppColors.green,
                                                  fontWeight: FontWeight.normal
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        onPressed: (){_showVoucher(cards);},
                                      ),
                                    ),
                                    SizedBox(height: MediaQuery.of(context).size.height * .02),
                                    Text(user.firstname + " " + user.middleName + " " + user.lastName,
                                      style: TextStyle(
                                        fontSize: 16.0, 
                                        color: AppColors.green,
                                        fontWeight: FontWeight.bold
                                      ),
                                    ),
                                    Text("Card No: " + cards.cards[0].cardPan,
                                      style: TextStyle(
                                        fontSize: 14.0, 
                                        color: AppColors.green,
                                        fontWeight: FontWeight.normal
                                      ),
                                    ),
                                    Text("Birthday: " + changedateFormat(user.birthDate),
                                      style: TextStyle(
                                        fontSize: 14.0, 
                                        color: AppColors.black,
                                        fontWeight: FontWeight.normal
                                      ),
                                    ),
                                    Text("Email: " + user.email,
                                      style: TextStyle(
                                        fontSize: 14.0, 
                                        color: AppColors.black,
                                        fontWeight: FontWeight.normal
                                      ),
                                    ),
                                    Text("Mobile: " + user.mobile,
                                      style: TextStyle(
                                        fontSize: 14.0, 
                                        color: AppColors.black,
                                        fontWeight: FontWeight.normal
                                      ),
                                    ),
                                    SizedBox(height: MediaQuery.of(context).size.height * .03),
                                    Container(
                                      height: 50.0,
                                      width: MediaQuery.of(context).size.width * 1,
                                      decoration: BoxDecoration(
                                        color: AppColors.white,
                                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                        border: Border.all(
                                          color: AppColors.pink,
                                          width: 2.0,
                                        ) 
                                      ),
                                      child: FlatButton(
                                        onPressed: () {
                                          _showExpiryDialog(cards.cards[0].cardExpiry);
                                        },
                                        child: Text("Card Expiry Date: " + cards.cards[0].cardExpiry.substring(0,10),
                                          style: TextStyle(
                                            color: AppColors.pink,
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.w600
                                          ), textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ),
                            );
                          }
                        )
                      );
                    }
                  ),
                  
                ],
              ),
            )
      ),
    );
  }

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      try{
        _cardBloc.getAccountDetails(_authBloc.session.value.accountId);
        _cardBloc.getCardDetails(_authBloc.session.value.accountId);
      }catch(Exception){}
    });
    _refreshController.refreshCompleted();
  }

  void _showExpiryDialog(String expiry) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .45,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(
                    height: 30,
                    width: 30,
                    child: RawMaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      elevation: 5.0,
                      child: Image.asset("assets/icons/close.png", fit: BoxFit.cover,),
                      padding: EdgeInsets.all(0.0),
                      shape: CircleBorder(),
                    )
                  ),
                  SizedBox(width: 15),
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Text("Your OG Card Expires on: \n"+expiry.substring(0,10),
                  style: TextStyle(
                  fontSize: 15.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height:MediaQuery.of(context).size.height * .03),
                Text("Your membership will expires in "+ compareDateTime(expiry) +" day(s)",
                  style: TextStyle(
                  fontSize: 10.0, 
                  color: AppColors.pink,
                  ),
                  textAlign: TextAlign.center
                ),
                Text("Find a Krispy Kreme store to renew your OG Card.",
                  style: TextStyle(
                  fontSize: 11.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .05),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 50,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
                      child:Text("Find a Store", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){
                        Navigator.pop(context);
                        Navigator.push(
                          context, MaterialPageRoute(
                            builder: (_) {
                              return BranchDetailsPage();
                            }
                          )
                        );
                      },
                    ),
                  )
                ),
                SizedBox(height:20)
              ]
            )
          ),
        );
      }
    );
  }

  void _showVoucher(Cards cards) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .65,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Text("Card Details",
                  style: TextStyle(
                  fontSize: 24.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                Text("(Expires in "+ cards.cards[0].cardExpiry.substring(0,10) +" )",
                  style: TextStyle(
                  fontSize: 14.0, 
                  color: AppColors.green,
                  fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10),
                Container(
                  margin: EdgeInsets.symmetric(horizontal:20.0),
                  child: CardVoucher(
                    code: cards.cards[0].cardPan
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 40,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
                      child:Text("Ok", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){Navigator.pop(context);},
                    ),
                  )
                ),
                SizedBox(height: 10),
              ]
            )
          ),
        );
      },
    );
  }

  String changedateFormat(DateTime date){
    final DateFormat formatter = DateFormat('MMM dd, yyyy');
    final String formatted = formatter.format(date);
    return formatted;
  }

  String compareDateTime(String expiry){
    final DateTime now = DateTime.now();
    final DateTime date = DateTime.parse(expiry);
    int remainingDays = date.difference(now).inDays;
    return remainingDays.toString();
  }
}