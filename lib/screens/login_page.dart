import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/blocs/auth_bloc.dart';
import 'package:krispykreme/blocs/master_provider.dart';
import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/request/login_request.dart';
import 'package:krispykreme/routes.dart';
import 'package:krispykreme/screens/screen_state.dart';
import 'package:krispykreme/widgets/error.dart';
import 'package:krispykreme/widgets/input_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode usernameFocusNode = new FocusNode();
  final FocusNode passwordFocusNode = new FocusNode();
  StreamSubscription stateSubs;
  AuthBloc _authBloc;
  bool passwordOn = true;

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    stateSubs = _authBloc.loginState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    stateSubs.cancel();
    super.dispose();
  }

  @override
  void initState(){
    getFCMToken();
    super.initState();
  }

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/image/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget> [
            ListView(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.height * .04,
                vertical: MediaQuery.of(context).size.height * .12
              ),
              children: <Widget>[
                Container(
                  height: 80.0,
                  child: Image.asset("assets/image/logo.png", fit: BoxFit.contain,)
                ), 
                SizedBox(height: 70.0),
                Container(
                  child: Text(
                    "Let's make your day sweeter!",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Quicksand',
                      color: AppColors.green
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 50.0),
                Container(
                  height: 50,
                  child:InputField(
                    focusNode: usernameFocusNode,
                    hintText: "Username/Email",
                    keyboardType: TextInputType.emailAddress,
                    controller: _usernameController,
                    submit: (value){
                      FocusScope.of(context).requestFocus(passwordFocusNode);
                    },
                  )
                ),
                SizedBox(height: 10.0),
                Stack(
                  children: [
                    Container(
                      height: 50,
                      child:InputField(
                        focusNode: passwordFocusNode,
                        hintText: "Password ",
                        autoValidate: true,
                        keyboardType: TextInputType.text,
                        controller: _passwordController,
                        masked: passwordOn,
                        submit: (value){
                          _login();
                        },  
                      )
                    ),
                    Positioned(
                      right: 0,
                      child: Container(
                        width: 50,
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: RawMaterialButton(
                          onPressed: () {
                            setState(() {
                              if(passwordOn == false){
                                passwordOn = true;
                              }else{
                                passwordOn = false;
                              }
                            });
                          },
                          elevation: 5.0,
                          child: getImage(),
                          shape: CircleBorder(),                              
                        )
                      )
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      child: Text("Forgot Password?",
                        style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 15.0,
                          color: AppColors.green
                        ),
                        textAlign: TextAlign.end,
                      ),
                      onPressed: _navigateToForgotPasswordPage
                    ),  
                  ],
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 60.0,
                  child:StreamBuilder<ScreenState>(
                    stream: _authBloc.loginState,
                    builder: (context, snapshot) => _buildLoginButton(snapshot)
                  ),
                ),
                SizedBox(height: 10.0),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                  height: 40,
                  child: Material(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.transparent,
                    child: MaterialButton(
                      child: Container(
                        decoration: new BoxDecoration(
                          border: Border(bottom: BorderSide(color: AppColors.green))
                        ),
                        child: Text("or Activate your OG Card",
                          style: TextStyle(
                            fontFamily: 'Quicksand',
                            color: AppColors.green,
                            fontSize: 15.0
                          )
                        ),
                      ),
                      onPressed: _navigateToRegistrationPage,
                    ),
                  )
                ),
              ],
            ),
            Positioned(
              top: 0,
              child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width * .5,
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(width:15),
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: RawMaterialButton(
                        onPressed: () {Navigator.pop(context);},
                        elevation: 5.0,
                        child: Image.asset("assets/icons/back.png", color: AppColors.green),
                        padding: EdgeInsets.all(0.0),
                        shape: CircleBorder(),
                      )
                    ),
                  ]
                )
              )
            )
          ],
        ) 
      )
    );
  }

  Widget _buildLoginButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal: 70, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child:CircularProgressIndicator(),
            onPressed: (){},
          ),
        )
      );
    } else {
      return Container(
        height: 50,
        margin: EdgeInsets.symmetric(horizontal:70, vertical: 5),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(15.0),
          color: AppColors.green,
          child: MaterialButton(
            height: 50,
            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(15.0)),
            child:Text("Login", 
              style: TextStyle(fontSize: 18.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
            onPressed: _login,
          ),
        )
      );
    }
  }

  Widget getImage(){
    if(passwordOn == false){
      return Image.asset(
        "assets/icons/eyeon.png",
        color: AppColors.green,
        width:35,
        height:35
      );
    }else{
      return Image.asset(
        "assets/icons/eyeoff.png", 
        width:35,
        height:35
      );
    }
  }
  
  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _navigateToDashboardPage();
        break;
      case States.ERROR:
        _handleError(state);
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _handleError(ScreenState state) {
      errorMessage(state.error.toString(), context);
  }

  void _navigateToDashboardPage() {
    savedtoSharedPref();
    Navigator.pushNamedAndRemoveUntil(context, Routes.DASHBOARD, ModalRoute.withName(Routes.DASHBOARD));
  }

  void _navigateToRegistrationPage() =>
    Navigator.pushReplacementNamed(context, Routes.REGISTRATION_QUESTION);

  void _navigateToForgotPasswordPage() =>
    Navigator.pushNamed(context, Routes.FORGOT_PASSWORD);

  void _login() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('username', _usernameController.text);
    final LoginRequest request = LoginRequest(
      username: _usernameController.text, password: _passwordController.text, grantType: "password"
    );
    String token = prefs.getString('token');
    print(token + "token");
    _authBloc.login(request, "", token );
  }

  void getFCMToken(){
    _firebaseMessaging.getToken().then((token) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', token);
    });
  }

  savedtoSharedPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(_authBloc.session.value.facebookEmail == "" || _authBloc.session.value.facebookEmail ==null){
      prefs.setBool("IsFacebookUrl", false);
      prefs.setBool("FacebookLink", false);
      prefs.setString("FacebookUrl", "");
    }else{
      prefs.setBool("IsFacebookUrl", true);
      prefs.setBool("FacebookLink", true);
      prefs.setString("FacebookUrl", _authBloc.session.value.facebookUrl);
    }
  }

}