import 'package:flutter/material.dart';

class NavigationController extends ChangeNotifier {
  static const int PAGE_HOME = 0;
  static const int PAGE_HISTORY = 1;
  static const int PAGE_NOTIFICATION = 2;
  static const int PAGE_PROMO = 3;
  static const int PAGE_USER = 4;

  int mainIndex;

  void setMainPage(int index) {
    mainIndex = index;
    notifyListeners();
  }
}
