class Activation {
 
  String _posRefNo;
  String _transactionDate;
  String _firstName;
  String _lastName;
  String _emailAdd;
  String _mobileNo;
  String _displayName;
  String _securityPin;

  Activation(
      {String posRefNo,
      String transactionDate,
      String firstName,
      String lastName,
      String emailAdd,
      String mobileNo,
      String displayName,
      String securityPin}) {
    this._posRefNo = posRefNo;
    this._transactionDate = transactionDate;
    this._firstName = firstName;
    this._lastName = lastName;
    this._emailAdd = emailAdd;
    this._mobileNo = mobileNo;
    this._displayName = displayName;
    this._securityPin = securityPin;
  }

  String get posRefNo => _posRefNo;
  set posRefNo(String posRefNo) => _posRefNo = posRefNo;
  String get firstName => _firstName;
  set firstName(String firstName) => _firstName = firstName;
  String get lastName => _lastName;
  set lastName(String lastName) => _lastName = lastName;
  String get transactionDate => _transactionDate;
  set transactionDate(String transactionDate) => _transactionDate = transactionDate;
  String get displayName => _displayName;
  set displayName(String displayName) => _displayName = displayName;
  String get securityPin => _securityPin;
  set securityPin(String securityPin) => _securityPin = securityPin;
  String get emailAdd => _emailAdd;
  set emailAdd(String emailAdd) => _emailAdd = emailAdd;
  String get mobileNo => _mobileNo;
  set mobileNo(String mobileNo) => _mobileNo = mobileNo;

  Activation.fromJson(Map<String, dynamic> json) {
    _posRefNo = json['pos_ref_no'];
    _transactionDate = json['transaction_date'];
    _firstName = json['first_name'];
    _lastName = json['last_name'];
    _emailAdd = json['email'];
    _mobileNo = json['mobile_no'];
    _displayName = json['display_name'];
    _securityPin = json['security_pin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pos_ref_no'] = this._posRefNo;
    data['transaction_date'] = this._transactionDate;
    data['first_name'] = this._firstName;
    data['last_name'] = this._lastName;
    data['email'] = this._emailAdd;
    data['mobile_no'] = this._mobileNo;
    data['display_name'] = this._displayName;
    data['security_pin'] = this._securityPin;
    return data;
  }

  
  bool hasNoNullsForRequiredField() {
    return ((firstName != null && firstName.isNotEmpty) &&
      (lastName != null && lastName.isNotEmpty) &&
      (emailAdd != null && emailAdd.isNotEmpty) &&
      (mobileNo != null && mobileNo.isNotEmpty) 
    );
  }


}