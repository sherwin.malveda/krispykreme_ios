import 'package:krispykreme/model/empty_checkable.dart';
import 'package:krispykreme/model/voucher.dart';

class Vouchers with EmptyCheckable {
  final int totalVoucher;
  final List<Voucher> vouchers;

  Vouchers({
    this.totalVoucher,
    this.vouchers
  });

  factory Vouchers.fromMap(Map<String, dynamic> map) {
    final int totalVoucher = map['total_records'] ?? 0;
    final List<Voucher> vouchers = map['vouchers'] != null
      ? List.from(map['vouchers'])
        .map((transaction) => Voucher.fromMap(transaction))
        .toList()
      : [];

    return Vouchers(
      totalVoucher: totalVoucher,
      vouchers: vouchers
    );
  }

  @override
  bool isEmpty() => vouchers == null || vouchers.isEmpty;
}