import 'package:collection/collection.dart';
import 'package:krispykreme/model/empty_checkable.dart';
import 'package:krispykreme/model/news_letter.dart';

class NewsLetters with EmptyCheckable {
  final int totalNewsLetters;
  final List<NewsLetter> newsLetters;

  NewsLetters({
    this.totalNewsLetters,
    this.newsLetters
  });

  factory NewsLetters.fromMap(Map<String, dynamic> map) {
    final int totalNewsLetters = map['total_records'] ?? 0;
    final List<NewsLetter> newsLetters = map['newsletters'] != null
      ? List.from(map['newsletters'])
        .map((newsletter) => NewsLetter.fromMap(newsletter))
        .toList()
      : [];

    return NewsLetters(
      totalNewsLetters: totalNewsLetters,
      newsLetters: newsLetters
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is NewsLetters &&
        runtimeType == other.runtimeType &&
        totalNewsLetters == other.totalNewsLetters &&
        ListEquality().equals(newsLetters, other.newsLetters);

  @override
  int get hashCode => 
    totalNewsLetters.hashCode ^ 
    newsLetters.hashCode;

  @override
  bool isEmpty() => newsLetters == null || newsLetters.isEmpty;
}
