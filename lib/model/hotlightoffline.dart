class HotlightOffline {
  int branchId;
  String branchName;
  String branchCode;
  String branchAddress;
  String branchDescription;
  String email;
  String landlineNo;
  String mobileNo;
  double longitude;
  double latitude;
  String startDate;
  String endDate;

  HotlightOffline(
      this.branchId,
      this.branchName,
      this.branchCode,
      this.branchAddress,
      this.branchDescription,
      this.email,
      this.landlineNo,
      this.mobileNo,
      this.longitude,
      this.latitude,
      this.startDate,
      this.endDate);
}