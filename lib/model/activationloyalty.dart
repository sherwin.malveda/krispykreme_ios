class ActivationLoyalty {
  final String firstName;
  final String lastName;
  final String emailAdd;
  final String mobileNo;

  ActivationLoyalty({
    this.firstName,
    this.lastName,
    this.emailAdd,
    this.mobileNo
  });

  factory ActivationLoyalty.fromMap(Map<String, dynamic> map) {
    final String firstName = map['first_name'] ?? "";
    final String lastName = map['last_name'] ?? "";
    final String emailAdd = map['email_add'] ?? "";
    final String mobileNo = map['mobile_no'] ?? "";

    return ActivationLoyalty(
      firstName: firstName,
      lastName: lastName,
      emailAdd: emailAdd,
      mobileNo: mobileNo
    );
  }
}
