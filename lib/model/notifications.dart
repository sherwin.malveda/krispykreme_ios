import 'package:krispykreme/model/empty_checkable.dart';
import 'package:krispykreme/model/notification.dart';

class Notifications with EmptyCheckable {
  final int totalMessages;
  final List<NotificationModel> message;

  Notifications({
    this.totalMessages,
    this.message
  });

  factory Notifications.fromMap(Map<String, dynamic> map) {
    final int totalMessages = map['total_records'] ?? 0;
    final List<NotificationModel> message = map['notifications'] != null
      ? List.from(map['notifications'])
        .map((message) => NotificationModel.fromMap(message))
        .toList()
      : [];

    return Notifications(
      totalMessages: totalMessages,
      message: message
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Notifications &&
        runtimeType == other.runtimeType &&
        totalMessages == other.totalMessages &&
        message == other.message;

  @override
  int get hashCode => 
    totalMessages.hashCode ^
    message.hashCode;

  @override
  bool isEmpty() => message == null || message.isEmpty;
}