import 'package:flutter/rendering.dart';
import 'package:krispykreme/model/cities.dart';
import 'package:krispykreme/model/empty_checkable.dart';

class Cities with EmptyCheckable{
  int count;
  int totalRecords;
  List<City> cities;

  Cities({this.count, this.totalRecords, this.cities});

  Cities.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    totalRecords = json['total_records'];
    if (json['cities'] != null) {
      cities = new List<City>();
      json['cities'].forEach((v) {
        cities.add(new City.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    data['total_records'] = this.totalRecords;
    if (this.cities != null) {
      data['cities'] = this.cities.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  bool isEmpty() => cities == null || cities.isEmpty;
}
