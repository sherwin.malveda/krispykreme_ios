class Card{
  
  String cardPan;
  int cardStatus;
  int cardType;
  String cardExpiry;
  bool virtualCard;

  Card(
      {this.cardPan,
      this.cardStatus,
      this.cardType,
      this.cardExpiry,
      this.virtualCard});

  Card.fromJson(Map<String, dynamic> json) {
    cardPan = json['card_pan'];
    cardStatus = json['card_status'];
    cardType = json['card_type'];
    cardExpiry = json['card_expiry'];
    virtualCard = json['virtual_card'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['card_pan'] = this.cardPan;
    data['card_status'] = this.cardStatus;
    data['card_type'] = this.cardType;
    data['card_expiry'] = this.cardExpiry;
    data['virtual_card'] = this.virtualCard;
    return data;
  }

}