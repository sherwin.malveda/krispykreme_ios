class Promo {
  final int promoId;
  final String promoName;
  final String promoDescription;
  final String promoCategory;
  final String promoImage;
  final String startDate;
  final String endDate;
  final String promosVideo;
  final String shareableImage;

  Promo({
    this.promoId,
    this.promoName,
    this.promoDescription,
    this.promoCategory,
    this.promoImage,
    this.startDate,
    this.endDate,
    this.promosVideo,
    this.shareableImage
  });

  factory Promo.fromMap(Map<String, dynamic> map) {
    final int promoId = map['promo_id'] ?? -1;
    final String promoName = map['promo_name'] ?? "";
    final String promoDescription = map['promo_description'] ?? "";
    final String promoCategory = map['promo_category'] ?? "";
    final String promoImage = map['promo_image'] ?? "";
    final String startDate = map['start_date'] ?? "";
    final String endDate = map['end_date'] ?? "";
    final String promosVideo = map['promo_video'] ?? "";
    final String shareableImage = map['shareable_image'] ?? "";

    return Promo(
      promoId: promoId,
      promoName: promoName,
      promoDescription: promoDescription,
      promoCategory: promoCategory,
      promoImage: promoImage,
      startDate: startDate,
      endDate: endDate,
      promosVideo: promosVideo,
      shareableImage: shareableImage
    );
  }
}
