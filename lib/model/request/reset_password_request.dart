import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:meta/meta.dart';

class ResetPasswordRequest {
  final String code;
  final String newPassword;
  final String confirmPassword;
  final String username;

  ResetPasswordRequest({
    @required this.code,
    @required this.newPassword,
    @required this.confirmPassword,
    @required this.username
  });

  Map<String, dynamic> toMap() => {
    "reset_code": code,
    "new_password": newPassword,
    "confirm_password": confirmPassword,
    "username": username
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is ResetPasswordRequest &&
        runtimeType == other.runtimeType &&
        code == other.code &&
        newPassword == other.newPassword &&
        confirmPassword == other.confirmPassword &&
        username == other.username;

  @override
  int get hashCode =>
    code.hashCode ^ 
    newPassword.hashCode ^ 
    confirmPassword.hashCode^ 
    username.hashCode;
}
