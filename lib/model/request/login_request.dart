class LoginRequest {
  final String username;
  final String password;
  final String grantType;

  LoginRequest({
    this.username,
    this.password,
    this.grantType
  });

  toMap() => {
    "username": username, 
    "password": password, 
    "grant_type": grantType
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is LoginRequest &&
        runtimeType == other.runtimeType &&
        username == other.username &&
        password == other.password  &&
        grantType == other.grantType;

  @override
  int get hashCode =>
    username.hashCode ^
    password.hashCode ^
    grantType.hashCode;  
}
