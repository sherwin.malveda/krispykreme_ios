import 'package:flutter/material.dart';

class CancelVoucherRequest {
  final int accountId;
  final String cardPan;
  final String voucherCode;

  CancelVoucherRequest({
    @required this.accountId,
    @required this.cardPan,
    @required this.voucherCode
  });

  Map<String, dynamic> toMap() => {
    "voucher_code": voucherCode,
  };
}
