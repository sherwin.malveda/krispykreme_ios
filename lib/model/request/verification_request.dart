import 'package:meta/meta.dart';

class VerificationRequest {
  final String email;
  final String cardPan;

  VerificationRequest({
    @required this.email,
    @required this.cardPan
  });

  Map<String, dynamic> toMap() => {
    "email": email,
    "card_pan": cardPan
  };

  factory VerificationRequest.fromMap(Map<String, dynamic> map) {
    final String email = map['email'] ?? "";
    final String cardPan = map['card_pan'] ?? "";

    return VerificationRequest(email: email, cardPan: cardPan);
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is VerificationRequest &&
        runtimeType == other.runtimeType &&
        email == other.email &&
        cardPan == other.cardPan;

  @override
  int get hashCode => 
    email.hashCode ^ 
    cardPan.hashCode;
}
