import 'package:meta/meta.dart';

class ChangePasswordRequest {
  final int accountId;
  final String currentPassword;
  final String newPassword;
  final String confirmPassword;

  ChangePasswordRequest({
    @required this.accountId,
    @required this.currentPassword,
    @required this.newPassword,
    @required this.confirmPassword
  });

  Map<String, dynamic> toMap() => {
    "current_password": currentPassword,
    "new_password": newPassword,
    "confirm_password": confirmPassword
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is ChangePasswordRequest &&
        runtimeType == other.runtimeType &&
        currentPassword == other.currentPassword &&
        newPassword == other.newPassword &&
        confirmPassword == other.confirmPassword;

  @override
  int get hashCode =>
    currentPassword.hashCode ^
    newPassword.hashCode ^
    confirmPassword.hashCode;
}
