import 'package:meta/meta.dart';

class MemberRequest {
  final String cardNumber;
  final String email;

  MemberRequest({
    @required this.cardNumber,
    @required this.email
  });

  Map<String, dynamic> toMap() => {
    "card_pan": cardNumber,
    "email": email
  };

  factory MemberRequest.fromMap(Map<String, dynamic> map) {
    final String cardNumber = map['card_pan'] ?? "";
    final String email = map['email '] ?? "";

    return MemberRequest(cardNumber: cardNumber, email: email);
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is MemberRequest &&
        runtimeType == other.runtimeType &&
        cardNumber == other.cardNumber &&
        email == other.email;

  @override
  int get hashCode => 
    cardNumber.hashCode ^ 
    email.hashCode;
}