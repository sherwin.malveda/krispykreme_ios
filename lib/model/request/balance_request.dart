import 'package:meta/meta.dart';

class BalanceRequest {
  final int accountId;
  final String cardPan;

  BalanceRequest({
    @required this.accountId,
    @required this.cardPan
  });

  toMap() => {
    "card_pan": cardPan
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is BalanceRequest &&
        runtimeType == other.runtimeType &&
        cardPan == other.cardPan;

  @override
  int get hashCode => 
    cardPan.hashCode;
}