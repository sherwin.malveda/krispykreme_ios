import 'package:meta/meta.dart';

class ActivationRequest {
  final String activationCode;

  ActivationRequest({
    @required this.activationCode
  });

  Map<String, dynamic> toMap() => {
    "activation_code": activationCode
  };
}
