class ForgotPasswordRequest {
  final String email;

  ForgotPasswordRequest({
    this.email
  });

  Map<String, dynamic> toMap() => {
    "email": email,
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is ForgotPasswordRequest &&
        runtimeType == other.runtimeType &&
        email == other.email;

  @override
  int get hashCode => 
    email.hashCode;
}