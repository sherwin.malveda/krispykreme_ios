import 'package:meta/meta.dart';

class ConvertPointsRequest {
  final int accountId;
  final String cardPan;
  final double points;

  ConvertPointsRequest({
    @required this.accountId,
    @required this.cardPan,
    @required this.points
  });

  Map<String, dynamic> toMap() => {
    "card_pan": cardPan,
    "redeem_datetime": DateTime.now().toUtc().toIso8601String(),
    "tender": [
      {"tender_code": "PTS", "tender_amount": points}
    ]
  };
}
