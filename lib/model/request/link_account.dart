class LinkAccountRequest {
  final String account;
  final String url;
  final String email;
  final String username;

  LinkAccountRequest({
    this.account,
    this.url,
    this.email,
    this.username
  });

  Map<String, dynamic> toMap() => {
    "external_account": account,
    "external_account_link": url,
    "email_address": email,
    "username": username,
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is LinkAccountRequest &&
        runtimeType == other.runtimeType &&
        account == other.account &&
        url == other.url &&
        email == other.email&&
        username == other.username;

  @override
  int get hashCode => 
    account.hashCode ^
    url.hashCode ^ 
    email.hashCode ^ 
    username.hashCode;
}