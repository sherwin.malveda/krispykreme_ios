class Hotlight {
  int branchId;
  String branchName;
  String branchCode;
  String branchAddress;
  String branchDescription;
  String email;
  String landlineNo;
  String mobileNo;
  double longitude;
  double latitude;
  String startDate;
  String endDate;
  String expiration;

  Hotlight(
      {this.branchId,
      this.branchName,
      this.branchCode,
      this.branchAddress,
      this.branchDescription,
      this.email,
      this.landlineNo,
      this.mobileNo,
      this.longitude,
      this.latitude,
      this.startDate,
      this.endDate,
      this.expiration});

  Hotlight.fromJson(Map<String, dynamic> json) {
    branchId = json['branch_id'];
    branchName = json['branch_name'];
    branchCode = json['branch_code'];
    branchAddress = json['branch_address'];
    branchDescription = json['branch_description'];
    email = json['email'];
    landlineNo = json['landline_no'];
    mobileNo = json['mobile_no'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    startDate = json['time_opening'];
    endDate = json['time_closing'];
    expiration = json['end_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['branch_id'] = this.branchId;
    data['branch_name'] = this.branchName;
    data['branch_code'] = this.branchCode;
    data['branch_address'] = this.branchAddress;
    data['branch_description'] = this.branchDescription;
    data['email'] = this.email;
    data['landline_no'] = this.landlineNo;
    data['mobile_no'] = this.mobileNo;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['time_opening'] = this.startDate;
    data['time_closing'] = this.endDate;
    data['end_date'] = this.expiration;
    return data;
  }
}