class ActivationTransaction {
  final String posrefno;
  final String cardpan;

  ActivationTransaction({
    this.posrefno,
    this.cardpan,
  });

  factory ActivationTransaction.fromMap(Map<String, dynamic> map) {
    final String posrefno = map['pos_ref_no'] ?? "";
    final String cardpan = map['card_pan'] ?? "";

    return ActivationTransaction(
      posrefno: posrefno,
      cardpan: cardpan,
    );
  }
}
