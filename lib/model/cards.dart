import 'package:krispykreme/model/card.dart';
import 'package:krispykreme/model/empty_checkable.dart';

class Cards with EmptyCheckable{
  
  List<Card> cards;

  Cards({this.cards});

  Cards.fromJson(Map<String, dynamic> json) {
    if (json['cards'] != null) {
      cards = new List<Card>();
      json['cards'].forEach((v) {
        cards.add(new Card.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.cards != null) {
      data['cards'] = this.cards.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  bool isEmpty() => cards == null || cards.isEmpty;
}