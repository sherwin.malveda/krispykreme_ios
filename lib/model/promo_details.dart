import 'package:krispykreme/model/empty_checkable.dart';

class PromoDetails with EmptyCheckable{
  final int promoId;
  final String promoName;
  final String promoDescription;
  final String promoCategory;
  final String promoImage;
  final String startDate;
  final String endDate;
  final String promosVideo;
  final String shareableImage;

  PromoDetails({
    this.promoId,
    this.promoName,
    this.promoDescription,
    this.promoCategory,
    this.promoImage,
    this.startDate,
    this.endDate,
    this.promosVideo,
    this.shareableImage
  });

  factory PromoDetails.fromMap(Map<String, dynamic> map) {
    final int promoId = map['promo_id'] ?? -1;
    final String promoName = map['promo_name'] ?? "";
    final String promoDescription = map['promo_description'] ?? "";
    final String promoCategory = map['promo_category'] ?? "";
    final String promoImage = map['promo_image'] ?? "";
    final String startDate = map['start_date'] ?? "";
    final String endDate = map['end_date'] ?? "";
    final String promosVideo = map['promo_video'] ?? "";
    final String shareableImage = map['shareable_image'] ?? "";

    return PromoDetails(
      promoId: promoId,
      promoName: promoName,
      promoDescription: promoDescription,
      promoCategory: promoCategory,
      promoImage: promoImage,
      startDate: startDate,
      endDate: endDate,
      promosVideo: promosVideo,
      shareableImage: shareableImage
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is PromoDetails &&
        runtimeType == other.runtimeType &&
        promoId == other.promoId &&
        promoName == other.promoName &&
        promoDescription == other.promoDescription &&
        promoCategory == other.promoCategory &&
        promoImage == other.promoImage &&
        promosVideo == other.promosVideo &&
        shareableImage == other.shareableImage ;

  @override
  int get hashCode =>
    promoId.hashCode ^
    promoName.hashCode ^
    promoDescription.hashCode ^
    promoCategory.hashCode ^
    promoImage.hashCode ^
    promosVideo.hashCode ^
    shareableImage.hashCode;

  @override
  bool isEmpty() => false;
}
