import 'package:meta/meta.dart';

class Item {
  final int id;
  final String value;

  Item({
    @required this.id, 
    @required this.value
  });

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Item &&
        runtimeType == other.runtimeType &&
        id == other.id &&
        value == other.value;

  @override
  int get hashCode => 
    id.hashCode ^ value.hashCode;
}