class Branch {
  final int branchId;
  final String branchName;
  final String branchCode;
  final String branchAddress;
  final String branchDescription;
  final String email;
  final String landlineno;
  final String mobileno;
  final double latitude;
  final double longitude;
  final String startDate;
  final String endDate;

  Branch({
    this.branchId,
    this.branchName,
    this.branchCode,
    this.branchAddress,
    this.branchDescription,
    this.email,
    this.landlineno,
    this.mobileno,
    this.latitude,
    this.longitude,
    this.startDate,
    this.endDate
  });

  factory Branch.fromMap(Map<String, dynamic> map) {
    final int branchId = map['branch_id'] ?? -1;
    final String branchName = map['branch_name'] ?? "";
    final String branchCode = map['branch_code'] ?? "";
    final String branchAddress = map['branch_address'] ?? "";
    final String branchDescription = map['branch_description'] ?? "";
    final String email = map['email'] ?? "";
    final String landlineno = map['landline_no'] ?? "";
    final String mobileno = map['mobile_no'] ?? "";
    final double latitude = map['latitude'] ?? "";
    final double longitude = map['longitude'] ?? "";
    final String startDate = map['time_opening'] ?? "";
    final String endDate = map['time_closing'] ?? "";

    return Branch(
      branchId: branchId,
      branchName: branchName,
      branchCode: branchCode,
      branchAddress: branchAddress,
      branchDescription: branchDescription,
      email: email,
      landlineno: landlineno,
      mobileno: mobileno,
      latitude: latitude,
      longitude: longitude,
      startDate: startDate,
      endDate: endDate
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Branch &&
        runtimeType == other.runtimeType &&
        branchId == other.branchId &&
        branchName == other.branchName &&
        branchCode == other.branchCode &&
        branchAddress == other.branchAddress&&
        branchDescription == other.branchDescription&&
        email == other.email&&
        landlineno == other.landlineno&&
        mobileno == other.mobileno&&
        latitude == other.latitude&&
        longitude == other.longitude&&
        startDate == other.startDate&&
        endDate == other.endDate;

  @override
  int get hashCode =>
    branchId.hashCode ^
    branchName.hashCode ^
    branchCode.hashCode ^
    branchAddress.hashCode ^
    branchDescription.hashCode ^
    email.hashCode ^
    landlineno.hashCode ^
    mobileno.hashCode ^
    latitude.hashCode ^
    longitude.hashCode^
    startDate.hashCode ^
    endDate.hashCode;
}
