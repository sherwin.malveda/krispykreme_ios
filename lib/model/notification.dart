class NotificationModel {
  final int taskId;
  final int messageId;
  final String messageTitle;
  final String messageBody;
  final String messageContent;
  final String messageImage;
  final String messageIcon;

  NotificationModel({
    this.taskId,
    this.messageId,
    this.messageTitle,
    this.messageBody,
    this.messageContent,
    this.messageImage,
    this.messageIcon
  });

  factory NotificationModel.fromMap(Map<String, dynamic> map) {
    final int taskId = map['task_id'] ?? -1;
    final int messageId = map['message_id'] ?? -1;
    final String messageTitle = map['title'] ?? "";
    final String messageBody = map['body'] ?? "";
    final String messageContent = map['content'] ?? "";
    final String messageImage = map['content_image'] ?? "";
    final String messageIcon = map['icon_image'] ?? "";

    return NotificationModel(
        taskId: taskId,
        messageId: messageId,
        messageTitle: messageTitle,
        messageBody: messageBody,
        messageContent: messageContent,
        messageImage: messageImage,
        messageIcon: messageIcon
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is NotificationModel &&
              runtimeType == other.runtimeType &&
              messageId == other.messageId &&
              messageTitle == other.messageTitle &&
              messageBody == other.messageBody &&
              messageContent == other.messageContent&&
              messageImage == other.messageImage&&
              messageIcon == other.messageIcon;

  @override
  int get hashCode =>
      messageId.hashCode ^
      messageTitle.hashCode ^
      messageBody.hashCode ^
      messageContent.hashCode^
      messageImage.hashCode^
      messageIcon.hashCode;
}
