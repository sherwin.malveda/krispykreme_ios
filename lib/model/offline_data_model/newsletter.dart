class OfflineNewsLetter {
  final int id;
  final String title;
  final String content;
  final String imagePath;

  OfflineNewsLetter(this.id,
      this.title,
      this.content,
      this.imagePath);
}