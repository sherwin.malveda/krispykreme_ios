class ImageBackground {
  final int backgroundType;
  final String imageUrl;

  ImageBackground(
    this.backgroundType,
    this.imageUrl,
  );
}
