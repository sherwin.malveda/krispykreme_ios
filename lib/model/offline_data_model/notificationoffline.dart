class NotificationOffline {
  final String title;
  final String body;
  final String datetime;
  final String imageurl;
  final String notificon;

  NotificationOffline(this.title,
      this.body,
      this.datetime,
      this.imageurl,
      this.notificon);
}
