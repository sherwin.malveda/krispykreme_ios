import 'package:krispykreme/model/empty_checkable.dart';

class Voucher with EmptyCheckable {
  final int voucherId;
  final int voucherType;
  final String voucherCode;
  final String description;
  final int status;
  final String expiration;
  final String campainDescription;

  Voucher({
    this.voucherId, 
    this.voucherType, 
    this.voucherCode, 
    this.description, 
    this.status, 
    this.expiration, 
    this.campainDescription
  });

  factory Voucher.fromMap(Map<String, dynamic> map) {
    final int voucherId = map['voucher_id'] ?? "";
    final int voucherType = map['voucher_type'] ?? "";
    final String voucherCode = map['voucher_code'] ?? "";
    final String description = map['voucher_description'] ?? "";
    final int status = map['voucher_status'] ?? "";
    final String expiration = map['voucher_expiration'] ?? "";
    final String campainDescription = map['campaign_description'] ?? "";

    return Voucher(
      voucherId: voucherId,
      voucherType: voucherType,
      voucherCode: voucherCode,
      description: description,
      status: status,
      expiration: expiration,
      campainDescription: campainDescription
    );
  }

  @override
  bool isEmpty() => false;
}