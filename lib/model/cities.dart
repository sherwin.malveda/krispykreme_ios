class City {
  int cityId;
  String cityName;
  int stateRegionId;
  String stateRegionName;
  int regionId;
  String regionCode;
  String regionName;

  City(
      {this.cityId,
      this.cityName,
      this.stateRegionId,
      this.stateRegionName,
      this.regionId,
      this.regionCode,
      this.regionName});

  City.fromJson(Map<String, dynamic> json) {
    cityId = json['city_id'];
    cityName = json['city_name'];
    stateRegionId = json['stateRegion_id'];
    stateRegionName = json['stateRegion_name'];
    regionId = json['region_id'];
    regionCode = json['region_code'];
    regionName = json['region_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city_id'] = this.cityId;
    data['city_name'] = this.cityName;
    data['stateRegion_id'] = this.stateRegionId;
    data['stateRegion_name'] = this.stateRegionName;
    data['region_id'] = this.regionId;
    data['region_code'] = this.regionCode;
    data['region_name'] = this.regionName;
    return data;
  }
}
