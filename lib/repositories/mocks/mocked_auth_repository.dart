import 'package:dio/dio.dart';
import 'package:krispykreme/dao/mao_dao.dart';
import 'package:krispykreme/model/activation.dart';
import 'package:krispykreme/model/request/activation_request.dart';
import 'package:krispykreme/model/request/change_password_request.dart';
import 'package:krispykreme/model/request/forgot_password_password.dart';
import 'package:krispykreme/model/request/link_account.dart';
import 'package:krispykreme/model/request/login_request.dart';
import 'package:krispykreme/model/request/member_request.dart';
import 'package:krispykreme/model/request/reset_password_request.dart';
import 'package:krispykreme/model/request/verification_request.dart';
import 'package:krispykreme/model/session.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/repositories/contracts/auth_repository.dart';

class MockedAuthRepository implements AuthRepository {
  MapDao mapDao = MapDao();

  @override
  Future<void> changePassword(ChangePasswordRequest changePassRequest) async {
    await Future.delayed(Duration(seconds: 1));
  }

  @override
  Future<void> forgotPassword(ForgotPasswordRequest request) async {
    await Future.delayed(Duration(seconds: 1));
  }

  @override
  Future<Session> login(LoginRequest request, String method, String fcmToken) async {
    final Map<String, dynamic> responseMap = {
      "account_id": 5,
      "token":
        "XVrLiAjgImiZGylllb3YpWAJVRgAkcphQWbRbR05P1ssis4HJJQY6pozbk09iX2U5w4VJP4e0fM7zkwPYaUUUPyBG_fM7zkwPYaUUUPyBG_4PYDHxI-BDgYAthyc4RGpIi4JGXR",
      "token_type": "Bearer",
      "expiry": "2019/03/14 10:00:36 PM",
      "card_pan": "6000089117"
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    if (response.statusCode == 200) {
      return Session.fromMap(response.data)..saveMap(mapDao);
    } else
      throw Exception("Invalid credentials");
  }

  @override
  Future<VerificationRequest> register(User user) async {
    final Map<String, dynamic> map = {
      "account_id": 5,
      "email": "jezer",
      "card_number": "6000089117"
    };
    await Future.delayed(Duration(seconds: 1));
    return VerificationRequest.fromMap(map);
  }

  @override
  Future<void> activateAccount(ActivationRequest request) async {
    await Future.delayed(Duration(seconds: 1));
  }

  @override
  Future<void> verifyAccount(VerificationRequest request) async {
    await Future.delayed(Duration(seconds: 1));
  }

  @override
  Future<Session> getCachedSession() async {
    final Map<String, dynamic> emptyMap = {
      "account_id": -1,
      "token": "",
      "token_type": "",
      "expiry": "",
      "card_pan": ""
    };
    return Session.fromMap(emptyMap);
  }

  @override
  Future<void> resetPassword(ResetPasswordRequest request) async {
    await Future.delayed(Duration(seconds: 1));
  }

  @override
  Future<User> existingMember(MemberRequest request, String cardNumber) {
    return null;
  }

  @override
  Future<User> activate(Activation activation, String cardNumber) {
    return null;
  }

  @override
  Future<void> linkAccount(LinkAccountRequest request) {
      throw UnimplementedError();
    }
  
    @override
    Future<void> unlinkAccount(LinkAccountRequest request) {
    throw UnimplementedError();
  }
}