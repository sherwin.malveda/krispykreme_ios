import 'package:krispykreme/model/balance.dart';
import 'package:krispykreme/model/cards.dart';
import 'package:krispykreme/model/city.dart';
import 'package:krispykreme/model/history.dart';
import 'package:krispykreme/model/request/balance_request.dart';
import 'package:krispykreme/model/request/cancel_voucher_request.dart';
import 'package:krispykreme/model/request/convert_points_request.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/model/voucher.dart';
import 'package:krispykreme/model/vouchers.dart';

abstract class CardRepository {
  Future<User> getAccountDetails(String accountId);

  Future<Cards> getCardDetails(String accountId);

  Future<void> updateAccount(String accountId, User user);

  Future<void> cancelVoucher(CancelVoucherRequest request);

  Future<Balance> getBalance(BalanceRequest request);

  Future<History> getHistory(ListRequest request);

  Future<Vouchers> getVouchers(ListRequest request);

  Future<Voucher> getVoucherDetails(String username, int voucherId);

  Future<Voucher> convertPoints(ConvertPointsRequest request);

  Future<Cities> getCities();
}
