import 'package:krispykreme/model/banners.dart';
import 'package:krispykreme/model/branch_details.dart';
import 'package:krispykreme/model/branches.dart';
import 'package:krispykreme/model/news_letter.dart';
import 'package:krispykreme/model/news_letters.dart';
import 'package:krispykreme/model/notifications.dart';
import 'package:krispykreme/model/promo_details.dart';
import 'package:krispykreme/model/promos.dart';
import 'package:krispykreme/model/request/list_request.dart';

abstract class StoreRepository {

  Future<Promos> getPromos(ListRequest request);

  Future<Promos> getPromosVideos(ListRequest request);

  Future<PromoDetails> getPromo(int promoId);

  Future<PromoDetails> getPromoVideo(int promoId);

  Future<NewsLetters> newsLetters(ListRequest request);

  Future<NewsLetter> getNewsletter(int newsletterId);

  Future<Branches> getBranches(ListRequest request);

  Future<BranchDetails> getBranch(int branchId);

  Future<Banners> getBanners(ListRequest request);

  Future<Notifications> getNotifications(ListRequest request);
  
  Future<void> deleteNotification(ListRequest request);
}
