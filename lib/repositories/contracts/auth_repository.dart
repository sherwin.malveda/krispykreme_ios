import 'package:krispykreme/model/activation.dart';
import 'package:krispykreme/model/request/activation_request.dart';
import 'package:krispykreme/model/request/change_password_request.dart';
import 'package:krispykreme/model/request/forgot_password_password.dart';
import 'package:krispykreme/model/request/link_account.dart';
import 'package:krispykreme/model/request/login_request.dart';
import 'package:krispykreme/model/request/member_request.dart';
import 'package:krispykreme/model/request/reset_password_request.dart';
import 'package:krispykreme/model/request/verification_request.dart';
import 'package:krispykreme/model/session.dart';
import 'package:krispykreme/model/user.dart';

abstract class AuthRepository {
  
  Future<User> existingMember(MemberRequest request, String cardNumber);

  Future<Session> login(LoginRequest request, String method, String fcmToken);

  Future<void> forgotPassword(ForgotPasswordRequest request);

  Future<void> linkAccount(LinkAccountRequest request);

  Future<void> unlinkAccount(LinkAccountRequest request);

  Future<VerificationRequest> register(User user);

  Future<void> activate(Activation activation, String cardNumber);

  Future<void> changePassword(ChangePasswordRequest request);

  Future<void> resetPassword(ResetPasswordRequest request);

  Future<void> activateAccount(ActivationRequest request);

  Future<void> verifyAccount(VerificationRequest request);
  
  Future<Session> getCachedSession();
}
