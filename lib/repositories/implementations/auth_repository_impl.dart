import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/model/activation.dart';
import 'package:krispykreme/model/request/link_account.dart';
import 'package:meta/meta.dart';
import 'package:krispykreme/api/api.dart';
import 'package:krispykreme/api/api_uris.dart';
import 'package:krispykreme/dao/mao_dao.dart';
import 'package:krispykreme/model/request/activation_request.dart';
import 'package:krispykreme/model/request/change_password_request.dart';
import 'package:krispykreme/model/request/forgot_password_password.dart';
import 'package:krispykreme/model/request/login_request.dart';
import 'package:krispykreme/model/request/member_request.dart';
import 'package:krispykreme/model/request/reset_password_request.dart';
import 'package:krispykreme/model/request/verification_request.dart';
import 'package:krispykreme/model/session.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/repositories/contracts/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final Api api;
  final MapDao mapDao;

  AuthRepositoryImpl({@required this.api, @required this.mapDao});

  @override
  Future<User> existingMember(MemberRequest request, String cardNumber) async {
    final Response response = await api.post(
      ApiUris.replaceParam(ApiUris.CARDVERIFICATION, "accountId", cardNumber),
      request.toMap(),
      false
    );
    return User.fromMap(response.data);
  }

  @override
  Future<Session> login(LoginRequest request, String method, String fcmToken) async {
    final Response response = await api.postLogin(ApiUris.LOGIN, request, method, fcmToken);

    return Session.fromMap(response.data)..saveMap(mapDao);
  }

  @override
  Future<void> forgotPassword(ForgotPasswordRequest request) async {
    await api.post(
      ApiUris.replaceParam(ApiUris.FORGOT_PASSWORD, "username", request.email),
      null, 
      false);
  }

  @override
  Future<VerificationRequest> register(User user) async {
    final Response response = await api.post(
      ApiUris.REGISTRATION, user.toMap(), false
    );
    return VerificationRequest.fromMap(response.data);
  }

  @override
  Future<void> activate(Activation activation, String cardNumber) async {
    await api.post(
      ApiUris.replaceParam(ApiUris.CARDACTICATION, "accountId", cardNumber), 
      activation.toJson(), false
    );
  }

  @override
  Future<void> changePassword(ChangePasswordRequest changePassRequest) async {
    await api.post(
      ApiUris.replaceParam(ApiUris.CHANGE_PASSWORD, "accountId", changePassRequest.accountId),
      changePassRequest.toMap(),
      true
    );
  }

  @override
  Future<void> activateAccount(ActivationRequest request) async {
    await api.post(ApiUris.ACTIVATE, request.toMap(), false);
  }

  @override
  Future<void> verifyAccount(VerificationRequest request) async {
    await api.post(ApiUris.VERIFY, request.toMap(), false);
  }

  @override
  Future<Session> getCachedSession() async {
    return Session.fromDao(MapDao.withPrefs(await mapDao.init()));
  }

  @override
  Future<void> resetPassword(ResetPasswordRequest request) async {
    await api.post(
      ApiUris.replaceParam(ApiUris.RESET_PASSWORD, "username", request.username),
      request.toMap(), 
      false);
  }

  @override
  Future<void> linkAccount(LinkAccountRequest request) async {
    await api.put(
      ApiUris.replaceParam(ApiUris.LINK_ACCOUNT, "accountId", request.username),
      request.toMap(), 
      true);
  }

  @override
  Future<void> unlinkAccount(LinkAccountRequest request) async {
    await api.put(
      ApiUris.replaceParam(ApiUris.UNLINK_ACCOUNT, "accountId", request.username),
      request.toMap(), 
      true);
  }

}
