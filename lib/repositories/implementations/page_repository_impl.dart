import 'package:dio/dio.dart';
import 'package:krispykreme/api/api.dart';
import 'package:krispykreme/api/api_uris.dart';
import 'package:krispykreme/model/pages_background.dart';
import 'package:krispykreme/repositories/contracts/page_repository.dart';

class PageRepositoryImpl implements PageRepository {
  final Api api;

  PageRepositoryImpl({this.api});

  Future<PageBackground> getPagesBackground(int pageId) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.REGISTRATION_BACKGROUND, "pageId", pageId),
      false
    );
    return PageBackground.fromMap(response.data);
  }

  @override
  Future getTermsandCondition(int pageId) async{
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.TERMS_AND_CONDITION, "pageId", pageId),
      false
    );
    return response.data;
  }

  @override
  Future getAboutUs(int pageId) async{
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.ABOUT_US, "pageId", pageId),
      false
    );
    return response.data;
  }

  @override
  Future getContactUs(int pageId) async{
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.CONTACT_US, "pageId", pageId),
      false
    );
    return response.data;
  }
}