import 'package:dio/dio.dart';
import 'package:krispykreme/api/api.dart';
import 'package:krispykreme/api/api_uris.dart';
import 'package:krispykreme/model/banners.dart';
import 'package:krispykreme/model/branch_details.dart';
import 'package:krispykreme/model/branches.dart';
import 'package:krispykreme/model/news_letter.dart';
import 'package:krispykreme/model/news_letters.dart';
import 'package:krispykreme/model/notifications.dart';
import 'package:krispykreme/model/promo_details.dart';
import 'package:krispykreme/model/promos.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/repositories/contracts/store_repository.dart';

class StoreRepositoryImpl implements StoreRepository {
  final Api api;

  StoreRepositoryImpl({this.api});

  Future<Promos> getPromos(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.PROMOTIONS, request), 
      false
    );
    return Promos.fromMap(response.data);
  }

    Future<Promos> getPromosVideos(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.PROMOTIONS_VDIEOS, request), 
      false
    );
    return Promos.fromMap(response.data);
  }


  Future<PromoDetails> getPromo(int promoId) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.PROMO_DETAILS, "promoId", promoId), 
      true
    );
    return PromoDetails.fromMap(response.data);
  }

    Future<PromoDetails> getPromoVideo(int promoId) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.PROMO_VIDEOS_DETAILS, "promoId", promoId), 
      true
    );
    return PromoDetails.fromMap(response.data);
  }

  Future<Branches> getBranches(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.BRANCHES, request), 
      false
    );
    return Branches.fromMap(response.data);
  }

  @override
  Future<NewsLetters> newsLetters(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.NEWS_LETTER, request), 
      false
    );
    return NewsLetters.fromMap(response.data);
  }

    Future<NewsLetter> getNewsletter(int newsletterId) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.NEWS_LETTER_DETAILS, "newsletter_id", newsletterId), 
      true
    );
    return NewsLetter.fromMap(response.data);
  }

  Future<BranchDetails> getBranch(int branchId) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.BRANCH_INFO, "branchId", branchId),
      true
    );
    return BranchDetails.fromMap(response.data);
  }

  Future<Banners> getBanners(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.BANNER_LIST, request),
      false
    );
    return Banners.fromMap(response.data);
  }

  Future<Notifications> getNotifications(ListRequest request) async {
    final Response response = await api.get(
        ApiUris.attachPaginationToUri2(ApiUris.NOTIFICATION_LIST, "username", request.accountId, request),
        true
    );
    return Notifications.fromMap(response.data);
  }

  Future<void> deleteNotification(ListRequest request) async {
    await api.delete(
        ApiUris.attachPaginationToUri2(ApiUris.NOTIFICATION_LIST, "username", request.accountId, request),
        true
    );
  }
}