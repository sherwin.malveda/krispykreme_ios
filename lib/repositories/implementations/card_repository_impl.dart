import 'package:dio/dio.dart';
import 'package:krispykreme/api/api.dart';
import 'package:krispykreme/api/api_uris.dart';
import 'package:krispykreme/model/balance.dart';
import 'package:krispykreme/model/cards.dart';
import 'package:krispykreme/model/city.dart';
import 'package:krispykreme/model/history.dart';
import 'package:krispykreme/model/request/balance_request.dart';
import 'package:krispykreme/model/request/cancel_voucher_request.dart';
import 'package:krispykreme/model/request/convert_points_request.dart';
import 'package:krispykreme/model/request/list_request.dart';
import 'package:krispykreme/model/user.dart';
import 'package:krispykreme/model/voucher.dart';
import 'package:krispykreme/model/vouchers.dart';
import 'package:krispykreme/repositories/contracts/card_repository.dart';

class CardRepositoryImpl implements CardRepository {
  final Api api;

  CardRepositoryImpl({this.api});

  Future<User> getAccountDetails(String accountId) async {
    final Response response = await api.get(ApiUris.replaceParam(ApiUris.ACCOUNT_INFO, "accountId", accountId),
      true
    );
    return User.fromMap(response.data);
  }

  Future<Cards> getCardDetails(String accountId) async {
    final Response response = await api.get(ApiUris.replaceParam(ApiUris.ACCOUNT_CARD, "accountId", accountId),
      true
    );
    return Cards.fromJson(response.data);
  }

  Future<void> updateAccount(String accountId, User user) async {
    await api.put(ApiUris.replaceParam(ApiUris.ACCOUNT_EDIT, "accountId", accountId),
      user.toMap(),
      true
    );
  }

  Future<Balance> getBalance(BalanceRequest request) async {
    final Response response = await api.post(
      ApiUris.replaceParam(ApiUris.BALANCE_INQUIRY, "accountId", request.accountId),
      request.toMap(),
      true
    );
    return Balance.fromMap(response.data);
  }

  Future<History> getHistory(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.TRANSACTION_HISTORY, "accountId", request.accountId),
      true
    );
    return History.fromMap(response.data);
  }

  Future<Vouchers> getVouchers(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.VOUCHER_LIST, "accountId", request.accountId),
      true
    );
    return Vouchers.fromMap(response.data);
  }

    Future<Voucher> getVoucherDetails(String username,int voucherid) async {
    final Response response = await api.get(
      ApiUris.replaceParam2(ApiUris.VOUCHER, "voucher_id", voucherid,
        "accountId",username), 
      true
    );
    return Voucher.fromMap(response.data);
  }

  @override
  Future<Voucher> convertPoints(ConvertPointsRequest request) async {
    final Response response = await api.post(
      ApiUris.replaceParam(ApiUris.REWARDS, "accountId", request.accountId),
      request.toMap(),
      true
    );
    return Voucher.fromMap(response.data);
  }

  @override
  Future<void> cancelVoucher(CancelVoucherRequest request) async {
    await api.post(
      ApiUris.replaceParam(ApiUris.CANCEL_VOUCHER, "accountId", request.accountId),
      request.toMap(),
      true
    );
  }

  @override
  Future<Cities> getCities() async {
    final Response response = await api.get(
      ApiUris.CITIES,true
    );
    return Cities.fromJson(response.data);
  }
}
