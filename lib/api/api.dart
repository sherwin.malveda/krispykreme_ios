import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:krispykreme/model/request/login_request.dart';
import 'package:meta/meta.dart';
import 'package:krispykreme/api/api_uris.dart';
import 'package:krispykreme/api/security_interceptor.dart';
import 'package:krispykreme/api/session_invalidation_notifier.dart';
import 'package:krispykreme/api/token_inteceptor.dart';
import 'package:krispykreme/dao/mao_dao.dart';
import 'package:krispykreme/model/session.dart';
import 'package:krispykreme/repositories/response_validator.dart';

class Api extends ResponseValidator {
  static const String API_KEY =
    "S0tNb2JpbGVDbXM6NjAzRkJGMTAtOUM4QS00MDNGLTkxN0ItMEFGRjk2QkUxNUYx";

  final Dio dio;
  final TokenInterceptor tokenInterceptor;
  final LogInterceptor logInterceptor;
  final SecurityInterceptor securityInterceptor;
  final SessionInvalidationNotifier sessionInvalidationNotifier;
  final MapDao mapDao;

  Api({@required this.dio,
    @required this.mapDao,
    @required this.tokenInterceptor,
    @required this.securityInterceptor,
    @required this.sessionInvalidationNotifier,
    this.logInterceptor})
      : super(sessionInvalidationNotifier) {
    dio.options.baseUrl = ApiUris.BASE_URL;
    dio.interceptors.add(securityInterceptor);
    dio.interceptors.add(tokenInterceptor);
    dio.interceptors.add(logInterceptor);
  }

  Future<Response> get(String path, bool isAuthenticated) async {
    Response get;
    try {
      if (isAuthenticated) {
        final String authorization = mapDao.getString(Session.KEY_TOKEN);
        final String tokenType = mapDao.getString(Session.KEY_TOKEN_TYPE);
        get = await dio.get(ApiUris.VERSION_URI+path,
          options: Options(
            headers: {"authorization": "$tokenType $authorization"}
          )
        );
        return get;
      } else {
        get = await dio.get(ApiUris.VERSION_URI+path);
      }
      checkError(get);
    } catch (error) {
      checkError(error.response);
    }
    return get;
  }

  Future<Response> post(String path, Map<String, dynamic> body,
      bool isAuthenticated) async {
    Response post;
    try {
      if (isAuthenticated) {
        final String authorization = mapDao.getString(Session.KEY_TOKEN);
        final String tokenType = mapDao.getString(Session.KEY_TOKEN_TYPE);
        post = await dio.post(ApiUris.VERSION_URI+path,
          data: body,
          options: Options(
            headers: {"authorization": "$tokenType $authorization"}
          )
        );
        return post;
      } else {
        post = await dio.post(ApiUris.VERSION_URI+path, data: body);
      }
      checkError(post);
    } catch (error) {
      checkError(error.response);
    }
    return post;
  }

   Future<Response> put(String path, Map<String, dynamic> body,
      bool isAuthenticated) async {
    Response post;
    try {
      if (isAuthenticated) {
        final String authorization = mapDao.getString(Session.KEY_TOKEN);
        final String tokenType = mapDao.getString(Session.KEY_TOKEN_TYPE);
        post = await dio.put(ApiUris.VERSION_URI+path,
          data: body,
          options: Options(
            headers: {"authorization": "$tokenType $authorization"}
          )
        );
        return post;
      } else {
        post = await dio.put(ApiUris.VERSION_URI+path, data: body);
      }
      checkError(post);
    } catch (error) {
      checkError(error.response);
    }
    return post;
  }

  Future<Response> delete(String path, bool isAuthenticated) async {
    Response delete;
    try {
      if (isAuthenticated) {
        final String authorization = mapDao.getString(Session.KEY_TOKEN);
        final String tokenType = mapDao.getString(Session.KEY_TOKEN_TYPE);
        delete = await dio.delete(ApiUris.VERSION_URI+path,
          options: Options(
            headers: {"authorization": "$tokenType $authorization"}
          )
        );
        return delete;
      } else {
        delete = await dio.delete(ApiUris.VERSION_URI+path);
      }
      checkError(delete);
    } catch (error) {
      checkError(error.response);
    }
    return delete;
  }
  
  Future<Response> postLogin(String path,LoginRequest request,String methods, String fcmToken) async {
    Response post;
    try {
      String username = 'KKMobileApp';
      String password = '3813F084-291A-49FA-B3D6-3F3759A36DB3';
      String basicAuth =
          'Basic ' + base64Encode(utf8.encode('$username:$password'));
  
      Map<String, String> qParams;

      if(methods == null || methods == ""){
        qParams = {
          "pn_token" : fcmToken
        };
      }else{
        qParams = {
          "external_account" : methods,
          "pn_token" : fcmToken
        };
      }

      post = await dio.post(
        path,
        data : {
          "username" : request.username,
          "password" : request.password,
          "grant_type" : "password",
        },
        options: Options(
          headers: {
            HttpHeaders.authorizationHeader: basicAuth,
          },
          contentType:Headers.formUrlEncodedContentType 
        ),
        queryParameters: qParams
      );
      checkError(post);
    } catch (error) {
      checkError(error.response);
    }
    return post;
  }
}