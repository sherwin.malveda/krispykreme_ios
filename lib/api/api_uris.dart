import 'package:krispykreme/constants.dart';
import 'package:krispykreme/model/request/list_request.dart';

class ApiUris {

  static const String BASE_URL = API_ENDPOINT;
  static const String VERSION_URI = "/api/v1";
  static const String REGISTRATION = "/register/account";
  static const String CARDVERIFICATION = "/cards/{accountId}/verify";
  static const String CARDACTICATION = "/cards/{accountId}/activate";
  static const String LOGIN = "/oauth/token";
  static const String ACTIVATE = "/activate/account";
  static const String VERIFY = "/verify/account";
  static const String TOKEN = "/login/token";
  static const String FACEBOOKLINK = "/accounts/{accountId}/link";
  static const String ACCOUNT_INFO = "/accounts/{accountId}";
  static const String ACCOUNT_CARD = "/accounts/{accountId}/cards";
  static const String ACCOUNT_EDIT = "/accounts/{accountId}";
  static const String LINK_ACCOUNT = "/accounts/{accountId}/link";
  static const String UNLINK_ACCOUNT = "/accounts/{accountId}/unlink";
  static const String BALANCE_INQUIRY = "/accounts/{accountId}/cards";
  static const String TRANSACTION_HISTORY = "/accounts/{accountId}/transactions/1/30";
  static const String PROMOTIONS = "/content/promotions";
  static const String PROMOTIONS_VDIEOS = "/content/promotions/videos/";
  static const String PROMO_DETAILS = "/content/promotions/{promoId}";
  static const String PROMO_VIDEOS_DETAILS = "/content/promotions/videos/{promoId}";
  static const String NEWS_LETTER = "/content/newsletters";
  static const String NEWS_LETTER_DETAILS = "/content/newsletters/{newsletter_id}";
  static const String BRANCHES = "/content/branches";
  static const String BRANCH_INFO = "/branch/info/{branchId}";
  static const String FORGOT_PASSWORD = "/accounts/{username}/forgot-password";
  static const String RESET_PASSWORD = "/accounts/{username}/reset-password";
  static const String CHANGE_PASSWORD = "/account/changepassword/{accountId}";
  static const String VOUCHER_LIST = "/accounts/{accountId}/vouchers/1/30";
  static const String VOUCHER = "/accounts/{accountId}/vouchers/{voucher_id}";
  static const String REWARDS = "/voucher/redeem/{accountId}";
  static const String CANCEL_VOUCHER = "/voucher/cancel/{accountId}";
  static const String REGISTRATION_BACKGROUND = "/page/{pageId}";
  static const String BANNER_LIST = "/banner";
  static const String TERMS_AND_CONDITION = "/termsandcondition";
  static const String ABOUT_US = "/aboutus";
  static const String CONTACT_US = "/contactus";
  static const String PROVINCE = "/dropdown/provinces";
  static const String CITIES = "/data/address/cities";
  static const String NOTIFICATION_LIST = "/accounts/{username}/notifications";
  
  static String replaceParam(String uri, String param, dynamic value) {
    return uri.replaceFirst("{$param}", "$value");
  }

    static String replaceParam2(String uri, String param, dynamic value, String param2, dynamic value2) {
    uri = uri.replaceFirst("{$param}", "$value");
    return uri.replaceFirst("{$param2}", "$value2");
  }

  static String attachPaginationToUri(String uri, ListRequest request) {
    return "$uri/${request.page}/${request.rows}";
  }

  static String attachPaginationToUri2(String uri, String param, dynamic value, ListRequest request) {
    uri = uri.replaceFirst("{$param}", "$value");
    return "$uri/${request.page}/${request.rows}";
  }
}