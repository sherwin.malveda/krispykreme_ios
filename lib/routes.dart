class Routes {
  static const String SPLASH_SCREEN = "/";
  static const String GETTING_STARTED = "/gettingstarted";
  static const String REGISTRATION_QUESTION = "/registration_question";
  static const String LANDING = "/landing_page";
  static const String REGISTRATION = "/registration";
  static const String REGISTER = "/register";
  static const String LOGIN_QUESTION_PAGE = "/login_question";
  static const String LOGIN = "/login";
  static const String DASHBOARD = "/dashboard";
  static const String FORGOT_PASSWORD = "/forgot_password";
  static const String FORGOT_RESET_PASSWORD = "/forgot_reset_password";
  static const String BRANCHES = "/branches";
  static const String CARD = "/card";
  static const String OTP = "/member_exist";
  static const String REWARDS = "/rewards";
  static const String VOUCHERS = "/vouchers";
  static const String NEW_VOUCHER = "/new_voucher";
  static const String VOUCHER_DETAILS = "/voucher_details";
  static const String PROMOS = "/promos";
  static const String NEWSLETTER = "/newsletter";
  static const String VIDEOS = "/videos";
  static const String HISTORY = "/history";
  static const String ACCOUNT_VERIFICATION = "/account_verification";
  static const String ACCOUNT_VERIFICATION2 = "/account_verification2";
  static const String SUCCESS_PAGE = "/success";
  static const String NOTIFICATION = "/notif";
  static const String NOTIFICATION_BODY = "/notif_body";
  static const String TERMS_AND_CONDITIONS = "/terms_and_conditions";
  static const String ABOUT_US = "/about_us";
  static const String CONTACT_US = "/contact_us";
  static const String FAQS = "/faqs";
  static const String PROFILEPAGE = "/profile";
  static const String EDIT_PROFILE = "/edit_profile";
  static const String CHANGE_PASSWORD = "/change_password";
}
