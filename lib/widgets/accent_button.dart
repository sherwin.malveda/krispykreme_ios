import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';

class AccentButton extends StatelessWidget {

  final Widget child;
  final VoidCallback onPressed;

  const AccentButton({Key key, @required this.child, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: AppColors.green,
      padding: EdgeInsets.symmetric(vertical: 16.0,horizontal: 24.0),
      onPressed: onPressed,
      child: child,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24.0)),
      elevation: 4.0,
    );
  }
}
