import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';

class VoucherList extends StatelessWidget {

  final VoidCallback onTap;
  final String voucher;
  final int type;

  const VoucherList({Key key, @required this.onTap,  @required this.voucher,  @required this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(15, 10, 10, 0),
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius:BorderRadius.only(topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0), bottomRight: Radius.circular(15.0))),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0), bottomRight: Radius.circular(15.0))
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * .20,
              width: MediaQuery.of(context).size.width * 1,
              decoration: BoxDecoration(
                color: AppColors.blue2,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0))
              ),
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * .20,
                    width: MediaQuery.of(context).size.width * 1,
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0)),
                      child:Image.asset("assets/image/voucher.png", fit: BoxFit.cover)
                    )
                  ),
                  Positioned(
                    left:  MediaQuery.of(context).size.width * .135,
                    top: MediaQuery.of(context).size.height * .03,
                    child:Container(
                      height: MediaQuery.of(context).size.height * .133,
                      width: MediaQuery.of(context).size.width * .276,
                      child:voucherImage(type),
                    )
                  )
                ],
              )
            ),
            SizedBox(height: 10),
            Container(
              padding: EdgeInsets.fromLTRB(10,0,0,0),
              child:Text(voucher,
                style: TextStyle(
                  fontSize: 15.0, 
                  color: AppColors.black,
                  fontWeight: FontWeight.bold
                ),
                textAlign: TextAlign.start,
              )
            ),
            SizedBox(height: MediaQuery.of(context).size.width * .02),
            Container(
              height: 40,
              alignment: Alignment.center,
              child: Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(10.0),
                color: AppColors.green,
                child: MaterialButton(
                  height: 40,
                  shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
                  child:Text("KNOW MORE", 
                    style: TextStyle(fontSize: 12.0, color: AppColors.white),textAlign: TextAlign.center,),
                  onPressed: onTap
                ),
              )
            ),
            SizedBox(height: MediaQuery.of(context).size.width * .05,)
          ],
        )
      )
    );
  }

  Widget voucherImage(int type){
    if(type == 1){
      return Image.asset("assets/image/donut.png", fit: BoxFit.contain);
    }else if(type == 2){
      return Image.asset("assets/image/boxes.jpg", fit: BoxFit.contain);
    } else if(type == 3){
      return Image.asset("assets/image/donuts.jpg", fit: BoxFit.contain);
    }else{
      return Image.asset("assets/image/logo.png", fit: BoxFit.contain);
    }
  }
}
