import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  final String hintText;
  final bool masked;
  final FormFieldValidator<String> validator;
  final bool autoValidate;
  final TextEditingController controller;
  final int maxLength;
  final TextInputType keyboardType;
  final TextCapitalization textCapitalization;
  final FocusNode focusNode;
  final Function submit;
  final Function change;
  final bool enabled;

  const InputField({
    Key key,
    this.hintText,
    @required this.controller,
    this.validator,
    this.keyboardType,
    this.textCapitalization = TextCapitalization.none,
    this.autoValidate = false,
    this.masked = false,
    this.maxLength,
    this.focusNode,
    this.submit,
    this.change,
    this.enabled = true
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: enabled,
      focusNode: focusNode,
      obscureText: masked,
      validator: validator,
      autovalidate: autoValidate,
      style: TextStyle(fontFamily: 'Quicksand'),
      keyboardType: keyboardType,
      textCapitalization: textCapitalization,
      controller: controller,
      maxLength: maxLength,
      onChanged:change,
      decoration: InputDecoration(
        counterText: '',
        labelText: hintText,
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5),
        border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(20.0),
        )
      ),
      onFieldSubmitted: submit,
    );
  }
}