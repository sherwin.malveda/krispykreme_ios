import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/model/faqs.dart';

class FAQsItem extends StatelessWidget {
  const FAQsItem(this.faqs);

  final FAQs faqs;

  Widget _buildTiles(FAQs root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title, 
      style: TextStyle(
        fontFamily: 'Quicksand',
        color: AppColors.green,
        fontSize: 13.0
        )
      )
    );
    return ExpansionTile(
      key: PageStorageKey<FAQs>(root),
      title: Text(root.title, 
        style: TextStyle(
          fontFamily: 'Quicksand',
          color: AppColors.green,
          fontSize: 15.0, 
          fontWeight: FontWeight.bold
        )
      ),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(faqs);
  }
}
