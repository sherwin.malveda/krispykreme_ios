import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';

class NewsList extends StatelessWidget {

  final VoidCallback onTap;
  final String news;
  final String description;
  final String image;

  const NewsList({Key key, @required this.onTap,  @required this.news, @required this.description, @required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius:BorderRadius.only(topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0), bottomRight: Radius.circular(15.0))),
      child: Container(
        height: MediaQuery.of(context).size.height * .3,
        width: MediaQuery.of(context).size.width * .5,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0), bottomRight: Radius.circular(15.0))
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * .17,
              width: MediaQuery.of(context).size.width * .5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0))
              ),
              child: Container(
                padding: EdgeInsets.all(0),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),
                    topRight: Radius.circular(15.0)),
                  child:CachedNetworkImage(
                    imageUrl: image,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => new Center (
                      child: CircularProgressIndicator(backgroundColor: AppColors.green,)
                    ),
                    errorWidget: (context, url, error) => new Container(
                      child:Center(
                        child: Image.asset('assets/image/logo.png', fit: BoxFit.contain)
                      )
                    )
                  )
                )
              )
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.fromLTRB(10,0,0,0),
                alignment: Alignment.topLeft,
                child:Text(news,
                maxLines: 3,
                  style: TextStyle(
                    fontSize: 13.0, 
                    color: AppColors.black,
                    fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.start,
                )
              )
            ),
            // Expanded(
            //   child: Container(
            //     padding: EdgeInsets.fromLTRB(10,10,0,0),
            //     child:Text(description,
            //       style: TextStyle(
            //         fontSize: 12.0, 
            //         color: AppColors.black,
            //       ),
            //       textAlign: TextAlign.start,
            //     )
            //   )
            // ),
            Container(
              padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
              height: 30,
              alignment: Alignment.center,
              child: Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(5.0),
                color: AppColors.green,
                child: MaterialButton(
                  padding:  EdgeInsets.fromLTRB(20, 0, 20,0),
                  child:Text("Read Article", 
                    style: TextStyle(fontSize: 12.0, color: AppColors.white,),textAlign: TextAlign.center,),
                  onPressed: onTap
                ),
              )
            ),
            SizedBox(height: 15)
          ],
        )
      )
    );
  }
}
