import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/widgets/dashed_rect.dart';
import 'package:qr_flutter/qr_flutter.dart';

class CardVoucher extends StatelessWidget {
  final String code;

  const CardVoucher({Key key, @required this.code})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        child: DashedRect(
          color: AppColors.green,
          gap: 1.0,
          strokeWidth: 4,
          child: Container(
            padding: EdgeInsets.fromLTRB(20.0, 10, 20, 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 10.0),
                Stack(
                  children: <Widget>[
                    QrImage(
                      data: code,
                      version: QrVersions.auto,
                      size: 180,
                      gapless: false,
                      foregroundColor: AppColors.green,
                      errorStateBuilder: (cxt, err) {
                        return Container(
                          child: Center(
                            child: Text(
                              "Uh oh! Something went wrong...",
                              textAlign: TextAlign.center,
                            ),
                          ),
                        );
                      },
                    )
                  ],
                ),
                Text("************"+code.substring(code.length-4, code.length),
                  style: TextStyle(color: AppColors.green, fontSize: 18.0),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}