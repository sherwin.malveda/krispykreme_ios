import 'package:flutter/material.dart';

class DisplayField extends StatelessWidget {
  final Widget child;

  const DisplayField({Key key, @required this.child})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          child: Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              border: Border.all(color: Theme.of(context).hintColor),
              borderRadius: BorderRadius.circular(20.0)),
              child: Row(
              children: <Widget>[
                child,
              ],
            ),
          ),
        ),
      ),
    );
  }
}