import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/model/transaction.dart';
import 'package:intl/intl.dart';

class TxnList extends StatelessWidget {

  final Transaction hist;

  const TxnList({Key key, @required this.hist})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
      decoration: new BoxDecoration(
        border: Border(bottom: BorderSide(color: AppColors.black))
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(hist.transactionType,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: AppColors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0
                )
              ),
              Text(changedateFormat(hist.transactionDate) + " " + hist.transactionDate.toString().substring(11,19),
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: AppColors.black,
                  fontSize: 12.0
                )
              ),
            ],
          ),
          
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("RRN. "+hist.transactionId,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: AppColors.black,
                  fontSize: 12.0
                )
              ),
              Text(hist.voucherCode,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: AppColors.black,
                  fontSize: 12.0
                )
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Claimed at "+ hist.storeLocation,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: AppColors.black,
                  fontSize: 12.0
                )
              ),
              Text(hist.invoiceNumber,
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: AppColors.black,
                  fontSize: 12.0
                )
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Php. "+ hist.transactionAmount.toString(),
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: AppColors.black,
                  fontSize: 12.0
                )
              ),
              Text(txnType(hist.transactionStatus),
                style: TextStyle(
                  fontFamily: 'Quicksand',
                  color: AppColors.black,
                  fontSize: 12.0
                )
              ),
            ],
          ),
          SizedBox(height: 10)
        ],
      ),
    );
  }

  String changedateFormat(DateTime date){
    final DateFormat formatter = DateFormat('MMM dd, yyyy');
    final String formatted = formatter.format(date);
    return formatted;
  }

  String txnType(int i){
    if(i == 0){
      return "Processing";
    }else if(i == 1){
      return "Success";
    }else if(i == 2){
      return "Failed";
    }else if(i == 3){
      return "Reversed";
    }else{
      return "Voided";
    }
  }
}
