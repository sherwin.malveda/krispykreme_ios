import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';

class PromoList extends StatelessWidget {

  final VoidCallback onTap;
  final String promo;
  final String validity;
  final String image;

  const PromoList({Key key, @required this.onTap,  @required this.promo, @required this.validity, @required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius:BorderRadius.only(topLeft: Radius.circular(25.0),
            topRight: Radius.circular(25.0), bottomRight: Radius.circular(25.0))),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(25.0),
            topRight: Radius.circular(25.0), bottomRight: Radius.circular(25.0))
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * .18,
              width: MediaQuery.of(context).size.width * .43,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(25.0))
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(25.0)),
                child: CachedNetworkImage(
                  imageUrl: image,
                  fit: BoxFit.cover,
                  placeholder: (context, url) => new Center (
                    child: CircularProgressIndicator(backgroundColor: AppColors.green,)
                  ),
                  errorWidget: (context, url, error) => new Container(
                    child:Center(
                      child: Image.asset('assets/image/logo.png', fit: BoxFit.contain)
                    )
                  )
                )
              )
            ),
            SizedBox(height: 10),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * .07,
                    padding: EdgeInsets.fromLTRB(10,10,5,0),
                    child:Text(promo,
                      maxLines: 2,
                      style: TextStyle(
                        fontSize: 12.0, 
                        color: AppColors.black,
                        fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.start,
                    )
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10,5,0,0),
                    alignment: Alignment.bottomLeft,
                    child:Text( "Valid till "+ validity.substring(0,10),
                      style: TextStyle(
                        fontSize: 11.0, 
                        color: AppColors.black,
                      ),
                      textAlign: TextAlign.start,
                    )
                  ),
                  SizedBox(height: MediaQuery.of(context).size.width * .02),
                  Container(
                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                    height: 30,
                    alignment: Alignment.bottomCenter,
                    child: Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(5.0),
                      color: AppColors.green,
                      child: MaterialButton(
                        padding:  EdgeInsets.fromLTRB(30, 0, 30,0),
                        child:Text("View More", 
                          style: TextStyle(fontSize: 12.0, color: AppColors.white,),textAlign: TextAlign.center,),
                        onPressed: onTap
                      ),
                    )
                  ),
                ],
              )
            )
          ],
        )
      )
    );
  }
}
