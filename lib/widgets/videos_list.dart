import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';
import 'package:krispykreme/screens/videos_detailed_page.dart';

class VideosList extends StatelessWidget {

  final VoidCallback onTap;
  final String description;
  final String image;
  final int id;

  const VideosList({Key key, @required this.onTap, @required this.description, @required this.image, @required this.id})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius:BorderRadius.only(topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0), bottomRight: Radius.circular(15.0))),
      child: Container(
        height: MediaQuery.of(context).size.height * .37,
        width: MediaQuery.of(context).size.width * .9,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0), bottomRight: Radius.circular(15.0))
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * .25,
              width: MediaQuery.of(context).size.width * .9,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0)),
                color: Colors.red
              ),
              child: FlatButton(
                shape: RoundedRectangleBorder(borderRadius:BorderRadius.only(topLeft: Radius.circular(15.0))),
                color: Colors.blue,
                padding: EdgeInsets.all(0),
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0)),
                      child:CachedNetworkImage(
                        imageUrl: image,
                        fit: BoxFit.cover,
                        placeholder: (context, url) => new Center (
                          child: CircularProgressIndicator(backgroundColor: AppColors.green,)
                        ),
                        errorWidget: (context, url, error) => new Container(
                          child:Center(
                            child: Image.asset('assets/image/logo.png', fit: BoxFit.contain)
                          )
                        )
                      ),
                    ),
                    Positioned(
                      top: MediaQuery.of(context).size.height * .09,
                      left: 0,
                      right: 0,
                      child: Image.asset("assets/icons/play.png", width: 50, height: 50,),
                    )
                  ],
                ),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>
                      VideosDetailsPage(
                        promosid: id,
                      )
                    )
                  );
                },
              )
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.fromLTRB(10,0,0,0),
                alignment: Alignment.topLeft,
                child:Text(description,
                maxLines: 2,
                  style: TextStyle(
                    fontSize: 14.0, 
                    color: AppColors.black,
                    fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.start,
                )
              )
            ),
            Container(
              padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
              height: 30,
              alignment: Alignment.center,
              child: Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(5.0),
                color: AppColors.green,
                child: MaterialButton(
                  padding:  EdgeInsets.fromLTRB(20, 0, 20,0),
                  child:Text("Watch Video", 
                    style: TextStyle(fontSize: 12.0, color: AppColors.white,),textAlign: TextAlign.center,),
                  onPressed: onTap
                ),
              )
            ),
            SizedBox(height: 15)
          ],
        )
      )
    );
  }
}
