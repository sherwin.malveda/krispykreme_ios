import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';

class BranchRow extends StatelessWidget {
  final String branch;
  final VoidCallback onPressed;

  const BranchRow({Key key, @required this.branch, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.violet3, borderRadius: BorderRadius.circular(4.0)
      ),
      margin: EdgeInsets.symmetric(horizontal: 4.0, vertical: 1.0),
      child: InkWell(
        onTap: onPressed,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Krispy Kreme " +branch,
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        color: AppColors.green, 
                        fontSize: 14,
                        fontWeight: FontWeight.w500 
                      )
                    ),
                  ],
                ),
              ),
              Icon(Icons.chevron_right, color: AppColors.green)
            ],
          ),
        ),
      ),
    );
  }
}