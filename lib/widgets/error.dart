import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:krispykreme/app_colors.dart';

void errorMessage (String errorMessage, BuildContext context){
    
  // Flushbar(
  //   flushbarPosition: FlushbarPosition.TOP,
  //   flushbarStyle: FlushbarStyle.GROUNDED,
  //   reverseAnimationCurve: Curves.decelerate,
  //   forwardAnimationCurve: Curves.elasticOut,
  //   backgroundColor: AppColors.green,
  //   icon: Icon(
  //     Icons.error,
  //     color:AppColors.pink,
  //   ),
  //   messageText: Text(
  //     "$errorMessage",
  //     style: TextStyle(
  //       fontSize: 15.0, 
  //       color: Colors.white,
  //       fontFamily: "Quicksand"),
  //   ),
  //   duration:  Duration(seconds: 8),              
  // )..show(context);
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: new DecorationImage(
                image: AssetImage('assets/image/bg.png'),
                fit: BoxFit.cover,
              ),
            ),
            height: MediaQuery.of(context).size.height * .42,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:<Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * .03 ),
                Icon(Icons.error, color: Colors.red, size: 80),
                SizedBox(height: MediaQuery.of(context).size.height * .01 ),
                Padding(
                  padding: EdgeInsets.fromLTRB(20,0,20,0),
                  child: Text(errorMessage,
                    style: TextStyle(
                    fontSize: 15.0, 
                    color: AppColors.black,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
                Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal:0, vertical: 5),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(15.0),
                    color: AppColors.green,
                    child: MaterialButton(
                      height: 40,
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0)),
                      child:Text("Ok", 
                        style: TextStyle(fontSize: 15.0, color: AppColors.white, fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ),
                  )
                ),
                SizedBox(height: MediaQuery.of(context).size.height * .02),
              ]
            )
          ),
        );
      },
    );

}