import 'package:flutter/material.dart';

class SelectorField extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;

  const SelectorField({Key key, @required this.child, @required this.onPressed})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              border: Border.all(color: Theme.of(context).hintColor),
              borderRadius: BorderRadius.circular(20.0)),
              child: Row(
              children: <Widget>[
                child,
                Spacer(),
                Icon(Icons.chevron_right, color: Theme.of(context).hintColor)
              ],
            ),
          ),
        ),
      ),
    );
  }
}