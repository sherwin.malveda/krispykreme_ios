
class FormValidators {
  
  static String isIdentical(String a, String b, String errorMessage) {
    if ((a == null || b == null) || (a.isEmpty || b.isEmpty)) return null;
    if (a != b) {
      return errorMessage;
    }
    return null;
  }

  static isAlphanumeric(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    if (value.length < 3 || value.length >16) return errorMessage;
  }

  static String isLettersOnly(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    return RegExp(r"^(?=.{1,50}$)[A-Za-z]+(?:['_.\s][A-Za-z]+)*$")
        .hasMatch(value)
        ? null
        : errorMessage;
  }

  static String isMobileNumber(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    return RegExp(r"\d{11}$").hasMatch(value)
        ? null
        : errorMessage;
  }

  static String isNumber(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    return RegExp(r"\d$").hasMatch(value)
        ? null
        : errorMessage;
  }

  static String isNotEmpty(String value, String errorMessage) {
    if (value == null || value.isEmpty) return errorMessage;

    return null;
  }

  static String isValidEmail(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    return RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(value)
        ? null
        : errorMessage;
  }
}
